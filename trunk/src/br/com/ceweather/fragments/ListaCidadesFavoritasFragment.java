package br.com.ceweather.fragments;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.ceweather.activity.AppDatabase;
import br.com.ceweather.activity.ComponentsActivity;
import br.com.ceweather.activity.R;
import br.gov.funceme.database.dao.CidadeDAO;
import br.gov.funceme.database.dao.RequisicaoDAO;
import br.gov.funceme.database.model.Cidade;
import br.gov.funceme.database.model.TempoAgoraFavorito;
import br.gov.funceme.webservices.service.TempoAgoraFavoritoService;
import br.gov.funceme.webservices.ws.AbstractServiceListener;

public class ListaCidadesFavoritasFragment extends ListFragment {

	private AppDatabase app;
	
	private ArrayList<TempoAgoraFavorito> listInfoCities;
	private ComponentsActivity componentsActivity;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.list, null);
	}
	
	public void onActivityCreated(Bundle savedInstanceState) {
		
		super.onActivityCreated(savedInstanceState);

		app = new AppDatabase(getActivity());
		
		componentsActivity = (ComponentsActivity)getActivity();
		
		List<Cidade> cidadesFavoritas = CidadeDAO.getCidadesFavoritas(app.getDB());
		
		getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {
				
				RequisicaoDAO.truncate(app.getDB());
				
				String codigoCidade = view.getTag().toString(); 
				
				Cidade cidade = CidadeDAO.getCidade(app.getDB(), codigoCidade);
				
				CidadeDAO.atualizarCidadeCarregada(app.getDB(), cidade);
				
				componentsActivity.reinit();
			}
		});

		TempoAgoraFavoritoService service = new TempoAgoraFavoritoService(getActivity());

		AbstractServiceListener listener = new AbstractServiceListener() {
			@SuppressWarnings("unchecked")
			@Override
			public void OnComplete(Object params) {
				
				ArrayList<TempoAgoraFavorito> list;
				
				if (params!=null)
					list = (ArrayList<TempoAgoraFavorito>) params;
				else 
					list = new ArrayList<TempoAgoraFavorito>();
				
				if (list != null){

					listInfoCities = new ArrayList<TempoAgoraFavorito>(list.size());
					listInfoCities = list;

					CidadesFavoritasList adapter = new CidadesFavoritasList(getActivity());

					for (Cidade cidade : CidadeDAO.getCidadesFavoritas(app.getDB())) {

						adapter.add(new CidadeFavoritaItem(cidade.getCodigo(), cidade.getNome()));
					}
					
					setListAdapter(adapter);
				}
			}
		};

		service.setServiceListener(listener);
		service.getWeatherNowFormFavorites(cidadesFavoritas); 
	}

	private class CidadeFavoritaItem {
		
		public String city;
		public String code;
		public CidadeFavoritaItem(String code, String city) {
			this.city = city;
			this.code = code;
		}
	}

	public class CidadesFavoritasList extends ArrayAdapter<CidadeFavoritaItem> {

		public CidadesFavoritasList(Context context) {
			super(context, 0);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.menu_fav_cities_row, null);
			}
			
			try {
				
				if (listInfoCities == null || position > listInfoCities.size())
					throw new Exception(getString(R.string.aviso_favoritos_vazio));
				
				convertView.setTag(getItem(position).code);
				
				TempoAgoraFavorito model = listInfoCities.get(position);
	
				TextView cityName = (TextView) convertView.findViewById(R.id.txtFavCitySelected); //por algum motivo macabro do android ou do adt, as chaves se trocam no meio do caminho :S:S:S:S:S:S:S
				cityName.setText(getItem(position).city);
	
				String[] format = new String[2];
				
				//TextView cityUpdateDate = (TextView) convertView.findViewById(R.id.txtFavCityAtualizacao);
				//cityUpdateDate.setText(model.getAtualizacao());
	
				TextView cityTemp = (TextView) convertView.findViewById(R.id.txtFavCityTemp);
				format = model.getTemperatura().toString().split(" ");
				
				cityTemp.setText(format[0].toString()+"°C");
	
				TextView cityHumid = (TextView) convertView.findViewById(R.id.txtFavCityUmidade);
				format = model.getUmidade().toString().split(" ");
				
				cityHumid.setText(format[0].toString()+"%");
	
				TextView cityRain = (TextView) convertView.findViewById(R.id.txtFavCityPrecipitacao);
				cityRain.setText(model.getChuva());
	
				TextView cityWind = (TextView) convertView.findViewById(R.id.txtFavCityVento);
				cityWind.setText(model.getVentoVel() + " " + model.getVentoDir());
				
			} catch (Exception e){
				Log.d("ERRO", e.getMessage());
			}

			return convertView;
		}
	}
}
