package br.com.ceweather.fragments;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import org.json.JSONObject;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.ceweather.activity.ComponentsActivity;
import br.com.ceweather.activity.R;
import br.gov.funceme.database.dao.RequisicaoDAO;
import br.gov.funceme.database.model.PrevisaoCE;
import br.gov.funceme.database.model.Requisicao;
import br.gov.funceme.database.tipo.RequisicaoTipo;
import br.gov.funceme.webservices.service.PrevisaoCEGridService;
import br.gov.funceme.webservices.ws.AbstractServiceListener;
import br.gov.funceme.webservices.ws.WaitView;

public class PrevisaoCEFragment extends Fragment {

	final static String ARG_POSITION = "position";
	int mCurrentPosition = -1;
	public ComponentsActivity activity;
	
	private WaitView waiting;
	public ViewGroup fragmentContainer;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, 
			Bundle savedInstanceState) {

		activity = (ComponentsActivity) getActivity();
		
		fragmentContainer = container;
		
		if (savedInstanceState != null) {
			mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
		}

		return inflater.inflate(R.layout.forecast_ce_grid, container, false);
	}

	@Override
	public void onStart() {
		
		super.onStart();
		
		Requisicao last = null;
		
		if (RequisicaoDAO.getRequisicaoCount(activity.getDB(), RequisicaoTipo.PREVISAO_CE) > 0) {
		
			last = RequisicaoDAO.getUltimaRequisicao(activity.getDB(), RequisicaoTipo.PREVISAO_CE);
			
			render(new PrevisaoCE(last.getJson().toString()));
		}	
		 
		if (last != null) {
			
			Calendar c = Calendar.getInstance(); 
			c.setTime(last.getDatahora());
			c.add(Calendar.MINUTE, Integer.parseInt(getString(R.string.TIMEOUT_PREVISAO)));
			
			Date now = new Date();
			
			if (now.after(c.getTime()))
				doRequest();
			else
				activity.requestCompleted();
			
		} else {
			doRequest();
		}
	}
	
	private void doRequest() {
		
		PrevisaoCEGridService service = new PrevisaoCEGridService(activity);
		
		waiting = new WaitView(activity, (ViewGroup) fragmentContainer.getChildAt(0));
		waiting.enable();

		AbstractServiceListener listener = new AbstractServiceListener() {
			
			@Override
			public void OnComplete(Object params) {
				
				waiting.disable();
				
				if(params != null)
				{		
					Requisicao req = new Requisicao();
					
					req.setDatahora(new Timestamp(new Date().getTime()));
					req.setInfo(RequisicaoTipo.PREVISAO_CE);
					
					req.setJson((String) params); 
					
					RequisicaoDAO.create(activity.getDB(), req);
					
					Requisicao last = RequisicaoDAO.getUltimaRequisicao(activity.getDB(), RequisicaoTipo.PREVISAO_CE);
					
					render(new PrevisaoCE(last.getJson().toString()));
				} else { 
					Log.e("CE", "Oops! Problemas ao carregar previsão do Ceará :(");
				}
				
				activity.requestCompleted();
			}
		};

		service.setServiceListener(listener);
		service.getForecast();		
	}
	
	private void render(PrevisaoCE forecast)
	{
		try {
			
			JSONObject jsonResponse = new JSONObject(forecast.getColunas()); 
		
			Iterator<?> keys = jsonResponse.keys();
			
			while( keys.hasNext() ){
	            
				String key = (String)keys.next();
	            String value = (String)jsonResponse.get(key);
	            
	            String type = key.substring(4, 5);
	            
	            if (type.equals("a")) {
	            	
	            	int resID = getResources().getIdentifier("image_forecast_ce_"+key.substring(0, 3), "id", getActivity().getPackageName());
	            	ImageView img0 = (ImageView)activity.findViewById(resID);
					img0.setImageResource(getImage(value));
	            }
	        }
			
			Typeface fontSemiBold = Typeface.createFromAsset(activity.getAssets(), "fonts/TitilliumWeb-SemiBold.ttf");
			
			TextView date = (TextView) activity.findViewById(R.id.text_forecast_ce_date);
			date.setTypeface(fontSemiBold);
			date.setText(getString(R.string.label_atualizacao)+" "+forecast.getDataPrevisao());
			
			TextView description = (TextView) activity.findViewById(R.id.text_forecast_ce_description);
			description.setText(forecast.getPrevisao24horas());
			
		} catch(Exception e){
			Log.d("DEBUG", e.getMessage());
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		outState.putInt(ARG_POSITION, mCurrentPosition);
	}
	
	private int getImage(String iconName) {
		
		String name = "img_indisponivel";

		if (iconName.equals("icone1.gif")) {
			name = "limpo_dia";	
		} else if (iconName.equals("icone2.gif")) {
			name = "icon_29_dia";	
		} else if (iconName.equals("icone3.gif")) {
			name = "icon_sol_chuva";		
		} else if (iconName.equals("icone4.gif")) {
			name = "icon_65_dia";
		} else if (iconName.equals("icone5.gif")) {
	    	name = "icon_88";
	    } else if (iconName.equals("icone6.gif")) {
	    	name = "icon_79";
	    }

		int resID = activity.getResources().getIdentifier(name , "drawable", activity.getPackageName());
	    
	    return resID;
	}
}