package br.com.ceweather.fragments;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import br.com.ceweather.activity.AppDatabase;
import br.com.ceweather.activity.ComponentsActivity;
import br.com.ceweather.activity.R;
import br.com.ceweather.adapters.SolLuaAdapter;
import br.gov.funceme.database.dao.CidadeDAO;
import br.gov.funceme.database.dao.RequisicaoDAO;
import br.gov.funceme.database.model.Mare;
import br.gov.funceme.database.model.PortoLuaMares;
import br.gov.funceme.database.model.Requisicao;
import br.gov.funceme.database.model.SolLua;
import br.gov.funceme.database.tipo.RequisicaoTipo;
import br.gov.funceme.webservices.service.SolLuaService;
import br.gov.funceme.webservices.ws.AbstractServiceListener;
import br.gov.funceme.webservices.ws.WaitView;

public class SolLuaFragment extends Fragment {

	final static String ARG_POSITION = "position";
	
	AppDatabase app;
	
	int mCurrentPosition = -1;
	
	public ComponentsActivity activity;
	
	private WaitView waiting;
	
	private View view;
	private TwoWayView sunMoonDaysView;
	private SolLuaAdapter sunMoonAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, 
			Bundle savedInstanceState) {

		activity = (ComponentsActivity)getActivity();
		
		app = new AppDatabase(activity);
		
		if (savedInstanceState != null) {
			mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
		}
		
		this.view = inflater.inflate(R.layout.sun_moon, container, false);
				
		sunMoonDaysView = (TwoWayView) this.view.findViewById(R.id.sun_moon_days);
		
		return this.view;
	}
	
	@Override
	public void onStart() {
		
		super.onStart();
		
		Requisicao last = null;
		
		if (RequisicaoDAO.getRequisicaoCount(activity.getDB(), RequisicaoTipo.SOL_LUA) > 0) {
		
			last = RequisicaoDAO.getUltimaRequisicao(activity.getDB(), RequisicaoTipo.SOL_LUA);
			
			render(parse(last.getJson()));
		}	
		
		if (last != null) {
			
			Calendar c = Calendar.getInstance(); 
			c.setTime(last.getDatahora());
			c.add(Calendar.MINUTE, Integer.parseInt(getString(R.string.TIMEOUT_PREVISAO)));
			
			Date now = new Date();
			
			if(now.after(c.getTime()))
				doRequest();
			else 
				activity.requestCompleted();
			
		} else {
			doRequest();
		}
	}

	private void doRequest() {
		
		SolLuaService sunService = new SolLuaService(activity);

		waiting = new WaitView(activity, (ViewGroup) this.view);
		waiting.enable();
		
		AbstractServiceListener sunServiceListener = new AbstractServiceListener() {
			@Override
			public void OnComplete(Object params) {

				waiting.disable();
				
				Requisicao req = new Requisicao();
				
				req.setDatahora(new Timestamp(new Date().getTime()));
				req.setInfo(RequisicaoTipo.SOL_LUA);
				
				req.setJson((String) params); 
				
				RequisicaoDAO.create(activity.getDB(), req);
				
				render(parse((String) params));
				
				activity.requestCompleted();				
			}
		};

		sunService.setServiceListener(sunServiceListener);
		sunService.getSunCity(CidadeDAO.getCidadeCarregada(app.getDB()).getCodigo());
	}
	
	private void render(List<SolLua> list)
	{
		if(list != null)
		{		
			sunMoonAdapter = new SolLuaAdapter(activity, R.layout.sun_moon_day, list);
			sunMoonDaysView.setAdapter(sunMoonAdapter);
		}
	}
	
	private List<SolLua> parse (String response) 
	{
		List<SolLua> sunList = null;
		
		JSONArray jsonResponse = null;
		
		try {
			
			if(response == null)
				throw new Exception("Oops! Resposta vazia para a solicitação de forescat da cidade.");
			
			jsonResponse = new JSONArray(response);
			
			sunList = new ArrayList<SolLua>();
			
			for(int i = 0; i < jsonResponse.length(); i++) {
				
				JSONObject sunJSON = jsonResponse.getJSONObject(i);
				
				SolLua sun = new SolLua();
				
				String dia = (sunJSON.has("dia"))? sunJSON.getString("dia") : null;
				
				if (dia!=null) {
					String d[] = dia.split("-"); 
					if (d.length==3)
						sun.setDia(d[2]+"/"+d[1]+"/"+d[0].substring(2, 4));
				}
				
				if (sunJSON.has("nascente")) sun.setNascente(sunJSON.getString("nascente"));
				if (sunJSON.has("poente")) sun.setPoente(sunJSON.getString("poente"));
				
				String lua = (sunJSON.has("lua"))? sunJSON.getString("lua") : "";
				
				if (lua.equals("che"))
					lua = "Cheia";
				else if (lua.equals("cre"))
					lua = "Crescente";
				else if (lua.equals("min"))
					lua = "Minguante";
				else if (lua.equals("nov"))
					lua = "Nova";
				
				sun.setLua(lua);
				
				sunList.add(sun);
			}
			
		} catch(Exception e)
		{
			sunList = null;
			Log.d("DEBUG", e.getMessage());
		}
		
		return sunList;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		
		super.onSaveInstanceState(outState);

		outState.putInt(ARG_POSITION, mCurrentPosition);
	}
}
