package br.com.ceweather.fragments;

import java.util.ArrayList;

import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import br.com.ceweather.activity.ComponentsActivity;
import br.com.ceweather.activity.R;
import br.gov.funceme.components.map.MapAnimationFragment;
import br.gov.funceme.database.model.Radar;
import br.gov.funceme.database.model.RadarResponse;
import br.gov.funceme.webservices.service.RadarSService;
import br.gov.funceme.webservices.service.RadarXService;
import br.gov.funceme.webservices.ws.AbstractServiceListener;
import br.gov.funceme.webservices.ws.WaitView;

import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

public class RadarSateliteFragment extends MapAnimationFragment {
	
	ComponentsActivity activity;
	
	private String[]  listTexts;
	private String [] listUrls;
	
	private final float ZOOM_BANDA_S = (float) 5.87;
	private final float ZOOM_BANDA_X = (float) 7.6;
	
	private final double LAT_NORTH_RADAR_S = -1.46845; 
	private final double LAT_SOUTH_RADAR_S = -8.640934;
	private final double LON_EAST_RADAR_S  = -35.675010;
	private final double LON_WEST_RADAR_S  = -42.889911;
	
	private final double LAT_CENTER_BANDA_S = -5.071640025;
	private final double LON_CENTER_BANDA_S = -39.28050183;	

	private final int RADIUS_BANDA_S = 400000; // em metros
	
	private final double LAT_NORTH_RADAR_X = -2.709; 
	private final double LAT_SOUTH_RADAR_X = -4.88;
	private final double LON_EAST_RADAR_X  = -37.47;
	private final double LON_WEST_RADAR_X  = -39.645;
	
	private final double LAT_CENTER_BANDA_X = -3.800332295;
	private final double LON_CENTER_BANDA_X = -38.55974678;	

	private final int RADIUS_BANDA_X = 120000; // em metros
	
	private final double LAT_CENTER_SATELITE = -10.59259;
	private final double LON_CENTER_SATELITE = -41.613;
	
	private final double LAT_NORTHEAST_SATELITE =  12.62; // 11.90763;
	private final double LON_NORTHEAST_SATELITE = -29.94; //-29.92768;
	private final double LAT_SOUTHWEST_SATELITE = -32.53; //-31.27755;
	private final double LON_SOUTHWEST_SATELITE = -57.24; //-57.29955;
	
	private final float ZOOM_SATELITE = (float) 4.5;
	
	private Button btnPaleta;
	
	private ImageView paleta;
	private float positionXPaleta;
	private TextView txtTime;
	
	private WaitView waiting;
	
	private boolean aviso = false;
	private boolean alerta = false;
	
	private Point size; 
	
	public RadarSateliteFragment() {
		super();
	}

	public RadarSateliteFragment(Point size) {
		super();
		
		this.size = size;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		this.setup(R.layout.radar_satellite, R.id.map, this.size);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		
		super.onViewCreated(view, savedInstanceState);
		
		btnPaleta = (Button) this.view.findViewById(R.id.btnPaleta);
		paleta = (ImageView) this.view.findViewById(R.id.imgPaleta);
		positionXPaleta = paleta.getX();
		txtTime = (TextView) this.view.findViewById(R.id.txtTimeRadar);
		
		waiting = new WaitView(getActivity(), (ViewGroup) this.view);
		
		activity = (ComponentsActivity) getActivity();
		
		this.scrolHandle();
		this.animatePaleta("hide");
		
		this.createActionsButtons();
		
		this.loadRadarQuixeramobim();
	}
	
	private int getScale(int value) {

		int scale; 

		DisplayMetrics metrics = this.getResources().getDisplayMetrics();

		float s = (float)value/(float)290;
		float xdpi = metrics.xdpi;

		scale = (int)Math.ceil(s*xdpi);

		return scale; 
	}
	
	private void animatePaleta(String action){
		
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		
		if (action.equals("show")) {
			
			if (currentapiVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN){
				paleta.animate()
					.translationX(positionXPaleta+this.getScale(1))
					.alpha(1.0f)
					.withLayer();
			} else{
				paleta.setAlpha(1.0f);
			}
			
			btnPaleta.setTag("show");
			btnPaleta.setText(getString(R.string.botao_legenda_satelite_esconder));
		} else {
			
			if (currentapiVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN){
				paleta.animate()
					.translationX(positionXPaleta-this.getScale(160))
					.alpha(0.0f)
					.withLayer();
			} else {
				paleta.setAlpha(0.0f);
			}
				
			btnPaleta.setTag("hide");
			btnPaleta.setText(getString(R.string.botao_legenda_satelite_exibir));
		}
	}
	
	private void scrolHandle() {
		
		ImageView transparentImageView = (ImageView) this.view.findViewById(R.id.imageViewTransparent);

		transparentImageView.setOnTouchListener(new View.OnTouchListener() {

			@Override
		    public boolean onTouch(View v, MotionEvent event) {
		        int action = event.getAction();
		        
		        ScrollView mainScrollView = (ScrollView) getActivity().findViewById(R.id.scrollViewContainer);
		        
		        switch (action) {
		           case MotionEvent.ACTION_DOWN:
		                mainScrollView.requestDisallowInterceptTouchEvent(true);
		                return false;

		           case MotionEvent.ACTION_UP:
		                mainScrollView.requestDisallowInterceptTouchEvent(false);
		                return true;

		           case MotionEvent.ACTION_MOVE:
		                mainScrollView.requestDisallowInterceptTouchEvent(true);
		                return false;

		           default: 
		                return true;
		        }   
		    }
		});
	}
	
	public void loadRadarQuixeramobim() {
		
		txtTime.setText(getString(R.string.carregando_radar_s)+"...");
		
		activity.ativarBotaoRadarS();
		
		paleta.setImageResource(this.activity.getResources().getIdentifier("paleta", "drawable", activity.getPackageName()));

		LatLng center = new LatLng(LAT_CENTER_BANDA_S, LON_CENTER_BANDA_S);
		
		clear();
		
		centralize(center, ZOOM_BANDA_S);
		drawCircle(center, RADIUS_BANDA_S);
		
		if (waiting.isEnabled())
			waiting.disable();
		
		waiting.enable();
		
		RadarSService service = new RadarSService(getActivity());

		AbstractServiceListener listener = new AbstractServiceListener() {
			
			@Override
			public void OnComplete(Object params) {
				@SuppressWarnings("unchecked")
				
				RadarResponse response = (RadarResponse) params;
				
				ArrayList<Radar> list = response.getList();
				
				try {
					
					waiting.disable();
					
					if (list == null) 
						throw new Exception(getString(R.string.erro_carregamento_imagens));
					
					if (list.size()==0) {
						showInfo("Radar Quixeramobim fora do ar. Direcionando para o Radar de Fortaleza.");
						loadRadarFortaleza();
						activity.desativarBotaoRadarS();
						throw new Exception(getString(R.string.aviso_nenhuma_imagem_carregada));
					}
					
					listUrls = new String[list.size()];
					listTexts = new String[list.size()];
					
					int i = 0;
					
					for (Radar radar : list) {
						
						listUrls[i]  = radar.getImg();		
						listTexts[i] = radar.getText();
						i++;
					}
					
					loadImages(listUrls);
					
					startAnimation(LAT_NORTH_RADAR_S, LAT_SOUTH_RADAR_S, LON_EAST_RADAR_S, LON_WEST_RADAR_S);
					
					if (!response.getInfo().equals("") && !aviso) {
						aviso = true;
						showInfo(response.getInfo());
					}
					
					if (!response.getAlert().equals("") && !alerta) {
						alerta = true;
						showAlert(response.getAlert());
					}
					
				} catch (Exception e) {
					txtTime.setText(e.getMessage());
					Log.d("ERRO", e.getMessage());
				}
				
				activity.requestCompleted();
			}
		};

		service.setServiceListener(listener);		
		service.getListImagesRadar(imagesDownload(32, 5), 3);
	}
	
	public void loadRadarFortaleza() {
		
		txtTime.setText(getString(R.string.carregando_radar_x)+"...");
		
		activity.ativarBotaoRadarX();
		
		paleta.setImageResource(this.activity.getResources().getIdentifier("paleta", "drawable", activity.getPackageName()));
		
		LatLng center = new LatLng(LAT_CENTER_BANDA_X, LON_CENTER_BANDA_X);
		
		clear();
		centralize(center, ZOOM_BANDA_X);
		drawCircle(center, RADIUS_BANDA_X);
		
		if (waiting.isEnabled())
			waiting.disable();
		
		waiting.enable();
		
		RadarXService service = new RadarXService(getActivity());

		AbstractServiceListener listener = new AbstractServiceListener() {
			
			@Override
			public void OnComplete(Object params) {
				@SuppressWarnings("unchecked")
				
				RadarResponse response = (RadarResponse) params;
				
				ArrayList<Radar> list = response.getList();
				
				try {
					
					waiting.disable();
					
					if (list == null) 
						throw new Exception(getString(R.string.erro_carregamento_imagens));
					
					if (list.size()==0) {
						showInfo("Radar Fortaleza fora do ar. Direcionando para o Satélite.");						
						activity.desativarBotaoRadarX();
						throw new Exception(getString(R.string.aviso_nenhuma_imagem_carregada));
					}
					
					listUrls = new String[list.size()];
					listTexts = new String[list.size()];
					
					int i = 0;
					
					for (Radar radar : list) {
						
						listUrls[i]  = radar.getImg();	
						listTexts[i] = radar.getText();
						i++;
					}
					
					loadImages(listUrls);
					
					startAnimation(LAT_NORTH_RADAR_X, LAT_SOUTH_RADAR_X, LON_EAST_RADAR_X, LON_WEST_RADAR_X);
					
					if (!response.getInfo().equals("") && !aviso) {
						aviso = true;	
						showInfo(response.getInfo());
					}
					
					if (!response.getAlert().equals("")) {
						alerta = true;
						showAlert(response.getAlert());
					}
					
				} catch (Exception e) {
					txtTime.setText(e.getMessage());
					Log.d("ERRO", e.getMessage());
				}
				
				activity.requestCompleted();
			}
		};

		service.setServiceListener(listener);	
		service.getListImagesRadar(imagesDownload(32, 5), 3);
	}
	
	@Override
	public void onStepAnimation() {
		
		txtTime.setText(listTexts[countTimer]);
		
		super.onStepAnimation();
	}
	
	private void createActionsButtons(){
		
		btnPaleta.setTextColor(Color.BLACK);
		btnPaleta.setAlpha((float)1.0);
		
		btnPaleta.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				if (btnPaleta.getTag().toString().equals("show")) {
					animatePaleta("hide");
				} else{
					animatePaleta("show");
				}
			}
		});
	}
	
	protected void drawCircle(LatLng center, double radius) {
		
		map.addCircle(new CircleOptions()
		    .center(center)
		    .radius(radius)
		    .strokeColor(Color.GRAY)
		    .strokeWidth((float) 2.0));
	}
}
