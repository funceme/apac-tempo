package br.com.ceweather.fragments;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import br.com.ceweather.activity.ComponentsActivity;
import br.com.ceweather.activity.R;
import br.com.ceweather.adapters.MareAdapter;
import br.gov.funceme.database.dao.RequisicaoDAO;
import br.gov.funceme.database.model.Mare;
import br.gov.funceme.database.model.PortoLuaMares;
import br.gov.funceme.database.model.Requisicao;
import br.gov.funceme.database.tipo.RequisicaoTipo;
import br.gov.funceme.webservices.service.PortosLuaMaresService;
import br.gov.funceme.webservices.ws.AbstractServiceListener;
import br.gov.funceme.webservices.ws.WaitView;

public class MaresFragment extends Fragment {

	final static String ARG_POSITION = "position";

	int mCurrentPosition = -1;

	public ComponentsActivity activity;

	public String citySelected;
	public String harborSelected;

	private WaitView waiting;

	private View view;
	private TwoWayView tideDaysView;
	private MareAdapter tideAdapter;
	
	public Boolean hasHarbor;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, 
			Bundle savedInstanceState) {

		activity = (ComponentsActivity) getActivity();

		if (savedInstanceState != null) {
			mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
		}

		this.view = inflater.inflate(R.layout.tides, container, false);

		tideDaysView = (TwoWayView) this.view.findViewById(R.id.twowayTides);

		return this.view;
	}
	
	@Override
	public void onStart() {
		
		super.onStart();
		
		Requisicao last = null;
		
		if (RequisicaoDAO.getRequisicaoCount(activity.getDB(), RequisicaoTipo.MARE) > 0) {
		
			last = RequisicaoDAO.getUltimaRequisicao(activity.getDB(), RequisicaoTipo.MARE);
			
			render(parse(last.getJson()));
		}	
		
		if (last != null) {
			
			Calendar c = Calendar.getInstance(); 
			c.setTime(last.getDatahora());
			c.add(Calendar.MINUTE, Integer.parseInt(getString(R.string.TIMEOUT_MARE)));
			
			Date now = new Date();
			
			if(now.after(c.getTime()))
				doRequest();
			else 
				activity.requestCompleted();
			
		} else {
			doRequest();
		}
	}

	private void doRequest() {

		PortosLuaMaresService service = new PortosLuaMaresService(activity);

		waiting = new WaitView(activity, (ViewGroup)this.view);
		waiting.enable();

		AbstractServiceListener tideServiceListener = new AbstractServiceListener() {
			@Override
			public void OnComplete(Object params) {

				waiting.disable();
				
				Requisicao req = new Requisicao();
				
				req.setDatahora(new Timestamp(new Date().getTime()));
				req.setInfo(RequisicaoTipo.MARE);
				
				req.setJson((String) params); 
				
				RequisicaoDAO.create(activity.getDB(), req);
				
				render(parse((String) params));
				
				activity.requestCompleted();
			}
		};

		service.setServiceListener(tideServiceListener);
		
		int cod = 2304400; //cidade Default = fortaleza

		if (citySelected != null) {
			if (Integer.parseInt(citySelected) > 0) 
				cod = Integer.parseInt(citySelected);
		}
		
		service.getLuaMares(cod);
	}
	
	public List<PortoLuaMares> parse(String response) {
		
		List<PortoLuaMares> harborList = null;

		JSONObject jsonResponse = null;
		try {
			
			if(response == null)
				throw new Exception("Oops! Resposta vazia para a solicitação de portos.");
			
			jsonResponse = new JSONObject(response);
			
			harborList = new ArrayList<PortoLuaMares>();
			
			if (!jsonResponse.has("portoDia"))
				throw new Exception("Oops! Nenhuma informação retornada.");
			
			JSONArray listJSON = jsonResponse.getJSONArray("portoDia");
			
			for(int i = 0; i < listJSON.length(); i++) {
				
				JSONObject objJSON = listJSON.getJSONObject(i);
				
				if (!(objJSON.has("data") && objJSON.has("faseLua") && objJSON.has("valorMedio") && objJSON.has("mares")))
					continue;
				
				PortoLuaMares harborTidesMoon = new PortoLuaMares();
				
				String data = objJSON.getString("data");
				String[] parts = data.split("-");
				
				harborTidesMoon.setData(parts[2]+'/'+parts[1]+'/'+parts[0].substring(2, 4));
				harborTidesMoon.setFaseLua(objJSON.getString("faseLua"));
				harborTidesMoon.setValorMedio(objJSON.getString("valorMedio"));
				
				JSONArray maresJSON = objJSON.getJSONArray("mares");
				
				List<Mare> mares = new ArrayList<Mare>();
				
				for(int k = 0; k < maresJSON.length(); k++) {
					 
					JSONObject mareJSON = maresJSON.getJSONObject(k);
					
					if (!(mareJSON.has("hora") && mareJSON.has("valor")))
						continue;
					
					Mare mare = new Mare();
					
					mare.setHora(mareJSON.getString("hora").substring(0, 5)+'h');
					
					String[] parts2 = mareJSON.getString("valor").split(" ");
					
					mare.setValor(Float.parseFloat(parts2[0]));	
					
					mares.add(mare);
				}				
				harborTidesMoon.setMares(mares);
				
				harborList.add(harborTidesMoon);
			}
		} catch(Exception e)
		{
			harborList = null;
			Log.d("DEBUG", e.getMessage());
		}
		
		return harborList;
	}
	
	private void render(List<PortoLuaMares> list)
	{
		if(list != null)
		{		
			tideAdapter = new MareAdapter(activity, R.layout.tide_day, list);
			tideDaysView.setAdapter(tideAdapter);
		} else {

		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// Save the current view selection in case we need to recreate the fragment
		outState.putInt(ARG_POSITION, mCurrentPosition);
	}
}