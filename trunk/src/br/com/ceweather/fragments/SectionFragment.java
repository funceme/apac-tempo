package br.com.ceweather.fragments;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager.LayoutParams;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import br.com.ceweather.activity.R;

public class SectionFragment extends Fragment {

	private static Activity activity;
	
	public View view;
	private String title;
	private String image;
	private View viewOptions = null;
	
	public void setTitle(String title) {
		
		this.title = title;		
	}

	public void setImage(String image) {
		
		this.image = image;		
	}
	
	public void addView(View viewOptions) {
		
		this.viewOptions = viewOptions;
	}
	
	public Activity getAct () {
		
		return activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		if (activity==null)
			activity = getActivity();
		
		this.view = inflater.inflate(R.layout.section, container, false);
		
		RelativeLayout sectionLayout = (RelativeLayout) this.view.findViewById(R.id.sectionLayout);
		
		Typeface fontBold = Typeface.createFromAsset(this.getAct().getAssets(), "fonts/TitilliumWeb-Bold.ttf");
		
		TextView title = (TextView) this.view.findViewById(R.id.txtSectionTitle);
		title.setTypeface(fontBold);
		title.setTextSize(16);
		title.setText(this.title);
		
		ImageView image = (ImageView) this.view.findViewById(R.id.imageSection);
		
		try {
		
			int resId = getAct().getResources().getIdentifier(this.image, "drawable", getAct().getPackageName());
			image.setImageResource(resId);
		} catch (Exception e) {
			Log.d("ERROR", e.getMessage() + "TRACK: "+e.getStackTrace());
		}
		
		if (this.viewOptions != null) {
			
			RelativeLayout optionsLayout = new RelativeLayout(getAct());
			
			RelativeLayout.LayoutParams containerParams = new RelativeLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			
			optionsLayout.setLayoutParams(containerParams);
			optionsLayout.setGravity(Gravity.RIGHT);
			optionsLayout.addView(this.viewOptions);
			
			optionsLayout.setId(7328466);
			
			sectionLayout.addView(optionsLayout);
		}

		return this.view;
	}
}