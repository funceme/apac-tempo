package br.com.ceweather.fragments;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.ceweather.activity.AppDatabase;
import br.com.ceweather.activity.ComponentsActivity;
import br.com.ceweather.activity.R;
import br.com.ceweather.adapters.PrevisaoDiaAdapter;
import br.gov.funceme.database.dao.CidadeDAO;
import br.gov.funceme.database.dao.RequisicaoDAO;
import br.gov.funceme.database.model.Previsao;
import br.gov.funceme.database.model.PrevisaoCE;
import br.gov.funceme.database.model.PrevisaoDia;
import br.gov.funceme.database.model.Requisicao;
import br.gov.funceme.database.tipo.RequisicaoTipo;
import br.gov.funceme.webservices.service.PrevisaoService;
import br.gov.funceme.webservices.ws.AbstractServiceListener;
import br.gov.funceme.webservices.ws.WaitView;

import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

@SuppressLint("SimpleDateFormat")
public class PrevisaoDiaFragment extends Fragment {

	AppDatabase app;

	PrevisaoDiaAdapter mAdapter;

	ViewPager mPager;
	PageIndicator mIndicator;

	FragmentManager fm;

	private View view;

	public ComponentsActivity activity;
	public Context context;

	private WaitView waiting;
	public ViewGroup fragmentContainer;

	final static String ARG_POSITION = "position";	
	int mCurrentPosition = -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, 
			Bundle savedInstanceState) {

		activity = (ComponentsActivity) getActivity();

		app = new AppDatabase(activity);

		fragmentContainer = container;

		if (savedInstanceState != null) {
			mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
		}

		this.view = inflater.inflate(R.layout.forecast_day, container, false);
		
		return this.view;
	}
	
	@Override
	public void onStart() {
		
		super.onStart();
		
		Requisicao last = null;
		
		if (RequisicaoDAO.getRequisicaoCount(activity.getDB(), RequisicaoTipo.PREVISAO_DIA) > 0) {
		
			last = RequisicaoDAO.getUltimaRequisicao(activity.getDB(), RequisicaoTipo.PREVISAO_DIA);
			
			render(PrevisaoService.parse(last.getJson()));
		}	
		
		if (last != null) {
			
			Calendar c = Calendar.getInstance();
			c.setTime(last.getDatahora());
			c.add(Calendar.MINUTE, Integer.parseInt(getString(R.string.TIMEOUT_PREVISAO)));
			
			Date now = new Date();
			
			if(now.after(c.getTime()))
				doRequest();
			else 
				activity.requestCompleted();
			
		} else {
			doRequest();
		}
	}
	
	private void render(List<PrevisaoDia> list)
	{
		if(list != null)
		{	
			Collections.sort(list, new Comparator<PrevisaoDia>() {
				public int compare(PrevisaoDia o1, PrevisaoDia o2) {
					
					String date1 = (String)o1.getData();
					String date2 = (String)o2.getData();
					
					SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
					
					Date strDate1 = null;
					Date strDate2 = null;
					
					try {
						strDate1 = sdf.parse(date1);
						strDate2 = sdf.parse(date2);
					} catch (java.text.ParseException e) {
						e.printStackTrace();
					}
					
					if (o1.getData() == null || o2.getData() == null)
						return 0;
					
					return strDate1.compareTo(strDate2);
				}
			});
			
			PrevisaoDia primeiroDia = list.get(0);
			
			Calendar cal = Calendar.getInstance();
			int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
			
			if (Integer.parseInt(primeiroDia.getData().substring(0, 2)) != dayOfMonth)
				list.remove(0);
			
			mAdapter = new PrevisaoDiaAdapter(activity, list);
			
			mPager = (ViewPager) this.view.findViewById(R.id.pagerForecastDay);
			mPager.setAdapter(mAdapter);

			CirclePageIndicator indicator = (CirclePageIndicator) this.view.findViewById(R.id.indicatorForecastDay);

			indicator.setViewPager(mPager);
			indicator.setCentered(true);
			indicator.setPadding(20, 20, 20, 20);
			indicator.notifyDataSetChanged();
			indicator.setCurrentItem(0);

	        DisplayMetrics metrics = activity.getResources().getDisplayMetrics();
			
			indicator.setRadius((int) Math.ceil(metrics.xdpi*0.027));
	        
		} else {
			showErrorMessage(activity.getResources().getString(R.string.erro_carregamento));
		}
	}

	private void doRequest() {

		final PrevisaoService service = new PrevisaoService(activity);

		waiting = new WaitView(activity, (ViewGroup) this.view);

		waiting.enable();

		AbstractServiceListener listener = new AbstractServiceListener() {
			@Override
			public void OnComplete(Object params) {
				
				waiting.disable();
				
				Requisicao req = new Requisicao();
				
				req.setDatahora(new Timestamp(new Date().getTime()));
				req.setInfo(RequisicaoTipo.PREVISAO_DIA);
				
				req.setJson((String) params); 
				
				RequisicaoDAO.create(activity.getDB(), req);
				
				render(PrevisaoService.parse((String) params));
				
				activity.requestCompleted();
			}
		};

		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Date _dateStart = Calendar.getInstance().getTime();        

		String dateStart = df.format(_dateStart);

		Calendar c = Calendar.getInstance(); 
		c.setTime(_dateStart); 
		c.add(Calendar.DATE, 2); 
		Date _dateFinish = c.getTime();

		String dateFinish = df.format(_dateFinish);

		service.setServiceListener(listener);
		service.getForecast(CidadeDAO.getCidadeCarregada(app.db).getCodigo(), dateStart, dateFinish);
	}
	
	private void showErrorMessage(String message) {
		
		TextView errorMessage = new TextView(activity);
		
		errorMessage.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		errorMessage.setGravity(Gravity.CENTER_HORIZONTAL);
		errorMessage.setText(message);
		errorMessage.setTextColor(Color.WHITE);
		errorMessage.setPadding(0, 60, 0, 0);
		
		Typeface fontSemiBold = Typeface.createFromAsset(this.activity.getAssets(), "fonts/TitilliumWeb-SemiBold.ttf");
		
		errorMessage.setTypeface(fontSemiBold);
		errorMessage.setTextSize(14);
		
		ViewGroup container = (ViewGroup)this.fragmentContainer.getChildAt(0);
		container.addView(errorMessage);
	}
}