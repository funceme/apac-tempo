package br.com.ceweather.fragments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import br.com.ceweather.activity.AppDatabase;
import br.com.ceweather.activity.ComponentsActivity;
import br.com.ceweather.activity.R;
import br.gov.funceme.database.dao.CidadeDAO;
import br.gov.funceme.database.dao.RequisicaoDAO;
import br.gov.funceme.database.model.Cidade;
import br.gov.funceme.webservices.service.PrevisaoCidadeService;
import br.gov.funceme.webservices.ws.AbstractServiceListener;
import br.gov.funceme.webservices.ws.WaitView;

public class ListaCidadesFragment extends Fragment {

	private AppDatabase app;

	private View view;

	public Context context;

	private ListView listView;
	
	private List<Cidade> listaCidades;

	private SearchView search;

	public ViewGroup fragmentContainer;

	private ComponentsActivity componentsActivity;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {		

		this.fragmentContainer 	= container;
		this.componentsActivity = (ComponentsActivity)getActivity();
		this.view 				= inflater.inflate(R.layout.list_cities, container, false);
		this.listView 			= (ListView)this.view.findViewById(R.id.listCities);
		this.search 			= (SearchView)this.view.findViewById(R.id.search);
		this.app 				= new AppDatabase(getActivity());
		
		listaCidades = CidadeDAO.getCidades(app.getDB());
		
		CityListAdapter adapter = new CityListAdapter(getActivity());

		for (Cidade cidade : listaCidades) {
			
			adapter.add(new CidadeItem(cidade.getCodigo(), cidade.getNome()));
		}
		
		listView.setAdapter(adapter);

		int id = search.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
		TextView textView = (TextView) search.findViewById(id);
		textView.setTextColor(Color.BLACK);
		
		search.setQuery("", false);
		search.clearFocus();

		search.setOnQueryTextListener(new OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) {

				CityListAdapter adapter = new CityListAdapter(getActivity());

				for (Cidade cidade : listaCidades) {

					String name = cidade.getNome().toString();
					
					if (name.toLowerCase().contains(newText.trim().toLowerCase())) {
						adapter.add(new CidadeItem(cidade.getCodigo(), cidade.getNome()));
					}
				}
				
				listView.setAdapter(adapter);

				return true;
			}
		});

		return this.view;
	}

	private class CidadeItem {
		
		public String nome;
		public String codigo;
		
		public CidadeItem(String code, String city) {
			this.nome = city; 
			this.codigo = code;
		}
	} 

	public class CityListAdapter extends ArrayAdapter<CidadeItem> {

		public CityListAdapter(Context context) {
			super(context, 0);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.menu_cities_row, null);
			}

			final Button button = (Button) convertView.findViewById(R.id.btn_fav_citys);
			
			Cidade cidade = CidadeDAO.getCidade(app.getDB(), getItem(position).codigo);

			if (cidade.getFavorita()==1)
				button.setBackgroundResource(R.drawable.ic_star_on);
			else 
				button.setBackgroundResource(R.drawable.ic_star);

			button.setTag(getItem(position).codigo + "_" +getItem(position).nome);

			button.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					String[] separated = button.getTag().toString().split("_");
					String codigoCidade = separated[0]; 
					
					Cidade cidade = CidadeDAO.getCidade(app.getDB(), codigoCidade);

					if (cidade.getFavorita()==1) {
						CidadeDAO.desfavoritarCidade(app.getDB(), codigoCidade);
						button.setBackgroundResource(R.drawable.ic_star);
					} else {
						CidadeDAO.favoritarCidade(app.getDB(), codigoCidade);
						button.setBackgroundResource(R.drawable.ic_star_on);
					}
				}
			});

			final Button title = (Button) convertView.findViewById(R.id.btnCityRow);
			title.setText(getItem(position).nome);
			title.setTag(getItem(position).codigo);

			title.setOnClickListener(new Button.OnClickListener() {  
				public void onClick(View v)
				{
					RequisicaoDAO.truncate(app.getDB());
					
					CidadeDAO.atualizarCidadeCarregada(app.getDB(), title.getTag().toString());

					componentsActivity.reinit();
				}
			});

			return convertView;
		}
	}
}