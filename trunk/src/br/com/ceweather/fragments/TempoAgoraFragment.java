package br.com.ceweather.fragments;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import br.com.ceweather.activity.AppDatabase;
import br.com.ceweather.activity.ComponentsActivity;
import br.com.ceweather.activity.R;
import br.gov.funceme.database.dao.CidadeDAO;
import br.gov.funceme.database.dao.RequisicaoDAO;
import br.gov.funceme.database.model.Requisicao;
import br.gov.funceme.database.model.TempoAgora;
import br.gov.funceme.database.tipo.RequisicaoTipo;
import br.gov.funceme.webservices.service.TempoAgoraService;
import br.gov.funceme.webservices.ws.AbstractServiceListener;
import br.gov.funceme.webservices.ws.WaitView;

public class TempoAgoraFragment extends Fragment {

	AppDatabase app;
	
	final static String ARG_POSITION = "position";
	int mCurrentPosition = -1;
	
	public ComponentsActivity activity;
	public ViewGroup fragmentContainer;
	
	private WaitView waiting; 
	
	private View view;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		activity = (ComponentsActivity) getActivity();
		
		app = new AppDatabase(activity);
		
		fragmentContainer = container;
		
		if (savedInstanceState != null)
			mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);

		this.view = inflater.inflate(R.layout.tempo_agora, container, false);
		
		return this.view;
	}

	@Override
	public void onStart() {
		
		super.onStart();
		
		Requisicao last = null;
		
		if (RequisicaoDAO.getRequisicaoCount(activity.getDB(), RequisicaoTipo.TEMPO_AGORA) > 0) {
		
			last = RequisicaoDAO.getUltimaRequisicao(activity.getDB(), RequisicaoTipo.TEMPO_AGORA);
			
			render(new TempoAgora(last.getJson()));
		}	
		
		if (last != null) {
			
			Calendar c = Calendar.getInstance(); 
			c.setTime(last.getDatahora());
			c.add(Calendar.MINUTE, Integer.parseInt(getString(R.string.TIMEOUT_TEMPO_AGORA)));
			
			Date now = new Date();
			
			if(now.after(c.getTime()))
				doRequest();
			else 
				activity.requestCompleted();
			
		} else {
			doRequest();
		}
	}
	
	private void doRequest() {
		
		TempoAgoraService service = new TempoAgoraService(activity);
		
		waiting = new WaitView(activity, (ViewGroup) this.view);

		waiting.enable();

		AbstractServiceListener listener = new AbstractServiceListener() {
			@Override
			public void OnComplete(Object params) {
								
				waiting.disable();
				
				try {
				
					TempoAgora tempo_agora = new TempoAgora((String) params);
					
					if(tempo_agora != null)
					{
						String SI = getString(R.string.abrev_sem_informacao);
						
						if (tempo_agora.getTemperatura() != null && tempo_agora.getTemperatura().equals(SI)
								&& tempo_agora.getTemperaturaMax().equals(SI)
								&& tempo_agora.getTemperaturaMin().equals(SI)
								&& tempo_agora.getVentoVel().equals(SI)
								&& tempo_agora.getUmidade().equals(SI)
								&& tempo_agora.getUmidadeMax().equals(SI)
								&& tempo_agora.getUmidadeMin().equals(SI)){
							hideFragment();
							
						} else{
							
							Requisicao req = new Requisicao();
							
							req.setDatahora(new Timestamp(new Date().getTime()));
							req.setInfo(RequisicaoTipo.TEMPO_AGORA);
							
							req.setJson((String) params); 
							
							RequisicaoDAO.create(activity.getDB(), req);
							
							render(tempo_agora);
						}
					}
				} catch (Exception e) {
					//TODO Handle exception
				}
			}
		};

		service.setServiceListener(listener);
		service.getWeatherNowFromCity(CidadeDAO.getCidadeCarregada(app.getDB()).getCodigo());
	}

	public void render(TempoAgora tempo_agora){
		
		TextView txtDateToday 			= (TextView)view.findViewById(R.id.txtDateToday);
		TextView txtTemp 				= (TextView)view.findViewById(R.id.txtTemp);
		
		TextView txtRainLastHour 		= (TextView)view.findViewById(R.id.txtRainLastHour);
		TextView txtRainSinceMidnight 	= (TextView)view.findViewById(R.id.txtRainSinceMidnight);
		
		TextView lbWind 				= (TextView)view.findViewById(R.id.lbWeatherNowWind);
		TextView txtWind 				= (TextView)view.findViewById(R.id.txtWind);
		TextView txtWindMax				= (TextView)view.findViewById(R.id.txtWindMax);
		TextView txtWindMin				= (TextView)view.findViewById(R.id.txtWindMin);
		
		TextView lbHumidity 			= (TextView)view.findViewById(R.id.lbWeatherNowHumidity);
		TextView txtHumidity 			= (TextView)view.findViewById(R.id.txtHumidity);
		
		TextView lbPressure 			= (TextView)view.findViewById(R.id.lbWeatherNowPressure);
		TextView txtPressure 			= (TextView)view.findViewById(R.id.txtPressure);
		
		TextView lbRadiation 			= (TextView)view.findViewById(R.id.lbWeatherNowRadiation);
		TextView txtRadiation 			= (TextView)view.findViewById(R.id.txtRadiation);
		
		TextView lbMinimun 				= (TextView)view.findViewById(R.id.lbWeatherNowMinimas);
		TextView lbMaximun 				= (TextView)view.findViewById(R.id.lbWeatherNowMaximas);
		
		TextView lbTemperatureMaxMin	= (TextView)view.findViewById(R.id.lbWeatherNowTemperatura);
		TextView txtTempMin 			= (TextView)view.findViewById(R.id.txtTempMin);
		TextView txtTempMax 			= (TextView)view.findViewById(R.id.txtTempMax);
		
		TextView lbHumidityMaxMin		= (TextView)view.findViewById(R.id.lbWeatherNowUmidade);
		TextView txtHumidityMin 		= (TextView)view.findViewById(R.id.txtHumidityMin);
		TextView txtHumidityMax 		= (TextView)view.findViewById(R.id.txtHumidityMax);
		
		TextView lbRadiationMaxMin		= (TextView)view.findViewById(R.id.lbWeatherNowRadiacao);
		TextView txtRadMin 				= (TextView)view.findViewById(R.id.txtRadMin);
		TextView txtRadMax 				= (TextView)view.findViewById(R.id.txtRadMax);
		
		TextView lbPressureMaxMin		= (TextView)view.findViewById(R.id.lbWeatherNowPressao);
		TextView txtPressureMin 		= (TextView)view.findViewById(R.id.txtPressureMin);
		TextView txtPressureMax 		= (TextView)view.findViewById(R.id.txtPressureMax);
		
		DisplayMetrics metrics = activity.getResources().getDisplayMetrics();
		
		if (metrics.heightPixels<500) {
			txtTemp.setTextSize(50);
		} else if (metrics.heightPixels>500 && metrics.heightPixels<900) {
			txtTemp.setTextSize(60);
		}
		txtTemp.setPadding(0, -5, 0, 5);
		
		Typeface fontRegular = Typeface.createFromAsset(activity.getAssets(), "fonts/TitilliumWeb-Regular.ttf");
		Typeface fontSemiBold = Typeface.createFromAsset(activity.getAssets(), "fonts/TitilliumWeb-SemiBold.ttf");
		
		lbWind.setTypeface(fontRegular);
		lbHumidity.setTypeface(fontRegular);
		lbPressure.setTypeface(fontRegular);
		lbRadiation.setTypeface(fontRegular);
		lbMinimun.setTypeface(fontRegular);
		lbMaximun.setTypeface(fontRegular);
		lbTemperatureMaxMin.setTypeface(fontRegular);
		lbHumidityMaxMin.setTypeface(fontRegular);
		lbRadiationMaxMin.setTypeface(fontRegular);
		lbPressureMaxMin.setTypeface(fontRegular);
		
		txtTemp.setTypeface(fontSemiBold);
		txtDateToday.setTypeface(fontSemiBold);
		txtRainLastHour.setTypeface(fontSemiBold);
		txtRainSinceMidnight.setTypeface(fontSemiBold);
		txtWind.setTypeface(fontSemiBold);
		txtWindMax.setTypeface(fontSemiBold);
		txtWindMin.setTypeface(fontSemiBold);
		txtHumidity.setTypeface(fontSemiBold);
		txtPressure.setTypeface(fontSemiBold);
		txtRadiation.setTypeface(fontSemiBold);
		
		int max_color = activity.getResources().getColor(R.color.max_color);
		int min_color = activity.getResources().getColor(R.color.min_color);
		
		txtTempMin.setTextColor(min_color);
		txtTempMin.setTypeface(fontSemiBold);
		
		txtTempMax.setTextColor(max_color);
		txtTempMax.setTypeface(fontSemiBold);
		
		txtWindMax.setTextColor(max_color);
		txtWindMin.setTextColor(min_color);
		
		txtHumidityMin.setTextColor(min_color);
		txtHumidityMin.setTypeface(fontSemiBold);
		
		txtHumidityMax.setTextColor(max_color);
		txtHumidityMax.setTypeface(fontSemiBold);
		
		txtRadMin.setTextColor(min_color);
		txtRadMin.setTypeface(fontSemiBold);
		
		txtRadMax.setTextColor(max_color);
		txtRadMax.setTypeface(fontSemiBold);
		
		txtPressureMin.setTextColor(min_color);
		txtPressureMin.setTypeface(fontSemiBold); 
		
		txtPressureMax.setTextColor(max_color);
		txtPressureMax.setTypeface(fontSemiBold);
		

		txtDateToday.setText		(getString(R.string.label_atualizacao)+" "+tempo_agora.getAtualizacao());
		txtTemp.setText				(tempo_agora.getTemperatura());
		txtRainLastHour.setText		(getString(R.string.label_chuva_ultima_hora)+": " + tempo_agora.getChuva());
		txtRainSinceMidnight.setText(getString(R.string.label_chuva_desde_meia_noite)+": " + tempo_agora.getChuvaAcum());
		txtWind.setText				(tempo_agora.getVentoVel() + " " + tempo_agora.getVentoDir());
		txtWindMax.setText			(tempo_agora.getVentoVelMax());
		txtWindMin.setText			(tempo_agora.getVentoVelMin());
		txtHumidity.setText			(tempo_agora.getUmidade());
		txtPressure.setText			(tempo_agora.getPressao());
		txtRadiation.setText		(tempo_agora.getRad());
		txtTempMin.setText			(tempo_agora.getTemperaturaMin());
		txtTempMax.setText			(tempo_agora.getTemperaturaMax());
		txtHumidityMin.setText		(tempo_agora.getUmidadeMin());
		txtHumidityMax.setText		(tempo_agora.getUmidadeMax());
		txtRadMin.setText			(tempo_agora.getRadMin());
		txtRadMax.setText			(tempo_agora.getRadMax());
		txtPressureMin.setText		(tempo_agora.getPressaoMin());
		txtPressureMax.setText		(tempo_agora.getPressaoMax());	

	}
	
	public void hideFragment(){
		activity.fragmentManager = activity.getSupportFragmentManager();
		activity.fragmentTransaction = activity.fragmentManager.beginTransaction();
		activity.fragmentTransaction.remove(activity.sectionWeatherNow);
		activity.fragmentTransaction.remove(activity.weatherNow);
		activity.fragmentTransaction.commit();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		
		super.onSaveInstanceState(outState);
		
		outState.putInt(ARG_POSITION, mCurrentPosition);
	}
}