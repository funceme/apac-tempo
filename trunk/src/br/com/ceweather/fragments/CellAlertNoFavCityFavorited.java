package br.com.ceweather.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import br.com.ceweather.activity.ComponentsActivity;
import br.com.ceweather.activity.R;
import br.gov.funceme.database.dao.RequisicaoDAO;

@SuppressLint("ValidFragment") 
public class CellAlertNoFavCityFavorited extends ListFragment {
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.list, null);
	}
	
	public void onActivityCreated(Bundle savedInstanceState) {
		
		super.onActivityCreated(savedInstanceState);
		
		AlertList adapter = new AlertList(getActivity());
		adapter.add(new AlertItem(getString(R.string.aviso_nenhum_favorito), getString(R.string.menu_todas)));

		setListAdapter(adapter);
	}
	
	private class AlertItem {
		public String txtAlert; 
		public String txtButton;
		public AlertItem(String txtAlert, String txtButton) {
			this.txtAlert = txtAlert; 
			this.txtButton = txtButton;
		}
	}

	@SuppressLint("ResourceAsColor")
	public class AlertList extends ArrayAdapter<AlertItem> {

		public AlertList(Context context) {
			super(context, 0);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.alert_no_cities_favorited, null);
			}
			
			TextView txtAlert = (TextView) convertView.findViewById(R.id.txtAlertNoCityFavorited); 
			txtAlert.setText(getItem(position).txtAlert);
			
			Button btnAllCities = (Button)convertView.findViewById(R.id.btnTodasCidades);
			btnAllCities.setTextColor(R.color.black);
			btnAllCities.setText(getItem(position).txtButton);
			btnAllCities.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.menu_frame, new ListaCidadesFragment()).commit();
					
					ComponentsActivity act = new ComponentsActivity();
					
					act = (ComponentsActivity) getActivity();
					
					act.isLoadAlertNoFavCity = false;
					act.isLoadFavCitys = false;										
				}
			});
			
			return convertView;
		}
	}
}
