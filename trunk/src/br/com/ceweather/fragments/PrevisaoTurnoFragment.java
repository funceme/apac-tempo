package br.com.ceweather.fragments;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import br.com.ceweather.activity.AppDatabase;
import br.com.ceweather.activity.ComponentsActivity;
import br.com.ceweather.activity.R;
import br.com.ceweather.adapters.PrevisaoPeriodoAdapter;
import br.gov.funceme.database.dao.CidadeDAO;
import br.gov.funceme.database.dao.RequisicaoDAO;
import br.gov.funceme.database.model.Previsao;
import br.gov.funceme.database.model.PrevisaoDia;
import br.gov.funceme.database.model.Requisicao;
import br.gov.funceme.database.tipo.RequisicaoTipo;
import br.gov.funceme.webservices.service.PrevisaoService;
import br.gov.funceme.webservices.ws.AbstractServiceListener;
import br.gov.funceme.webservices.ws.WaitView;

import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

@SuppressLint({ "ValidFragment", "SimpleDateFormat" })
public class PrevisaoTurnoFragment extends Fragment {
	
	AppDatabase app;
	
	PrevisaoPeriodoAdapter mAdapter;
	
	ViewPager mPagerPeriod;
	PageIndicator mIndicatorPeriod;
	
	FragmentManager fm;
	
	private View view;
	
	public ComponentsActivity activity;
	public Context context;
	
	private WaitView waiting;
	public ViewGroup fragmentContainer;
	
	final static String ARG_POSITION = "position";	
	int mCurrentPosition = -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, 
			Bundle savedInstanceState) {
	
		activity = (ComponentsActivity) getActivity();
		
		app = new AppDatabase(activity);
		
		fragmentContainer = container;
		
		if (savedInstanceState != null) {
			mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
		}

		this.view = inflater.inflate(R.layout.forecast_period, container, false);
		
		Requisicao last = null;
		
		if (RequisicaoDAO.getRequisicaoCount(activity.getDB(), RequisicaoTipo.PREVISAO_DIA) > 0) {
		
			last = RequisicaoDAO.getUltimaRequisicao(activity.getDB(), RequisicaoTipo.PREVISAO_DIA);
			
			render(parse(last.getJson()));
		}	
		
		if (last != null) {
			
			Calendar c = Calendar.getInstance(); 
			c.setTime(last.getDatahora());
			c.add(Calendar.MINUTE, Integer.parseInt(getString(R.string.TIMEOUT_PREVISAO)));
			
			Date now = new Date();
			
			if(now.after(c.getTime()))
				doRequest();
			else 
				activity.requestCompleted();
			
		} else {
			doRequest();
		}

		return this.view;
	}

	private void doRequest() {
		
		PrevisaoService service = new PrevisaoService(activity);
		
		waiting = new WaitView(activity, (ViewGroup) this.view);
		
		waiting.enable();
		
		AbstractServiceListener previsaoServiceListener = new AbstractServiceListener() {
			@Override
			public void OnComplete(Object params) {
				
				waiting.disable();
				
				Requisicao req = new Requisicao();
				
				req.setDatahora(new Timestamp(new Date().getTime()));
				req.setInfo(RequisicaoTipo.PREVISAO_DIA);
				
				req.setJson((String) params); 
				
				RequisicaoDAO.create(activity.getDB(), req);
				
				render(parse((String) params));
								
				activity.requestCompleted();
			}
		};
		
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Date _dateStart = Calendar.getInstance().getTime();        

		String dateStart = df.format(_dateStart);
		
		Calendar c = Calendar.getInstance(); 
		c.setTime(_dateStart); 
		c.add(Calendar.DATE, 2);
		Date _dateFinish = c.getTime();
		
		String dateFinish = df.format(_dateFinish);

		service.setServiceListener(previsaoServiceListener);
		service.getForecast(CidadeDAO.getCidadeCarregada(app.db).getCodigo(), dateStart, dateFinish);
	}
	
	private void render(List<PrevisaoDia> list)
	{
		if(list != null)
		{
			Collections.sort(list, new Comparator<PrevisaoDia>() {
				@SuppressLint("SimpleDateFormat")
				public int compare(PrevisaoDia o1, PrevisaoDia o2) {
					
					String date1 = (String)o1.getData();
					String date2 = (String)o2.getData();
					
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					
					Date strDate1 = null;
					Date strDate2 = null;
					
					try {
						strDate1 = sdf.parse(date1);
						strDate2 = sdf.parse(date2);
					} catch (java.text.ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if (o1.getData() == null || o2.getData() == null)
						return 0;
					
					return strDate1.compareTo(strDate2);
				}
			});
			
			PrevisaoDia primeiroDia = list.get(0);
			
			Calendar cal = Calendar.getInstance();
			int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
			
			if (Integer.parseInt(primeiroDia.getData().substring(0, 2)) != dayOfMonth)
				list.remove(0);
			
			mAdapter = new PrevisaoPeriodoAdapter(activity, list);
	
	        mPagerPeriod = (ViewPager) this.view.findViewById(R.id.pager);
	        mPagerPeriod.setAdapter(mAdapter);
	
	        CirclePageIndicator indicator = (CirclePageIndicator) this.view.findViewById(R.id.indicator);
	        
	        indicator.setViewPager(mPagerPeriod);
	        indicator.setCentered(true);
	        indicator.setPadding(20, 20, 20, 20);
	        indicator.notifyDataSetChanged();
	        indicator.setCurrentItem(0); 
	        
	        DisplayMetrics metrics = activity.getResources().getDisplayMetrics();
					
			indicator.setRadius((int) Math.ceil(metrics.xdpi*0.027));
	        
		} else {
			showErrorMessage(activity.getResources().getString(R.string.erro_carregamento));
		}
	}
	
	public List<PrevisaoDia> parse(String response) {
		
		List<PrevisaoDia> forecastDays = null;

		JSONObject jsonResponse = null;
		try {
			
			if(response == null)
				throw new Exception("Oops! Resposta vazia para a solicitação de pevisão.");
			
			jsonResponse = new JSONObject(response);
			
			forecastDays = new ArrayList<PrevisaoDia>();
			
			Iterator<?> keys = jsonResponse.keys();
			
			while( keys.hasNext() ){
				
	            String key = (String)keys.next();
	            
	            if( jsonResponse.get(key) instanceof JSONObject ){
	            	
	            	JSONObject forecastDayJSON = (JSONObject) jsonResponse.get(key);
	            	
	            	Previsao forecastDia 		= null;
	            	Previsao forecastManha 		= null;
	            	Previsao forecastTarde 		= null;
	            	Previsao forecastNoite 		= null;
	            	Previsao forecastMadrugada 	= null;
	            	
	            	if (forecastDayJSON.has("dia")) {
	            	
		            	JSONObject forecastDiaJSON = (JSONObject) forecastDayJSON.getJSONObject("dia");
		            	
		            	forecastDia = new Previsao();
		            	
		            	if (forecastDiaJSON.has("codigo")) 					forecastDia.setCodigo(forecastDiaJSON.getString("codigo"));
		            	if (forecastDiaJSON.has("rodadaIni")) 				forecastDia.setRodadaIni(forecastDiaJSON.getString("rodadaIni"));
		            	if (forecastDiaJSON.has("rodadaFim")) 				forecastDia.setRodadaFim(forecastDiaJSON.getString("rodadaFim"));
		            	if (forecastDiaJSON.has("modelo")) 					forecastDia.setModelo(forecastDiaJSON.getString("modelo"));
		            	if (forecastDiaJSON.has("tipoArea")) 				forecastDia.setTipoArea(forecastDiaJSON.getString("tipoArea"));
		            	if (forecastDiaJSON.has("area")) 					forecastDia.setArea(forecastDiaJSON.getString("area"));
		            	if (forecastDiaJSON.has("dataIni")) 				forecastDia.setDataIni(forecastDiaJSON.getString("dataIni"));
		            	if (forecastDiaJSON.has("dataFim")) 				forecastDia.setDataFim(forecastDiaJSON.getString("dataFim"));
		            	if (forecastDiaJSON.has("temperaturaMax")) 			forecastDia.setTemperaturaMax(forecastDiaJSON.getString("temperaturaMax"));
		            	if (forecastDiaJSON.has("temperaturaMin")) 			forecastDia.setTemperaturaMin(forecastDiaJSON.getString("temperaturaMin"));
		            	if (forecastDiaJSON.has("precipitacaoAcumulada")) 	forecastDia.setPrecipitacaoAcumulada(forecastDiaJSON.getString("precipitacaoAcumulada"));
		            	if (forecastDiaJSON.has("precipitacaoMin")) 		forecastDia.setPrecipitacaoMin(forecastDiaJSON.getString("precipitacaoMin"));
		            	if (forecastDiaJSON.has("precipitacaoMax")) 		forecastDia.setPrecipitacaoMax(forecastDiaJSON.getString("precipitacaoMax"));
		            	if (forecastDiaJSON.has("ventoVelMin")) 			forecastDia.setVentoVelMin(forecastDiaJSON.getString("ventoVelMin"));
		            	if (forecastDiaJSON.has("ventoVelMax")) 			forecastDia.setVentoVelMax(forecastDiaJSON.getString("ventoVelMax"));
		            	if (forecastDiaJSON.has("ventoDirecao")) 			forecastDia.setVentoDirecao(forecastDiaJSON.getString("ventoDirecao"));
		            	if (forecastDiaJSON.has("coberturaNuvensMin")) 		forecastDia.setCoberturaNuvensMin(forecastDiaJSON.getString("coberturaNuvensMin"));
		            	if (forecastDiaJSON.has("coberturaNuvensMax")) 		forecastDia.setCoberturaNuvensMax(forecastDiaJSON.getString("coberturaNuvensMax"));
		            	if (forecastDiaJSON.has("umidadeRelativaMin")) 		forecastDia.setUmidadeRelativaMin(forecastDiaJSON.getString("umidadeRelativaMin"));
		            	if (forecastDiaJSON.has("umidadeRelativaMax")) 		forecastDia.setUmidadeRelativaMax(forecastDiaJSON.getString("umidadeRelativaMax"));
		            	if (forecastDiaJSON.has("radiacaoUv")) 				forecastDia.setRadiacaoUv(forecastDiaJSON.getString("radiacaoUv"));
						
		            	if (forecastDiaJSON.has("descricao")) {
							
		            		JSONObject descricaoDia = forecastDiaJSON.getJSONObject("descricao");
			            	
		            		if (descricaoDia.has("codigo") && descricaoDia.has("descricao") && descricaoDia.has("img")) {
		            			forecastDia.setImgCodigo(descricaoDia.getString("codigo"));
		            			forecastDia.setDescricao(descricaoDia.getString("descricao"));
		            			forecastDia.setImg(descricaoDia.getString("img"));
		            		}
		            	}
	            	}
	            	
	            	if (forecastDayJSON.has("manha")) {
	            	
						JSONObject forecastManhaJSON = (JSONObject) forecastDayJSON.getJSONObject("manha");
		            	
		            	forecastManha = new Previsao();
		            	
		            	if (forecastManhaJSON.has("codigo")) 				forecastManha.setCodigo(forecastManhaJSON.getString("codigo"));
		            	if (forecastManhaJSON.has("rodadaIni")) 			forecastManha.setRodadaIni(forecastManhaJSON.getString("rodadaIni"));
		            	if (forecastManhaJSON.has("rodadaFim")) 			forecastManha.setRodadaFim(forecastManhaJSON.getString("rodadaFim"));
		            	if (forecastManhaJSON.has("modelo")) 				forecastManha.setModelo(forecastManhaJSON.getString("modelo"));
		            	if (forecastManhaJSON.has("tipoArea")) 				forecastManha.setTipoArea(forecastManhaJSON.getString("tipoArea"));
		            	if (forecastManhaJSON.has("area")) 					forecastManha.setArea(forecastManhaJSON.getString("area"));
		            	if (forecastManhaJSON.has("dataIni")) 				forecastManha.setDataIni(forecastManhaJSON.getString("dataIni"));
		            	if (forecastManhaJSON.has("dataFim")) 				forecastManha.setDataFim(forecastManhaJSON.getString("dataFim"));
		            	if (forecastManhaJSON.has("temperaturaMax")) 		forecastManha.setTemperaturaMax(forecastManhaJSON.getString("temperaturaMax"));
		            	if (forecastManhaJSON.has("temperaturaMin")) 		forecastManha.setTemperaturaMin(forecastManhaJSON.getString("temperaturaMin"));
		            	if (forecastManhaJSON.has("precipitacaoAcumulada")) forecastManha.setPrecipitacaoAcumulada(forecastManhaJSON.getString("precipitacaoAcumulada"));
		            	if (forecastManhaJSON.has("precipitacaoMin")) 		forecastManha.setPrecipitacaoMin(forecastManhaJSON.getString("precipitacaoMin"));
		            	if (forecastManhaJSON.has("precipitacaoMax")) 		forecastManha.setPrecipitacaoMax(forecastManhaJSON.getString("precipitacaoMax"));
		            	if (forecastManhaJSON.has("ventoVelMin"))			forecastManha.setVentoVelMin(forecastManhaJSON.getString("ventoVelMin"));
		            	if (forecastManhaJSON.has("ventoVelMax")) 			forecastManha.setVentoVelMax(forecastManhaJSON.getString("ventoVelMax"));
		            	if (forecastManhaJSON.has("ventoDirecao")) 			forecastManha.setVentoDirecao(forecastManhaJSON.getString("ventoDirecao"));
		            	if (forecastManhaJSON.has("coberturaNuvensMin")) 	forecastManha.setCoberturaNuvensMin(forecastManhaJSON.getString("coberturaNuvensMin"));
		            	if (forecastManhaJSON.has("coberturaNuvensMax")) 	forecastManha.setCoberturaNuvensMax(forecastManhaJSON.getString("coberturaNuvensMax"));
		            	if (forecastManhaJSON.has("umidadeRelativaMin")) 	forecastManha.setUmidadeRelativaMin(forecastManhaJSON.getString("umidadeRelativaMin"));
		            	if (forecastManhaJSON.has("umidadeRelativaMax")) 	forecastManha.setUmidadeRelativaMax(forecastManhaJSON.getString("umidadeRelativaMax"));
		            	if (forecastManhaJSON.has("radiacaoUv")) 			forecastManha.setRadiacaoUv(forecastManhaJSON.getString("radiacaoUv"));
		            	
		            	
		            	if (forecastManhaJSON.has("descricao")) {
			            	
		            		JSONObject descricaoManha = forecastManhaJSON.getJSONObject("descricao");
			            	
		            		if (descricaoManha.has("codigo") && descricaoManha.has("descricao") && descricaoManha.has("img")) {
		            			forecastManha.setImgCodigo(descricaoManha.getString("codigo"));
		            			forecastManha.setDescricao(descricaoManha.getString("descricao"));
		            			forecastManha.setImg(descricaoManha.getString("img"));
		            		}
		            	}
	            	}
	            	
	            	if (forecastDayJSON.has("tarde")) {
	            	
		            	JSONObject forecastTardeJSON = (JSONObject) forecastDayJSON.getJSONObject("tarde");
		            	
		            	forecastTarde = new Previsao();
		            	
		            	if (forecastTardeJSON.has("codigo")) 				forecastTarde.setCodigo(forecastTardeJSON.getString("codigo"));
		            	if (forecastTardeJSON.has("rodadaIni"))				forecastTarde.setRodadaIni(forecastTardeJSON.getString("rodadaIni"));
		            	if (forecastTardeJSON.has("rodadaFim"))				forecastTarde.setRodadaFim(forecastTardeJSON.getString("rodadaFim"));
		            	if (forecastTardeJSON.has("modelo"))				forecastTarde.setModelo(forecastTardeJSON.getString("modelo"));
		            	if (forecastTardeJSON.has("tipoArea"))				forecastTarde.setTipoArea(forecastTardeJSON.getString("tipoArea"));
		            	if (forecastTardeJSON.has("area"))					forecastTarde.setArea(forecastTardeJSON.getString("area"));
		            	if (forecastTardeJSON.has("dataIni"))				forecastTarde.setDataIni(forecastTardeJSON.getString("dataIni"));
		            	if (forecastTardeJSON.has("dataFim"))				forecastTarde.setDataFim(forecastTardeJSON.getString("dataFim"));
		            	if (forecastTardeJSON.has("temperaturaMax"))		forecastTarde.setTemperaturaMax(forecastTardeJSON.getString("temperaturaMax"));
		            	if (forecastTardeJSON.has("temperaturaMin"))		forecastTarde.setTemperaturaMin(forecastTardeJSON.getString("temperaturaMin"));
		            	if (forecastTardeJSON.has("precipitacaoAcumulada"))	forecastTarde.setPrecipitacaoAcumulada(forecastTardeJSON.getString("precipitacaoAcumulada"));
		            	if (forecastTardeJSON.has("precipitacaoMin"))		forecastTarde.setPrecipitacaoMin(forecastTardeJSON.getString("precipitacaoMin"));
		            	if (forecastTardeJSON.has("precipitacaoMax"))		forecastTarde.setPrecipitacaoMax(forecastTardeJSON.getString("precipitacaoMax"));
		            	if (forecastTardeJSON.has("ventoVelMin"))			forecastTarde.setVentoVelMin(forecastTardeJSON.getString("ventoVelMin"));
		            	if (forecastTardeJSON.has("ventoVelMax"))			forecastTarde.setVentoVelMax(forecastTardeJSON.getString("ventoVelMax"));
		            	if (forecastTardeJSON.has("ventoDirecao"))			forecastTarde.setVentoDirecao(forecastTardeJSON.getString("ventoDirecao"));
		            	if (forecastTardeJSON.has("coberturaNuvensMin"))	forecastTarde.setCoberturaNuvensMin(forecastTardeJSON.getString("coberturaNuvensMin"));
		            	if (forecastTardeJSON.has("coberturaNuvensMax"))	forecastTarde.setCoberturaNuvensMax(forecastTardeJSON.getString("coberturaNuvensMax"));
		            	if (forecastTardeJSON.has("umidadeRelativaMin"))	forecastTarde.setUmidadeRelativaMin(forecastTardeJSON.getString("umidadeRelativaMin"));
		            	if (forecastTardeJSON.has("umidadeRelativaMax"))	forecastTarde.setUmidadeRelativaMax(forecastTardeJSON.getString("umidadeRelativaMax"));
		            	if (forecastTardeJSON.has("radiacaoUv"))			forecastTarde.setRadiacaoUv(forecastTardeJSON.getString("radiacaoUv"));
		            	
		            	if (forecastTardeJSON.has("descricao")) {
			            	
		            		JSONObject descricaoTarde = forecastTardeJSON.getJSONObject("descricao");
			            	
		            		if (descricaoTarde.has("codigo") && descricaoTarde.has("descricao") && descricaoTarde.has("img")) {
		            			forecastTarde.setImgCodigo(descricaoTarde.getString("codigo"));
		            			forecastTarde.setDescricao(descricaoTarde.getString("descricao"));
		            			forecastTarde.setImg(descricaoTarde.getString("img"));
		            		}
		            	}
	            	}
	            	
	            	if (forecastDayJSON.has("noite")) { 
	            		
		            	JSONObject forecastNoiteJSON = (JSONObject) forecastDayJSON.getJSONObject("noite");
		            	
		            	forecastNoite = new Previsao();
		            	
		            	if (forecastNoiteJSON.has("codigo")) 				forecastNoite.setCodigo(forecastNoiteJSON.getString("codigo"));
		            	if (forecastNoiteJSON.has("rodadaIni")) 			forecastNoite.setRodadaIni(forecastNoiteJSON.getString("rodadaIni"));
		            	if (forecastNoiteJSON.has("rodadaFim")) 			forecastNoite.setRodadaFim(forecastNoiteJSON.getString("rodadaFim"));
		            	if (forecastNoiteJSON.has("modelo")) 				forecastNoite.setModelo(forecastNoiteJSON.getString("modelo"));
		            	if (forecastNoiteJSON.has("tipoArea")) 				forecastNoite.setTipoArea(forecastNoiteJSON.getString("tipoArea"));
		            	if (forecastNoiteJSON.has("area")) 					forecastNoite.setArea(forecastNoiteJSON.getString("area"));
		            	if (forecastNoiteJSON.has("dataIni")) 				forecastNoite.setDataIni(forecastNoiteJSON.getString("dataIni"));
		            	if (forecastNoiteJSON.has("dataFim")) 				forecastNoite.setDataFim(forecastNoiteJSON.getString("dataFim"));
		            	if (forecastNoiteJSON.has("temperaturaMax")) 		forecastNoite.setTemperaturaMax(forecastNoiteJSON.getString("temperaturaMax"));
		            	if (forecastNoiteJSON.has("temperaturaMin")) 		forecastNoite.setTemperaturaMin(forecastNoiteJSON.getString("temperaturaMin"));
		            	if (forecastNoiteJSON.has("precipitacaoAcumulada")) forecastNoite.setPrecipitacaoAcumulada(forecastNoiteJSON.getString("precipitacaoAcumulada"));
		            	if (forecastNoiteJSON.has("precipitacaoMin")) 		forecastNoite.setPrecipitacaoMin(forecastNoiteJSON.getString("precipitacaoMin"));
		            	if (forecastNoiteJSON.has("precipitacaoMax")) 		forecastNoite.setPrecipitacaoMax(forecastNoiteJSON.getString("precipitacaoMax"));
		            	if (forecastNoiteJSON.has("ventoVelMin")) 			forecastNoite.setVentoVelMin(forecastNoiteJSON.getString("ventoVelMin"));
		            	if (forecastNoiteJSON.has("ventoVelMax")) 			forecastNoite.setVentoVelMax(forecastNoiteJSON.getString("ventoVelMax"));
		            	if (forecastNoiteJSON.has("ventoDirecao")) 			forecastNoite.setVentoDirecao(forecastNoiteJSON.getString("ventoDirecao"));
		            	if (forecastNoiteJSON.has("coberturaNuvensMin"))	forecastNoite.setCoberturaNuvensMin(forecastNoiteJSON.getString("coberturaNuvensMin"));
		            	if (forecastNoiteJSON.has("coberturaNuvensMax")) 	forecastNoite.setCoberturaNuvensMax(forecastNoiteJSON.getString("coberturaNuvensMax"));
		            	if (forecastNoiteJSON.has("umidadeRelativaMin")) 	forecastNoite.setUmidadeRelativaMin(forecastNoiteJSON.getString("umidadeRelativaMin"));
		            	if (forecastNoiteJSON.has("umidadeRelativaMax")) 	forecastNoite.setUmidadeRelativaMax(forecastNoiteJSON.getString("umidadeRelativaMax"));
		            	if (forecastNoiteJSON.has("radiacaoUv")) 			forecastNoite.setRadiacaoUv(forecastNoiteJSON.getString("radiacaoUv"));
		            	
		            	if (forecastNoiteJSON.has("descricao")) {
		            		
			            	JSONObject descricaoNoite = forecastNoiteJSON.getJSONObject("descricao");
			            	
			            	if (descricaoNoite.has("codigo") && descricaoNoite.has("descricao") && descricaoNoite.has("img")) {
			            		forecastNoite.setImgCodigo(descricaoNoite.getString("codigo"));
			            		forecastNoite.setDescricao(descricaoNoite.getString("descricao"));
			            		forecastNoite.setImg(descricaoNoite.getString("img"));
			            	}
		            	}
	            	}
	            	
	            	if (forecastDayJSON.has("madrugada")) { 
	            		
		            	JSONObject forecastMadrugadaJSON = (JSONObject) forecastDayJSON.getJSONObject("madrugada");
		            	
		            	forecastMadrugada = new Previsao();
		            	
		            	if (forecastMadrugadaJSON.has("codigo"))				forecastMadrugada.setCodigo(forecastMadrugadaJSON.getString("codigo"));
		            	if (forecastMadrugadaJSON.has("rodadaIni"))				forecastMadrugada.setRodadaIni(forecastMadrugadaJSON.getString("rodadaIni"));
		            	if (forecastMadrugadaJSON.has("rodadaFim"))				forecastMadrugada.setRodadaFim(forecastMadrugadaJSON.getString("rodadaFim"));
		            	if (forecastMadrugadaJSON.has("modelo"))				forecastMadrugada.setModelo(forecastMadrugadaJSON.getString("modelo"));
		            	if (forecastMadrugadaJSON.has("tipoArea"))				forecastMadrugada.setTipoArea(forecastMadrugadaJSON.getString("tipoArea"));
		            	if (forecastMadrugadaJSON.has("area"))					forecastMadrugada.setArea(forecastMadrugadaJSON.getString("area"));
		            	if (forecastMadrugadaJSON.has("dataIni"))				forecastMadrugada.setDataIni(forecastMadrugadaJSON.getString("dataIni"));
		            	if (forecastMadrugadaJSON.has("dataFim"))				forecastMadrugada.setDataFim(forecastMadrugadaJSON.getString("dataFim"));
		            	if (forecastMadrugadaJSON.has("temperaturaMax"))		forecastMadrugada.setTemperaturaMax(forecastMadrugadaJSON.getString("temperaturaMax"));
		            	if (forecastMadrugadaJSON.has("temperaturaMin"))		forecastMadrugada.setTemperaturaMin(forecastMadrugadaJSON.getString("temperaturaMin"));
		            	if (forecastMadrugadaJSON.has("precipitacaoAcumulada"))	forecastMadrugada.setPrecipitacaoAcumulada(forecastMadrugadaJSON.getString("precipitacaoAcumulada"));
		            	if (forecastMadrugadaJSON.has("precipitacaoMin"))		forecastMadrugada.setPrecipitacaoMin(forecastMadrugadaJSON.getString("precipitacaoMin"));
		            	if (forecastMadrugadaJSON.has("precipitacaoMax"))		forecastMadrugada.setPrecipitacaoMax(forecastMadrugadaJSON.getString("precipitacaoMax"));
		            	if (forecastMadrugadaJSON.has("ventoVelMin"))			forecastMadrugada.setVentoVelMin(forecastMadrugadaJSON.getString("ventoVelMin"));
		            	if (forecastMadrugadaJSON.has("ventoVelMax"))			forecastMadrugada.setVentoVelMax(forecastMadrugadaJSON.getString("ventoVelMax"));
		            	if (forecastMadrugadaJSON.has("ventoDirecao"))			forecastMadrugada.setVentoDirecao(forecastMadrugadaJSON.getString("ventoDirecao"));
		            	if (forecastMadrugadaJSON.has("coberturaNuvensMin"))	forecastMadrugada.setCoberturaNuvensMin(forecastMadrugadaJSON.getString("coberturaNuvensMin"));
		            	if (forecastMadrugadaJSON.has("coberturaNuvensMax"))	forecastMadrugada.setCoberturaNuvensMax(forecastMadrugadaJSON.getString("coberturaNuvensMax"));
		            	if (forecastMadrugadaJSON.has("umidadeRelativaMin"))	forecastMadrugada.setUmidadeRelativaMin(forecastMadrugadaJSON.getString("umidadeRelativaMin"));
		            	if (forecastMadrugadaJSON.has("umidadeRelativaMax"))	forecastMadrugada.setUmidadeRelativaMax(forecastMadrugadaJSON.getString("umidadeRelativaMax"));
		            	if (forecastMadrugadaJSON.has("radiacaoUv"))			forecastMadrugada.setRadiacaoUv(forecastMadrugadaJSON.getString("radiacaoUv"));
		            	
		            	if (forecastMadrugadaJSON.has("descricao")) {
			            	JSONObject descricaoMadrugada = forecastMadrugadaJSON.getJSONObject("descricao");
			            	
			            	if (descricaoMadrugada.has("codigo") && descricaoMadrugada.has("descricao") && descricaoMadrugada.has("img")) {
			            		forecastMadrugada.setImgCodigo(descricaoMadrugada.getString("codigo"));
			            		forecastMadrugada.setDescricao(descricaoMadrugada.getString("descricao"));
			            		forecastMadrugada.setImg(descricaoMadrugada.getString("img"));
			            	}
		            	}
	            	}
	            	
	            	PrevisaoDia forecastDay = new PrevisaoDia();
	            	
	            	String[] keyParts = key.split("-");
	            	
	            	if(keyParts.length == 3)
	            		forecastDay.setData(keyParts[2]+"/"+keyParts[1]+"/"+keyParts[0]);
	            	else 
	            		forecastDay.setData(key);
	            	
	            	forecastDay.setDia(forecastDia);
	            	forecastDay.setManha(forecastManha);
	            	forecastDay.setTarde(forecastTarde);
	            	forecastDay.setNoite(forecastNoite);
	            	forecastDay.setMadrugada(forecastMadrugada);
	            	
	            	forecastDays.add(forecastDay);
	            }
	        }			
		} catch(Exception e)
		{
			forecastDays = null;
			Log.d("DEBUG", e.getMessage());
		}
		
		return forecastDays;
	}
	
	private void showErrorMessage(String message) {
		
		TextView errorMessage = new TextView(activity);
		
		errorMessage.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		errorMessage.setGravity(Gravity.CENTER_HORIZONTAL);
		errorMessage.setText(message);
		errorMessage.setTextColor(Color.WHITE);
		errorMessage.setPadding(0, 60, 0, 0);
		
		Typeface fontSemiBold = Typeface.createFromAsset(this.activity.getAssets(), "fonts/TitilliumWeb-SemiBold.ttf");
		
		errorMessage.setTypeface(fontSemiBold);
		errorMessage.setTextSize(14);
		
		ViewGroup container = (ViewGroup)this.fragmentContainer.getChildAt(0);
		container.addView(errorMessage);
	}
}
