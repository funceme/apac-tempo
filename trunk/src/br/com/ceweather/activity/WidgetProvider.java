package br.com.ceweather.activity;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import br.gov.funceme.database.dao.CidadeDAO;
import br.gov.funceme.database.dao.RequisicaoDAO;
import br.gov.funceme.database.model.PrevisaoDia;
import br.gov.funceme.database.model.Requisicao;
import br.gov.funceme.database.model.TempoAgora;
import br.gov.funceme.database.tipo.RequisicaoTipo;
import br.gov.funceme.webservices.service.PrevisaoService;
import br.gov.funceme.webservices.service.TempoAgoraService;
import br.gov.funceme.webservices.ws.AbstractServiceListener;

public class WidgetProvider extends AppWidgetProvider{

	public AppWidgetManager widgetManager = null;
	public Context ctx;

	public int widgetId;

	public AppDatabase app;

	public TempoAgora tempoAgora;
	public List<PrevisaoDia> previsaoDiaList;


	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds )
	{
		ComponentName thisWidget = new ComponentName(context, WidgetProvider.class);

		int[] allWidgetInstancesIds = appWidgetManager.getAppWidgetIds(thisWidget);
		for (int _widgetId : allWidgetInstancesIds) {
			this.widgetId = _widgetId;
		}

		this.widgetManager = appWidgetManager;

		this.ctx = context;

		app = new AppDatabase(this.ctx);
		
		tempoAgora();
	}
	
	public void previsao() {
		
		Requisicao lastPrevisao = null;
		
		if (RequisicaoDAO.getRequisicaoCount(app.getDB(), RequisicaoTipo.PREVISAO_DIA) > 0) {
			lastPrevisao = RequisicaoDAO.getUltimaRequisicao(app.getDB(), RequisicaoTipo.PREVISAO_DIA);
			previsaoDiaList = PrevisaoService.parse(lastPrevisao.getJson());
		}
		
		if (lastPrevisao != null) {
			
			Calendar c = Calendar.getInstance(); 
			c.setTime(lastPrevisao.getDatahora());
			c.add(Calendar.MINUTE, Integer.parseInt(ctx.getString(R.string.TIMEOUT_PREVISAO)));
			
			Date now = new Date();
			
			if(now.after(c.getTime()))
				doRequestPrevisao();
			else 
				buildWidgetPrevisao();
			
		} else {
			doRequestPrevisao();
		}
	}
	
	public void tempoAgora()
	{
		Requisicao lastTempoAgora = null;
		
		if (RequisicaoDAO.getRequisicaoCount(app.getDB(), RequisicaoTipo.TEMPO_AGORA) > 0) {
			lastTempoAgora = RequisicaoDAO.getUltimaRequisicao(app.getDB(), RequisicaoTipo.TEMPO_AGORA);
			tempoAgora = new TempoAgora((String) lastTempoAgora.getJson());
		}
		
		if (lastTempoAgora != null) {
			
			Calendar c = Calendar.getInstance(); 
			c.setTime(lastTempoAgora.getDatahora());
			c.add(Calendar.MINUTE, Integer.parseInt(ctx.getString(R.string.TIMEOUT_PREVISAO)));
			
			Date now = new Date();
			
			if(now.after(c.getTime()))
				doRequestTempoAgora();
			else 
				buildWidgetTempoAgora();
			
		} else {
			doRequestTempoAgora();
		}
	}

	public void doRequestTempoAgora(){
		
		TempoAgoraService service = new TempoAgoraService(ctx);

		AbstractServiceListener listener = new AbstractServiceListener() {
			@Override
			public void OnComplete(Object params) {
				
				Requisicao req = new Requisicao();
				
				req.setDatahora(new Timestamp(new Date().getTime()));
				req.setInfo(RequisicaoTipo.TEMPO_AGORA);
				
				req.setJson((String) params); 
				
				RequisicaoDAO.create(app.getDB(), req);
				
				tempoAgora = new TempoAgora((String) params);
				
				buildWidgetTempoAgora();
			}
		};
		
		service.setServiceListener(listener);
		service.getWeatherNowFromCity(CidadeDAO.getCidadeCarregada(app.db).getCodigo());
	}

	public void doRequestPrevisao(){

		PrevisaoService service = new PrevisaoService(ctx);	
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Date _dateStart = Calendar.getInstance().getTime();        

		String dateStart = df.format(_dateStart);

		Calendar c = Calendar.getInstance(); 
		c.setTime(_dateStart); 
		c.add(Calendar.DATE, 2);
		Date _dateFinish = c.getTime();

		String dateFinish = df.format(_dateFinish);

		AbstractServiceListener previsaoServiceListener = new AbstractServiceListener() {
			@Override
			public void OnComplete(Object params) {

				Requisicao req = new Requisicao();
				
				req.setDatahora(new Timestamp(new Date().getTime()));
				req.setInfo(RequisicaoTipo.PREVISAO_DIA);
				
				req.setJson((String) params); 
				
				previsaoDiaList = PrevisaoService.parse((String) params);
				
				if (previsaoDiaList.size() > 0) {
				
					RequisicaoDAO.create(app.getDB(), req);
				
					buildWidgetPrevisao();
				}
			}
		};
		service.setServiceListener(previsaoServiceListener);
		service.getForecast(CidadeDAO.getCidadeCarregada(app.db).getCodigo(), dateStart, dateFinish);
	}
	
	public void buildWidgetTempoAgora()
	{
		try {
		
			RemoteViews remoteView = new RemoteViews(ctx.getPackageName(), R.layout.widget_layout);
		
			Intent intent = new Intent(ctx, WidgetProvider.class);
			
			intent.setAction("br.com.ceweather.activity.WidgetProvider");
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	
			PendingIntent pendingIntent = PendingIntent.getBroadcast(ctx,0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			remoteView.setOnClickPendingIntent(R.id.layoutAtualiza, pendingIntent);
			
			Intent intentApp = new Intent(ctx, MainActivity.class);
			intentApp.addFlags(8);
			
			PendingIntent pendingApp = PendingIntent.getActivity(ctx, 0, intentApp, 0);
			remoteView.setOnClickPendingIntent(R.id.layoutTemp, pendingApp);
			remoteView.setOnClickPendingIntent(R.id.layoutQuadrosPrevisao, pendingApp);
		
			if (tempoAgora != null) {
				
				String vento = "";
				
				if (tempoAgora.getVentoVel() != null) {
					vento = tempoAgora.getVentoVel();
					if (tempoAgora.getVentoDir() != null)
						vento += " - "+tempoAgora.getVentoDir();
				}
		
				remoteView.setTextViewText(R.id.txtCidade, CidadeDAO.getCidadeCarregada(app.db).getNome());
				remoteView.setTextViewText(R.id.txtWidAtualizacao, "Atualizado às " + tempoAgora.getAtualizacao());
				remoteView.setTextViewText(R.id.txtTempAgora, tempoAgora.getTemperatura().toLowerCase().replace(" ", ""));
				remoteView.setTextViewText(R.id.txtVentoAgora, vento);
				remoteView.setTextViewText(R.id.txtUmidadeAgora, tempoAgora.getUmidade());
				remoteView.setTextViewText(R.id.txtRadiacaoAgora, tempoAgora.getRad());
				remoteView.setTextViewText(R.id.txtChuva, "Chuva de " + tempoAgora.getChuvaAcum() + " (desde 00h)");
			}
			
			widgetManager.updateAppWidget(widgetId, remoteView);
			
			previsao();
			
		} catch(Exception e) {
			
		}
	}

	public void buildWidgetPrevisao()
	{
		RemoteViews remoteView = new RemoteViews(ctx.getPackageName(), R.layout.widget_layout);
		
		Intent intent = new Intent(ctx, WidgetProvider.class);
		
		intent.setAction("br.com.ceweather.activity.WidgetProvider");
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		PendingIntent pendingIntent = PendingIntent.getBroadcast(ctx,0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		remoteView.setOnClickPendingIntent(R.id.layoutAtualiza, pendingIntent);
		
		Intent intentApp = new Intent(ctx, MainActivity.class);
		intentApp.addFlags(8);
		
		PendingIntent pendingApp = PendingIntent.getActivity(ctx, 0, intentApp, 0);
		remoteView.setOnClickPendingIntent(R.id.layoutTemp, pendingApp);
		remoteView.setOnClickPendingIntent(R.id.layoutQuadrosPrevisao, pendingApp);

		if(previsaoDiaList != null)
		{
			Collections.sort(previsaoDiaList, new Comparator<PrevisaoDia>() {
				@SuppressLint("SimpleDateFormat")
				public int compare(PrevisaoDia o1, PrevisaoDia o2) {

					String date1 = (String)o1.getData();
					String date2 = (String)o2.getData(); 

					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

					Date strDate1 = null;
					Date strDate2 = null;

					try {
						strDate1 = sdf.parse(date1);
						strDate2 = sdf.parse(date2);
					} catch (java.text.ParseException e) {
						e.printStackTrace();
					}

					if (o1.getData() == null || o2.getData() == null)
						return 0;
					return strDate1.compareTo(strDate2);
				}
			});

			PrevisaoDia info1 = previsaoDiaList.get(0);
			remoteView.setTextViewText(R.id.txtPrevisaoDia1, info1.getData());
			String mDrawableName1 = getImageName(info1.getDia().getImg());
			int resID1 = ctx.getResources().getIdentifier(mDrawableName1 , "drawable", ctx.getPackageName());
			
			remoteView.setImageViewResource(R.id.iconWidgetPrevDia1, resID1);
			remoteView.setTextViewText(R.id.txtTempMax1, info1.getDia().getTemperaturaMax()+"°c");
			remoteView.setTextViewText(R.id.txtTempMin1, info1.getDia().getTemperaturaMin()+"°c");

			PrevisaoDia info2 = previsaoDiaList.get(1);
			remoteView.setTextViewText(R.id.txtPrevisaoDia2, info2.getData());
			
			String mDrawableName2 = getImageName(info2.getDia().getImg());
			int resID2 = ctx.getResources().getIdentifier(mDrawableName2 , "drawable", ctx.getPackageName());
			
			remoteView.setImageViewResource(R.id.iconWidgetPrevDia2, resID2);
			remoteView.setTextViewText(R.id.txtTempMax2, info2.getDia().getTemperaturaMax()+"°c");
			remoteView.setTextViewText(R.id.txtTempMin2, info2.getDia().getTemperaturaMin()+"°c");

			PrevisaoDia info3 = previsaoDiaList.get(2);
			remoteView.setTextViewText(R.id.txtPrevisaoDia3, info3.getData());
			String mDrawableName3 = getImageName(info3.getDia().getImg());
			int resID3 = ctx.getResources().getIdentifier(mDrawableName3 , "drawable", ctx.getPackageName());
			remoteView.setImageViewResource(R.id.iconWidgetPrevDia3, resID3);
			remoteView.setTextViewText(R.id.txtTempMax3, info3.getDia().getTemperaturaMax()+"°c");
			remoteView.setTextViewText(R.id.txtTempMin3, info3.getDia().getTemperaturaMin()+"°c");
		}

		widgetManager.updateAppWidget(widgetId, remoteView);
	}

	@Override
	public void onReceive(final Context context, Intent intent) { Log.i("widget", "onReceive");

		if (intent.getAction().equals("br.com.ceweather.activity.WidgetProvider")) { Log.i("widget", "botaoClicado");
	
		ctx = context;
	
		RemoteViews remoteView = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
	
		remoteView.setTextViewText(R.id.txtWidAtualizacao, "Atualizando...");
		remoteView.setTextViewText(R.id.txtCidade, "Aguarde!");
		remoteView.setTextViewText(R.id.txtTempAgora, "--");
		remoteView.setTextViewText(R.id.txtVentoAgora, "--");
		remoteView.setTextViewText(R.id.txtUmidadeAgora, "--");
	//	remoteView.setTextViewText(R.id.txtPressaoAgora, "--");
		remoteView.setTextViewText(R.id.txtRadiacaoAgora, "--");
		remoteView.setTextViewText(R.id.txtChuva, "--");
		remoteView.setTextViewText(R.id.txtPrevisaoDia1, "--");
		remoteView.setTextViewText(R.id.txtTempMax1, "--");
		remoteView.setTextViewText(R.id.txtTempMin1, "--");
		remoteView.setTextViewText(R.id.txtPrevisaoDia2, "--");
		remoteView.setTextViewText(R.id.txtTempMax2, "--");
		remoteView.setTextViewText(R.id.txtTempMin2, "--");
		remoteView.setTextViewText(R.id.txtPrevisaoDia3, "--");
		remoteView.setTextViewText(R.id.txtTempMax3, "--");
		remoteView.setTextViewText(R.id.txtTempMin3, "--");
	
		ComponentName thisWidget = new ComponentName(context, WidgetProvider.class);
	
		AppWidgetManager manager = AppWidgetManager.getInstance(context); 
		int[] allWidgetInstancesIds = manager.getAppWidgetIds(thisWidget);
		for (int _widgetId : allWidgetInstancesIds) {
			this.widgetId = _widgetId;
		}
		manager.updateAppWidget(widgetId, remoteView);
	
		pushWidgetUpdate(context, remoteView);
		}
	
		super.onReceive(context, intent);
	}

	public void pushWidgetUpdate(Context context, RemoteViews remoteViews) { 

		AppWidgetManager manager = AppWidgetManager.getInstance(context); 

		ComponentName thisWidget = new ComponentName(context, WidgetProvider.class);

		int[] allWidgetInstancesIds = manager.getAppWidgetIds(thisWidget);
		onUpdate(context, manager, allWidgetInstancesIds);
	}

	public String getImageName(String icon) {
		
		String name = "img_indisponivel";	

		if (icon == null)
			return name;

		if (icon.equals("14.png")) {
			name = "icon_14";
		} else if (icon.equals("71.png") || icon.equals("88.png") ) {
			name = "icon_88";
		} else if (icon.equals("79.png")) {
			name = "icon_79";
		} else if (icon.equals("81.png")) {
			name = "icon_81";
		} else if (icon.equals("82.png")) {
			name = "icon_82";
		}else if (icon.equals("34.png") || icon.equals("null.png")){
        	name = "icon_34_dia";
        } else if (icon.equals("12.png")){
        	name = "icon_12_dia";
        } else if (icon.equals("29.png") || icon.equals("44.png")){
        	name = "icon_29_dia";
        } else if (icon.equals("63.png") || icon.equals("39.png") || icon.equals("89.png")){
        	name = "icon_63_dia";
        } else if(icon.equals("64.png")){
        	name = "icon_64_dia";
        } else if(icon.equals("65.png")){
        	name = "icon_65_dia";
	    } else if(icon.equals("66.png")){
        	name = "icon_66_dia";
	    } else if(icon.equals("69.png")){
        	name = "icon_69_dia";
	    } else if(icon.equals("73.png")){
        	name = "icon_73_dia";
	    } else if(icon.equals("83.png")){
        	name = "icon_83_dia";
	    } else if(icon.equals("84.png")){
        	name = "icon_84_dia";
	    } else if(icon.equals("85.png")){
        	name = "icon_85_dia";
	    } else if(icon.equals("86.png")){
        	name = "icon_86_dia";
	    } else if(icon.equals("87.png")){
        	name = "icon_87_dia";
	    }

		return name;
	}
}
