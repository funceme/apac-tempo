package br.com.ceweather.activity;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

public class InfoActivity extends DialogFragment{
	
	public ComponentsActivity activity;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

	    Rect displayRectangle = new Rect();
	    Window window = getDialog().getWindow();
	    
	    this.activity = (ComponentsActivity) getActivity();

	    getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
	    window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

	    LayoutInflater layoutInflater = getActivity().getLayoutInflater();

	    final View view;
	    view = layoutInflater.inflate(R.layout.info, null);

	    view.setMinimumWidth((int) (displayRectangle.width() * 0.95f));
	    view.setMinimumHeight((int) (displayRectangle.height() * 0.88f));
	    
	    if (this.activity.latitude != 0 && this.activity.longitude != 0)
			((TextView)view.findViewById(R.id.txtLocalizacao)).setText("GPS: "+this.activity.latitude+", "+this.activity.longitude);
	    
	    ((TextView)view.findViewById(R.id.txtVersao)).setText(R.string.VERSAO);
	    
	    return view;
	}
}