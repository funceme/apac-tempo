package br.com.ceweather.activity;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import br.gov.funceme.database.dao.CidadeDAO;
import br.gov.funceme.database.dao.RequisicaoDAO;
import br.gov.funceme.database.model.Requisicao;
import br.gov.funceme.database.model.TempoAgora;
import br.gov.funceme.database.tipo.RequisicaoTipo;
import br.gov.funceme.webservices.service.TempoAgoraService;
import br.gov.funceme.webservices.ws.AbstractServiceListener;

public class WidgetMediumProvider extends AppWidgetProvider{

	public AppWidgetManager appWidgetManager = null;
	public Context ctx;
	public AppDatabase app;
	
	public int widgetId;

	@Override
	public void onUpdate( Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds )
	{
		ComponentName widget = new ComponentName(context, WidgetMediumProvider.class);

		int[] allWidgetInstancesIds = appWidgetManager.getAppWidgetIds(widget);
		
		for (int _widgetId : allWidgetInstancesIds) {
			this.widgetId = _widgetId;
		}
		
		this.appWidgetManager = appWidgetManager;
		
		this.ctx = context;
		
		app = new AppDatabase(context);
		
		previsao();
	}
	
	public void previsao()
	{
		Requisicao last = null;
		
		if (RequisicaoDAO.getRequisicaoCount(app.getDB(), RequisicaoTipo.TEMPO_AGORA) > 0) {
		
			last = RequisicaoDAO.getUltimaRequisicao(app.getDB(), RequisicaoTipo.TEMPO_AGORA);
			
			render(new TempoAgora(last.getJson()));
		}	
		
		if (last != null) {
			
			Calendar c = Calendar.getInstance(); 
			c.setTime(last.getDatahora());
			c.add(Calendar.MINUTE, Integer.parseInt(ctx.getString(R.string.TIMEOUT_TEMPO_AGORA)));
			
			Date now = new Date();
			
			if(now.after(c.getTime()))
				doRequest();
			
		} else {
			doRequest();
		}
	}

	public void doRequest(){
		
		TempoAgoraService weatherNowService = new TempoAgoraService(ctx);

		AbstractServiceListener weatherNowServiceListener = new AbstractServiceListener() {
			@Override
			public void OnComplete(Object params) {
				
				TempoAgora tempo_agora = new TempoAgora((String) params);
				
				if(tempo_agora != null)
				{
					Requisicao req = new Requisicao();
					
					req.setDatahora(new Timestamp(new Date().getTime()));
					req.setInfo(RequisicaoTipo.TEMPO_AGORA);
					
					req.setJson((String) params); 
					
					RequisicaoDAO.create(app.getDB(), req);
					
					render(tempo_agora);
				}
			}
		};

		weatherNowService.setServiceListener(weatherNowServiceListener);
		weatherNowService.getWeatherNowFromCity(CidadeDAO.getCidadeCarregada(app.getDB()).getCodigo());
	}

	public void render(TempoAgora weatherNow){
		
		RemoteViews remoteView = new RemoteViews(ctx.getPackageName(), R.layout.widget_medium_layout);
		ComponentName watchWidget = new ComponentName(ctx, WidgetMediumProvider.class );
		
		Intent intent = new Intent(ctx, WidgetMediumProvider.class);
		intent.setAction("br.com.ceweather.activity.WidgetMediumProvider");
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		PendingIntent pendingIntent = PendingIntent.getBroadcast(ctx,0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		remoteView.setOnClickPendingIntent(R.id.layoutAtualizaMed, pendingIntent);
		
		Intent intentApp = new Intent(ctx, MainActivity.class);
		intentApp.addFlags(8);
		PendingIntent pendingApp = PendingIntent.getActivity(ctx, 0, intentApp, 0);
		remoteView.setOnClickPendingIntent(R.id.layoutTempoAgoraMed, pendingApp);
		
		if (weatherNow != null) {
			
			float temp = Float.parseFloat(weatherNow.getTemperatura().split(" ")[0].toString()); 
			remoteView.setTextViewText(R.id.txtCidadeMed, CidadeDAO.getCidadeCarregada(app.db).getNome());
			remoteView.setTextViewText(R.id.txtWidAtualizacaoMed, "Atualizado às " + weatherNow.getAtualizacao());
			remoteView.setTextViewText(R.id.txtTempAgoraMed, (int)temp + "°");
			remoteView.setTextViewText(R.id.txtVentoAgoraMed, weatherNow.getVentoVel() + " - " + weatherNow.getVentoDir());
			remoteView.setTextViewText(R.id.txtUmidadeAgoraMed, weatherNow.getUmidade());
			remoteView.setTextViewText(R.id.txtRadiacaoAgoraMed, weatherNow.getRad());
			remoteView.setTextViewText(R.id.txtChuvaMed, "Chuva de " + weatherNow.getChuvaAcum() + " (desde 00h)");
		}
		
		Log.i("widget", "atualizado!");
		appWidgetManager.updateAppWidget(watchWidget, remoteView);
	}
	
	@Override
	public void onReceive(final Context context, Intent intent) { 
	
		if (intent.getAction().equals("br.com.ceweather.activity.WidgetMediumProvider")) { Log.i("widget", "botaoClicado");
			
			ctx = context;
			
			RemoteViews remoteView = new RemoteViews(context.getPackageName(), R.layout.widget_medium_layout);
			
			remoteView.setTextViewText(R.id.txtCidadeMed, "Aguarde!");
			remoteView.setTextViewText(R.id.txtWidAtualizacaoMed, "Atualizando...");
			remoteView.setTextViewText(R.id.txtTempAgoraMed, "--");
			remoteView.setTextViewText(R.id.txtVentoAgoraMed, "--");
			remoteView.setTextViewText(R.id.txtUmidadeAgoraMed, "--");
			remoteView.setTextViewText(R.id.txtRadiacaoAgoraMed, "--");
			remoteView.setTextViewText(R.id.txtChuvaMed, "--");
			
			ComponentName thisWidget = new ComponentName(context, WidgetMediumProvider.class);

			AppWidgetManager manager = AppWidgetManager.getInstance(context); 
			int[] allWidgetInstancesIds = manager.getAppWidgetIds(thisWidget);
			for (int _widgetId : allWidgetInstancesIds) {
				this.widgetId = _widgetId;
			}
			manager.updateAppWidget(widgetId, remoteView);
		
			pushWidgetUpdate(context, remoteView);
		}

		super.onReceive(context, intent);
	}
	
	public void pushWidgetUpdate(Context context, RemoteViews remoteViews) { 
		
		AppWidgetManager manager = AppWidgetManager.getInstance(context); 
		
		ComponentName thisWidget = new ComponentName(context, WidgetMediumProvider.class);

		int[] allWidgetInstancesIds = manager.getAppWidgetIds(thisWidget);
		onUpdate(context, manager, allWidgetInstancesIds);
	}
}