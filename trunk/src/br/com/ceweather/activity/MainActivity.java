package br.com.ceweather.activity;

import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Tracker;

public class MainActivity extends Activity{
	
	private final String PROPERTY_ID = "UA-7298747-15";
	
	private final int SPLASH_DISPLAY_LENGHT = 500;
	
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        setContentView(R.layout.splash);
        
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                
            	Intent showContent = new Intent(getApplicationContext(), ComponentsActivity.class);
                startActivity(showContent);
            }
        }, SPLASH_DISPLAY_LENGHT);
    }
    
    @Override
    protected void onStart() {
        
    	super.onStart();
        
        Tracker tracker = GoogleAnalytics.getInstance(this).getTracker(PROPERTY_ID);

        HashMap<String, String> hitParameters = new HashMap<String, String>();
        hitParameters.put(Fields.HIT_TYPE, "appview");
        hitParameters.put(Fields.SCREEN_NAME, getString(R.string.analytics_home_screen_name));

        tracker.send(hitParameters);
    }
    
    @Override 
    protected void onRestart() {
    	super.onRestart();
    	
    	Intent showContent = new Intent(getApplicationContext(), ComponentsActivity.class);
        startActivity(showContent);
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    }
    
    @Override
    protected void onResume() {
        super.onResume();
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    }
}