package br.com.ceweather.activity;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import br.gov.funceme.database.dao.CidadeDAO;
import br.gov.funceme.database.dao.RequisicaoDAO;
import br.gov.funceme.database.model.PrevisaoDia;
import br.gov.funceme.database.model.Requisicao;
import br.gov.funceme.database.model.TempoAgora;
import br.gov.funceme.database.tipo.RequisicaoTipo;
import br.gov.funceme.webservices.service.TempoAgoraService;
import br.gov.funceme.webservices.ws.AbstractServiceListener;

public class WidgetSmallProvider extends AppWidgetProvider{
	
	public AppWidgetManager appWidgetManager = null;
	public Context ctx;
	
	public AppDatabase app;
	
	public TempoAgora tempoAgora;
	public List<PrevisaoDia> previsaoDiaList;

	public int widgetId;

	@Override
	public void onUpdate( Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds )
	{
		ComponentName thisWidget = new ComponentName(context, WidgetSmallProvider.class);

		int[] allWidgetInstancesIds = appWidgetManager.getAppWidgetIds(thisWidget);
		
		for (int _widgetId : allWidgetInstancesIds) {
			this.widgetId = _widgetId;
		}
		
		this.appWidgetManager = appWidgetManager;
		
		this.ctx = context;

		app = new AppDatabase(context);
		
		doRequest();
	}
	
	public void previsao()
	{
		Requisicao last = null;
		
		if (RequisicaoDAO.getRequisicaoCount(app.getDB(), RequisicaoTipo.TEMPO_AGORA) > 0) {
		
			last = RequisicaoDAO.getUltimaRequisicao(app.getDB(), RequisicaoTipo.TEMPO_AGORA);
			
			tempoAgora = new TempoAgora(last.getJson());
			
			render();
		}	
		
		if (last != null) {
			
			Calendar c = Calendar.getInstance(); 
			c.setTime(last.getDatahora());
			c.add(Calendar.MINUTE, Integer.parseInt(ctx.getString(R.string.TIMEOUT_TEMPO_AGORA)));
			
			Date now = new Date();
			
			if(now.after(c.getTime()))
				doRequest();
			else
				render();
			
		} else {
			doRequest();
		}
	}

	public void doRequest()
	{
		TempoAgoraService weatherNowService = new TempoAgoraService(ctx);

		AbstractServiceListener weatherNowServiceListener = new AbstractServiceListener() {
			@Override
			public void OnComplete(Object params) {
				
				tempoAgora = new TempoAgora((String) params);
				
				if(tempoAgora != null)
				{
					Requisicao req = new Requisicao();
					
					req.setDatahora(new Timestamp(new Date().getTime()));
					req.setInfo(RequisicaoTipo.TEMPO_AGORA);
					
					req.setJson((String) params); 
					
					RequisicaoDAO.create(app.getDB(), req);
					
					render();
				}
			}
		};

		weatherNowService.setServiceListener(weatherNowServiceListener);
		weatherNowService.getWeatherNowFromCity(CidadeDAO.getCidadeCarregada(app.db).getCodigo());
	}
	
	public void render(){
		
		RemoteViews remoteView = new RemoteViews(ctx.getPackageName(), R.layout.widget_small_layout);
		ComponentName watchWidget = new ComponentName(ctx, WidgetSmallProvider.class );
		
		Intent intent = new Intent(ctx, WidgetSmallProvider.class);
		intent.setAction("br.com.ceweather.activity.WidgetSmallProvider");
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		PendingIntent pendingIntent = PendingIntent.getBroadcast(ctx,0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		remoteView.setOnClickPendingIntent(R.id.layoutAtualizaSmall, pendingIntent);
		
		Intent intentApp = new Intent(ctx, MainActivity.class);
		intentApp.addFlags(8);
		PendingIntent pendingApp = PendingIntent.getActivity(ctx, 0, intentApp, 0);
		remoteView.setOnClickPendingIntent(R.id.txtCidadeSmall, pendingApp);
		remoteView.setOnClickPendingIntent(R.id.txtTempAgoraSmall, pendingApp);
	
		if (tempoAgora != null) {
			float temp = Float.parseFloat(tempoAgora.getTemperatura().split(" ")[0].toString()); 
			remoteView.setTextViewText(R.id.txtCidadeSmall, CidadeDAO.getCidadeCarregada(app.db).getNome());
			remoteView.setTextViewText(R.id.txtTempAgoraSmall, (int)temp + "°");		
		}
		
		appWidgetManager.updateAppWidget(watchWidget, remoteView);
	}
	
	@Override
	public void onReceive(final Context context, Intent intent) { Log.i("widget", "onReceive");

		Log.i("widget", intent.getAction());
	
		if (intent.getAction().equals("br.com.ceweather.activity.WidgetSmallProvider")) { Log.i("widget", "botaoClicado");
			
			ctx = context;
			
			RemoteViews remoteView = new RemoteViews(context.getPackageName(), R.layout.widget_small_layout);
			
			remoteView.setTextViewText(R.id.txtCidadeSmall, "Atualizando");
			remoteView.setTextViewText(R.id.txtTempAgoraSmall, "--");		
			
			ComponentName thisWidget = new ComponentName(context, WidgetSmallProvider.class);

			AppWidgetManager manager = AppWidgetManager.getInstance(context); 
			int[] allWidgetInstancesIds = manager.getAppWidgetIds(thisWidget);
			for (int _widgetId : allWidgetInstancesIds) {
				this.widgetId = _widgetId;
			}
			manager.updateAppWidget(widgetId, remoteView);
		
			pushWidgetUpdate(context, remoteView);
		}

		super.onReceive(context, intent);
	}
	
	public void pushWidgetUpdate(Context context, RemoteViews remoteViews) { 
		
		AppWidgetManager manager = AppWidgetManager.getInstance(context); 
		
		ComponentName thisWidget = new ComponentName(context, WidgetSmallProvider.class);

		int[] allWidgetInstancesIds = manager.getAppWidgetIds(thisWidget);
		onUpdate(context, manager, allWidgetInstancesIds);
	}
}