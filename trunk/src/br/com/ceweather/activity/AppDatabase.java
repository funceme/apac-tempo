package br.com.ceweather.activity;

import android.content.Context;
import br.gov.funceme.database.DatabaseModel;
import br.gov.funceme.database.SQLiteHelper;

public class AppDatabase {
	
	private static final String DATABASE_NAME = "FuncemeTempo5";
    private static final int DATABASE_VERSION = 5;
    
    public Context ctx;

	private DatabaseModel dbModel;
	
	public SQLiteHelper db;
	
	public AppDatabase(Context ctx) {
		
		super();
		
		this.ctx = ctx;
		
		String sqlOnCreate = this.ctx.getResources().getString(R.string.SQL_ONCREATE);
		String sqlOnUpdate = this.ctx.getResources().getString(R.string.SQL_ONUPDATE);
		
		dbModel = new DatabaseModel(DATABASE_NAME, DATABASE_VERSION, sqlOnCreate, sqlOnUpdate);
		
		db = new SQLiteHelper(this.ctx, dbModel);
		
	}
	
	public SQLiteHelper getDB() {
		return db;
	}
}
