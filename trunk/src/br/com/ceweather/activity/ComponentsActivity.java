package br.com.ceweather.activity;

import java.util.ArrayList;
import java.util.List;

import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;
import android.R.color;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager.LayoutParams;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import br.com.ceweather.fragments.CellAlertNoFavCityFavorited;
import br.com.ceweather.fragments.ListaCidadesFavoritasFragment;
import br.com.ceweather.fragments.ListaCidadesFragment;
import br.com.ceweather.fragments.MaresFragment;
import br.com.ceweather.fragments.PrevisaoCEFragment;
import br.com.ceweather.fragments.PrevisaoDiaFragment;
import br.com.ceweather.fragments.PrevisaoTurnoFragment;
import br.com.ceweather.fragments.RadarSateliteFragment;
import br.com.ceweather.fragments.SectionFragment;
import br.com.ceweather.fragments.SolLuaFragment;
import br.com.ceweather.fragments.TempoAgoraFragment;
import br.gov.funceme.database.SQLiteHelper;
import br.gov.funceme.database.dao.CidadeDAO;
import br.gov.funceme.database.dao.PortoDAO;
import br.gov.funceme.database.model.Cidade;
import br.gov.funceme.database.model.Porto;
import br.gov.funceme.webservices.service.CidadeLatLonService;
import br.gov.funceme.webservices.service.PortosService;
import br.gov.funceme.webservices.service.PrevisaoCidadeService;
import br.gov.funceme.webservices.ws.AbstractServiceListener;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnCloseListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnOpenListener;

public class ComponentsActivity extends FragmentActivity implements OnRefreshListener, LocationListener {

	public Context ctx;

	private SlidingMenu mainMenu;

	AppDatabase app;
	
	private String default_city = "2304400"; // Fortaleza
	private int gps_refresh_time = 120*1000;
	public float latitude;
	public float longitude;

	private PullToRefreshLayout mPullToRefreshLayout;

	private int selectedForecastOption;

	public Boolean isLoadFavCitys;
	public Boolean isLoadAlertNoFavCity;
	
	public Boolean isCE;

	Button dia;
	Button turno;
	Button estado;
	
	Button btnRadarS;
	Button btnRadarX;
	
	public FragmentManager fragmentManager;
	public FragmentTransaction fragmentTransaction;

	public SectionFragment sectionWeatherNow;
	public TempoAgoraFragment weatherNow;

	public SectionFragment sectionForecast;
	public PrevisaoDiaFragment forecastDay;

	public SectionFragment sectionTides;
	public MaresFragment maresFragment;
	private List<Porto> portos;

	public SectionFragment sectionSunMoon;
	public SolLuaFragment sunMoon;

	public SectionFragment sectionRadarSatellite;        
	
	public RadarSateliteFragment radarSatellite;
	
	private int carregarCidades;
	private int carregarPortos;
	
	private Boolean cidadeTemPorto;
	
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		ctx = this;
		
		app = new AppDatabase(ctx);
		
		carregarCidades = CidadeDAO.getCidadesCount(app.getDB());
		carregarPortos = PortoDAO.getPortosCount(app.getDB());
		
		if (carregarCidades==0) 
			this.carregarCidades();
		
		else if (carregarPortos==0)
			this.carregarPortos();
		
		else
			portos = PortoDAO.getPortos(app.getDB());
		
		getIntent().setAction("Already created");
		
		setContentView(R.layout.activity_main);

		mPullToRefreshLayout = (PullToRefreshLayout) findViewById(R.id.scrollViewPull);
		mPullToRefreshLayout.setBackgroundColor(color.white);

		ActionBarPullToRefresh.from(this)
			.allChildrenArePullable()
			.listener(this)
			.setup(mPullToRefreshLayout);

		mainMenu = new SlidingMenu(this);
		mainMenu.setShadowWidthRes(R.dimen.shadow_width);
		mainMenu.setShadowDrawable(R.drawable.shadow);
		mainMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		mainMenu.setFadeDegree(0.5f);
		mainMenu.setBackgroundColor(Color.BLACK);
		mainMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);

		mainMenu.setMenu(R.layout.menu_frame);

		mainMenu.setOnCloseListener(new OnCloseListener() { 

			@Override
			public void onClose() {

				getActionBar().setDisplayHomeAsUpEnabled(true);
				
				Cidade lastLoaded = CidadeDAO.getCidadeCarregada(app.db);

				getActionBar().setTitle(lastLoaded.getNome());
			}
		});

		mainMenu.setOnOpenListener(new OnOpenListener() {

			@Override
			public void onOpen() {

				getActionBar().setDisplayHomeAsUpEnabled(false);
				
				if(isLoadFavCitys){
					getActionBar().setTitle(R.string.menu_favoritas);
				} else { 
					getActionBar().setTitle(R.string.menu_todas);
				}
			}
		});

		if (CidadeDAO.getCidadesCount(app.db) == 0) {
			getSupportFragmentManager().beginTransaction().replace(R.id.menu_frame, new CellAlertNoFavCityFavorited()).commit();
			isLoadAlertNoFavCity = true;
		}else{
			getSupportFragmentManager().beginTransaction().replace(R.id.menu_frame, new ListaCidadesFavoritasFragment()).commit();
			isLoadAlertNoFavCity = false;
		}
		
		isLoadFavCitys = true;

		getActionBar().setDisplayShowHomeEnabled(false);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		if (!(carregarCidades==0 && carregarPortos==0))
			init();
	}
	
	@Override
	protected void onPause() {
		
		super.onPause();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		finish();
	}
	
	@Override
	protected void onRestart() {
		super.onRestart();
		
		init();
	}
	
	@Override
	public void onRefreshStarted(View view) {

		reinit();	
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
	    outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
	    super.onSaveInstanceState(outState);
	}
	
	private void carregarCidades() {
		
		PrevisaoCidadeService citiesService = new PrevisaoCidadeService(ctx);
		
		AbstractServiceListener citiesServiceListener = new AbstractServiceListener() {
			@Override
			public void OnComplete(Object params) {
				
				try {
					
					@SuppressWarnings("unchecked")
					ArrayList<Cidade> listaCidades = (ArrayList<Cidade>) params;
					
					if (listaCidades == null)
						throw new Exception(getString(R.string.erro_lista_cidades));
					
					CidadeDAO.truncate(app.getDB());
					
					for (Cidade cidade : listaCidades) {
						
						String codigo = cidade.getCodigo();
						
						if (codigo.equals(default_city)) {
							cidade.setFavorita(1);
							cidade.setCarregada(1);
						}
						
						CidadeDAO.create(app.getDB(), cidade);
					}
					
					if (carregarPortos==0)
						carregarPortos();
					
				} catch (Exception e) {
					Log.d("ERRO", e.getMessage());
				}
			}
		};

		citiesService.setServiceListener(citiesServiceListener);
		citiesService.getForecastCities();
	}
	
	private void carregarPortos() {
		
		PortosService service = new PortosService(this);
		
		AbstractServiceListener listener = new AbstractServiceListener() {
			@Override
			public void OnComplete(Object params) {
				@SuppressWarnings("unchecked")

				List<Porto> list = PortosService.parse(params.toString());
				
				if (list != null) {
				
					PortoDAO.truncate(app.getDB());
					
					for (Porto porto : list) {
						try {
							PortoDAO.create(app.getDB(), porto);
						} catch (Exception e) {
							Log.d("ERRO", e.getMessage());
						}
					}
					
					portos = PortoDAO.getPortos(app.getDB());
				}
				
				init();
			}
		};
		service.setServiceListener(listener);
		service.getHarbors();
	}

	private void getLocation() {
		
		LocationManager locationManager;
		
		if (hasGPS(ctx)) {
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);				
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, gps_refresh_time, 0, this);	
		}
	}

	public boolean hasGPS(Context context)
    {
	    final LocationManager mgr = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
	    
	    if ( mgr == null ) 
	    	return false;
	    
	    final ArrayList<String> providers = (ArrayList<String>) mgr.getAllProviders();
	    
	    if ( providers == null ) 
	    	return false;
	    
	    return providers.contains(LocationManager.GPS_PROVIDER);
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);

		menu.add(0, R.style.AppTheme, 0, R.string.menu_favoritas);
		menu.add(0, R.style.AppTheme, 1, R.string.menu_todas);
		menu.add(0, R.style.AppTheme, 2, R.string.menu_sobre);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		
		if (item.getTitle().equals(getString(R.string.menu_favoritas))) {
			
			if (!checkConn(getApplicationContext())) {
				
				Toast toast = Toast.makeText(ctx, "Não é possível exibir cidades favoritas offline!", Toast.LENGTH_LONG);
				toast.show();
				
				return false;
			}
			
			mainMenu.showMenu();
			 
			if (!isLoadFavCitys) {
				if (CidadeDAO.getCidadesCount(app.db) == 0) {
					getSupportFragmentManager().beginTransaction().replace(R.id.menu_frame, new CellAlertNoFavCityFavorited()).commit();
					isLoadAlertNoFavCity = true;
				}else{
					getSupportFragmentManager().beginTransaction().replace(R.id.menu_frame, new ListaCidadesFavoritasFragment()).commit();
					isLoadAlertNoFavCity = false;
				}
				getActionBar().setTitle(R.string.menu_favoritas);
				isLoadFavCitys = true;
			}
		} else if(item.getTitle().equals(getString(R.string.menu_todas))){
			
			if (!checkConn(getApplicationContext())) {
				
				Toast toast = Toast.makeText(ctx, "Não é possível exibir todas as cidades offline!", Toast.LENGTH_LONG);
				toast.show();
				
				return false;
			}
			
			mainMenu.showMenu();
			
			if (isLoadFavCitys || isLoadAlertNoFavCity) {
				getSupportFragmentManager().beginTransaction().replace(R.id.menu_frame, new ListaCidadesFragment()).commit();
				getActionBar().setTitle(R.string.menu_todas);
				isLoadFavCitys = false;
				isLoadAlertNoFavCity = false;
			}
		} else if(item.getTitle().equals(getString(R.string.menu_sobre))){
			
			DialogFragment sobre = new InfoActivity();
		    sobre.show(getSupportFragmentManager(), "Sobre o Funceme Tempo");
			
		} else if(CidadeDAO.getCidadeCarregada(app.db) != null && item.getTitle().equals(CidadeDAO.getCidadeCarregada(app.db).getNome())){
			mainMenu.showMenu();
		}
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (mainMenu.isMenuShowing()) {
			mainMenu.showContent();
		}else{
			mainMenu.showMenu();
		}

		return true;
	}
	
	private void init(){
		
		fragmentManager = getSupportFragmentManager();

		fragmentTransaction = fragmentManager.beginTransaction();

		if (!checkConn(getApplicationContext())) 
			getActionBar().setTitle(R.string.sem_conexao);
			
		try {
			
			getLocation();
		
			Cidade cidadeCaregada = CidadeDAO.getCidadeCarregada(app.getDB());
			
			if (cidadeCaregada==null)
				throw new Exception("Oops! Nenhuma cidade selecionada.");
			
			isCE = (cidadeCaregada.getUf().equals("CE"))? true : false;

			getActionBar().setTitle(cidadeCaregada.getNome());

			// Tempo Agora
			sectionWeatherNow = new SectionFragment();
			sectionWeatherNow.setTitle(getString(R.string.tempo_agora));
			sectionWeatherNow.setImage("icon_termometro");

			weatherNow = new TempoAgoraFragment();

			fragmentTransaction.add(R.id.row_weather_now_section, sectionWeatherNow);        
			fragmentTransaction.add(R.id.row_weather_now_content, weatherNow);

			// Previsão
			sectionForecast = new SectionFragment();
			sectionForecast.setTitle(getString(R.string.previsao));
			sectionForecast.setImage("icon_sol_nuvens");

			sectionForecast.addView(this.createSectionForecastOptions(cidadeCaregada));	

			fragmentTransaction.add(R.id.row_forecast_section, sectionForecast);
			
			if (isCE(cidadeCaregada))
				fragmentTransaction.add(R.id.row_forecast_content, new PrevisaoCEFragment());
			else
				fragmentTransaction.add(R.id.row_forecast_content, new PrevisaoDiaFragment());
					
			// Marés
			sectionTides = new SectionFragment();
			sectionTides.setTitle(getString(R.string.mares));
			sectionTides.setImage("icon_onda");

			maresFragment = new MaresFragment();
			maresFragment.citySelected = (String)cidadeCaregada.getCodigo();

			if (portos != null) {
				
				cidadeTemPorto = false;
	
				for (Porto porto : portos) {
					
					String codigoCidadeCarregada = (String)cidadeCaregada.getCodigo();
					
					if (!codigoCidadeCarregada.equals("null") && codigoCidadeCarregada.equals(porto.getCodigoMunicipio())){
						cidadeTemPorto = true;
						maresFragment.harborSelected = porto.getNome();
						break;
					}
				}
	
				if (cidadeTemPorto) {
					fragmentTransaction.add(R.id.row_tides_section, sectionTides);
					fragmentTransaction.add(R.id.row_tides_content, maresFragment);
					
					this.setHarborName(maresFragment.harborSelected);
				} else if (this.hasTideFragment()){
					fragmentTransaction.remove(sectionTides);
				}
			}

			// Sol e Lua
			sectionSunMoon = new SectionFragment();
			sectionSunMoon.setTitle(getString(R.string.solelua));
			sectionSunMoon.setImage("icon_sol_lua");

			sunMoon = new SolLuaFragment();

			fragmentTransaction.add(R.id.row_sun_section, sectionSunMoon);
			fragmentTransaction.add(R.id.row_sun_content, sunMoon);	

			// Radar e Satelite
			sectionRadarSatellite = new SectionFragment();
			sectionRadarSatellite.setTitle(getString(R.string.radaresatelite));
			sectionRadarSatellite.setImage("icon_radar");
			
			sectionRadarSatellite.addView(this.createSectionRadarSateliteOptions());
				
			int width = this.getResources().getDisplayMetrics().widthPixels;
			int height = (int) Math.ceil(this.getResources().getDisplayMetrics().heightPixels * 0.7);
						
			radarSatellite = new RadarSateliteFragment(new Point(width, height));

			fragmentTransaction.add(R.id.row_radar_section, sectionRadarSatellite);
			fragmentTransaction.add(R.id.row_radar_content, radarSatellite);
		
		} catch (Exception e) {
		
			Toast toast = Toast.makeText(ctx, e.getMessage(), Toast.LENGTH_LONG);
			toast.show();
		}

		fragmentTransaction.commit();		
	}

	public void reinit(){

		if (mainMenu.isMenuShowing()) {
			mainMenu.showContent();
		}

		fragmentManager = getSupportFragmentManager(); 

		fragmentTransaction = fragmentManager.beginTransaction();

		if (!checkConn(getApplicationContext())) {

			getActionBar().setTitle(R.string.sem_conexao);

		} else {
			
			Cidade cidadeCaregada = CidadeDAO.getCidadeCarregada(app.getDB());
			
			if (cidadeCaregada == null) {
				CidadeDAO.atualizarCidadeCarregada(app.getDB(), default_city);
				cidadeCaregada = CidadeDAO.getCidadeCarregada(app.getDB());
			}
			
			getActionBar().setTitle(cidadeCaregada.getNome());

			weatherNow = new TempoAgoraFragment();
			
			if (!this.hasWeatherNowFragment()) {
				fragmentTransaction.add(R.id.row_weather_now_section, sectionWeatherNow); 
			}
			
			fragmentTransaction.replace(R.id.row_weather_now_content, weatherNow);

			Fragment forecast;
			
			if (isCE(cidadeCaregada)) 
				this.enableCEButton();
			else 
				this.disableCEButton();

			switch (selectedForecastOption) {

				case 0: 
					forecast = new PrevisaoDiaFragment();
					break;
				case 1: 
					forecast = new PrevisaoTurnoFragment();
					break;
				case 2: 
					forecast = new PrevisaoCEFragment();
					selectedForecastOption = 0;
					break;
				default:
					forecast = new PrevisaoDiaFragment();
					break;
			}
		
			fragmentTransaction.replace(R.id.row_forecast_content, forecast);

			cidadeTemPorto = false;

			if (portos != null) {

				for (Porto porto : portos) {
					
					String codigoCidadeCarregada = (String)cidadeCaregada.getCodigo();
					
					if (!codigoCidadeCarregada.equals("null") && codigoCidadeCarregada.equals(porto.getCodigoMunicipio())){
						cidadeTemPorto = true;
						maresFragment.harborSelected = porto.getNome();
						break;
					}
				}
	
				if (cidadeTemPorto) {
					
					sectionTides = new SectionFragment();
					sectionTides.setTitle(getString(R.string.mares));
					sectionTides.setImage("icon_onda");
					
					setHarborName(maresFragment.harborSelected);
					
					maresFragment = new MaresFragment();
					
					if (!this.hasTideFragment()) {
						fragmentTransaction.add(R.id.row_tides_content, maresFragment);
						fragmentTransaction.add(R.id.row_tides_section, sectionTides);
					}	
					else {					
						fragmentTransaction.replace(R.id.row_tides_section, sectionTides);
						fragmentTransaction.replace(R.id.row_tides_content, maresFragment);
					}
					
					maresFragment.citySelected = (String)cidadeCaregada.getCodigo();
				} else {
					if (this.hasTideFragment()) {
						fragmentTransaction.remove(sectionTides);
						fragmentTransaction.remove(maresFragment);
					}
				}
			}
			
			sunMoon = new SolLuaFragment();
			fragmentTransaction.replace(R.id.row_sun_content, sunMoon);	
		}

		fragmentTransaction.commit();

		mPullToRefreshLayout.setRefreshComplete();
	}
	
	private Boolean hasWeatherNowFragment() {
		
		Fragment f = getSupportFragmentManager().findFragmentById(R.id.row_weather_now_section);
		
		if(f != null && f instanceof SectionFragment)
			return true;
		
		return false;
	}
	
	private Boolean hasTideFragment() {
		
		Fragment f = getSupportFragmentManager().findFragmentById(R.id.row_tides_section);
		
		if(f != null && f instanceof SectionFragment)
			return true;
		
		return false;
	}
		
	public void setHarborName(String name) {
		
		TextView textName = new TextView(this);
		textName.setText(name);
		textName.setTextColor(Color.WHITE);
		textName.setPadding(0, this.getScale(20), this.getScale(15), 0);
		
		sectionTides.addView(textName);
	}
	
	private LinearLayout createSectionRadarSateliteOptions() {
		
		LinearLayout options = new LinearLayout(this);

		RelativeLayout.LayoutParams containerParams = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);

		options.setLayoutParams(containerParams);
		options.setOrientation(LinearLayout.HORIZONTAL);
		options.setGravity(Gravity.RIGHT);

		btnRadarS = new Button(this);
		btnRadarS.setText(R.string.botao_radar_s);
		btnRadarS.setTextColor(Color.GRAY);
		btnRadarS.setTextSize(12);
		btnRadarS.setOnClickListener(radarSateliteOptionButtonClicked);
		btnRadarS.setId(723270);
		btnRadarS.setTag(0);
		
		btnRadarX = new Button(this);
		btnRadarX.setText(R.string.botao_radar_x);
		btnRadarX.setTextColor(Color.GRAY);
		btnRadarX.setTextSize(12);
		btnRadarX.setOnClickListener(radarSateliteOptionButtonClicked);
		btnRadarX.setId(723271);
		btnRadarX.setTag(0);

		options.addView(btnRadarS);
		options.addView(btnRadarX);
		
		return options;
	}

	private LinearLayout createSectionForecastOptions(Cidade cidade) {

		LinearLayout options = new LinearLayout(this);

		RelativeLayout.LayoutParams containerParams = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);

		options.setLayoutParams(containerParams);
		options.setOrientation(LinearLayout.HORIZONTAL);
		options.setGravity(Gravity.RIGHT);
		
		Boolean isCE = isCE(cidade);
		
		estado = new Button(this);
		estado.setVisibility((isCE)? View.VISIBLE : View.INVISIBLE);
		estado.setText(R.string.botao_previsao_ceara);
		estado.setTextColor((isCE)? Color.WHITE : Color.GRAY);
		estado.setOnClickListener(forecastOptionButtonClicked);
		estado.setId(0);
		
		options.addView(estado);
		
		dia = new Button(this);
		dia.setText(R.string.botao_previsao_dia);
		dia.setOnClickListener(forecastOptionButtonClicked);
		dia.setId(1);	
		dia.setTextColor((isCE)? Color.GRAY : Color.WHITE);
		
		options.addView(dia);

		turno = new Button(this);
		turno.setText(R.string.botao_previsao_turno);
		turno.setTextColor(Color.GRAY);
		turno.setOnClickListener(forecastOptionButtonClicked);
		turno.setId(2);
		
		options.addView(turno);

		return options;
	}
	
	private void enableCEButton() {
		
		estado.setVisibility(View.VISIBLE);
		
		estado.setTextColor(Color.WHITE);
		dia.setTextColor(Color.GRAY);
		turno.setTextColor(Color.GRAY);
		
		selectedForecastOption = 2;
	}
	
	private void disableCEButton() {
		
		estado.setVisibility(View.INVISIBLE);
		
		estado.setTextColor(Color.GRAY);
		dia.setTextColor(Color.WHITE);
		turno.setTextColor(Color.GRAY);
		
		selectedForecastOption = 0;
	}

	private Boolean isCE(Cidade cidade) {
		
		String cidadeNome = cidade.getNome();
		
		String[] parts = cidadeNome.split("-");
		
		String uf = parts[parts.length - 1]; 
		
		return uf.equals("CE");
	}
	
	private OnClickListener radarSateliteOptionButtonClicked = new OnClickListener() {
		@Override
		public void onClick(final View v) {

			switch(v.getId()){
			
			case 723270:
				if (!btnRadarS.getTag().toString().equals("1")) { 
					radarSatellite.loadRadarQuixeramobim();
					ativarBotaoRadarS();
					btnRadarX.setClickable(false);					
				}
				break;
			case 723271:
				if (!btnRadarX.getTag().toString().equals("1")) {
					radarSatellite.loadRadarFortaleza();
					ativarBotaoRadarX();
					btnRadarS.setClickable(false);					
				}
				break;			
			}
		}
	};
	
	public void ativarBotaoRadarS() {
		
		btnRadarS.setTextColor(Color.WHITE);
		btnRadarS.setTag(1);
		
		if (!btnRadarX.getTag().toString().equals("-1")) {
			btnRadarX.setTextColor(Color.GRAY);
			btnRadarX.setTag(0);
		}
	}
	
	public void ativarBotaoRadarX() {
		
		if (!btnRadarS.getTag().toString().equals("-1")) {
			btnRadarS.setTextColor(Color.GRAY);
			btnRadarS.setTag(0);
		}
		
		btnRadarX.setTextColor(Color.WHITE);
		btnRadarX.setTag(1);
	}
	
	public void ativarBotaoSatelite() {
		
		if (!btnRadarS.getTag().toString().equals("-1")) {
			btnRadarS.setTextColor(Color.GRAY);
			btnRadarS.setTag(0);
		}
		
		if (!btnRadarX.getTag().toString().equals("-1")) {
			btnRadarX.setTextColor(Color.GRAY);
			btnRadarX.setTag(0);
		}
	}
	
	public void desativarBotaoRadarS() {
		
		btnRadarS.setTextColor(Color.RED);
		btnRadarS.setTag(-1);
	}
	
	public void desativarBotaoRadarX() {
		
		btnRadarX.setTextColor(Color.RED);
		btnRadarX.setTag(-1);
	}

	private OnClickListener forecastOptionButtonClicked = new OnClickListener() {
		@Override
		public void onClick(final View v) {

			fragmentTransaction = fragmentManager.beginTransaction();

			switch(v.getId()){
			case 0:
				if (selectedForecastOption != 0) {
					PrevisaoCEFragment f0 = new PrevisaoCEFragment();
					fragmentTransaction.replace(R.id.row_forecast_content, f0);
					
					estado.setTextColor(Color.WHITE);
					
					dia.setTextColor(Color.GRAY);
					dia.setClickable(false);
					
					turno.setTextColor(Color.GRAY);
					turno.setClickable(false);

					selectedForecastOption = 0;
				}
				break;
			case 1:
				if (selectedForecastOption != 1) {
					PrevisaoDiaFragment f1 = new PrevisaoDiaFragment();
					fragmentTransaction.replace(R.id.row_forecast_content, f1);
					
					if (estado!=null) {
						estado.setTextColor(Color.GRAY);
						estado.setClickable(false);
					}
					
					dia.setTextColor(Color.WHITE);
					
					turno.setTextColor(Color.GRAY);
					turno.setClickable(false);
					
					selectedForecastOption = 1;
				}
				break;
			case 2:
				if (selectedForecastOption != 2) {
					PrevisaoTurnoFragment f2 = new PrevisaoTurnoFragment();
					fragmentTransaction.replace(R.id.row_forecast_content, f2);
					
					if (estado!=null) {
						estado.setTextColor(Color.GRAY);
						estado.setClickable(false);
					}
					
					dia.setTextColor(Color.GRAY);
					dia.setClickable(false);
					
					turno.setTextColor(Color.WHITE);

					selectedForecastOption = 2;
				}
				break;
			}

			fragmentTransaction.commit();
		}
	};

	@Override
	public void onLocationChanged(Location location) {

//		float latitude = (float) (location.getLatitude());
//		float longitude = (float) (location.getLongitude());
		
		this.longitude = (float) location.getLongitude();
		this.latitude = (float) location.getLatitude();
		
		AbstractServiceListener serviceListener = new AbstractServiceListener() {
			
			private Boolean isFavorite;

			@Override
			public void OnComplete(Object params) {

				final Cidade city = (Cidade) params;

				if (city != null && city.getCodigo()!=null && city.getNome()!= null) {

					Cidade cidadeCarregada = CidadeDAO.getCidadeCarregada(app.getDB());
					
					if (cidadeCarregada == null) {
						CidadeDAO.atualizarCidadeCarregada(app.getDB(), default_city);
						cidadeCarregada = CidadeDAO.getCidadeCarregada(app.getDB());
					}

					if (!city.getCodigo().equals(cidadeCarregada.getCodigo())){

						isFavorite = false;

						for (Cidade cidade : CidadeDAO.getCidadesFavoritas(app.getDB())) {

							if (cidade.getCodigo().equals(city.getCodigo()))
								isFavorite = true;
						}

						if (isFavorite) {
							CidadeDAO.atualizarCidadeCarregada(app.db, city);
							reinit();
						} else{
							
							new AlertDialog.Builder(ctx)
							.setTitle(getString(R.string.pre_texto_localizacao)+city.getNome()+" ("+city.getLat()+", "+city.getLon()+")")
							.setMessage(R.string.confirmacao_cidade)
							.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) { 
									CidadeDAO.atualizarCidadeCarregada(app.db, city);
									reinit();
								}
							})
							.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) { 
									// do nothing
								}
							})
							.show();
						}
					}
				}
			}
		};
		
		//if (latitude > 0 && longitude > 0) {
			CidadeLatLonService service = new CidadeLatLonService(getApplicationContext());
		
			service.setServiceListener(serviceListener);
			service.getCity(latitude, longitude);
		//}
	}

	@Override
	public void onProviderDisabled(String provider) {

		Log.d("PROVIDER", "Disabled : "+provider);
	}

	@Override
	public void onProviderEnabled(String provider) {

		Log.d("PROVIDER", "Enabled : "+provider);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

		Log.d("PROVIDER", "Changed : "+provider+" | status: "+status);
	}

	public static boolean checkConn(Context ctx) {

		ConnectivityManager conMgr = (ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo i = conMgr.getActiveNetworkInfo();

		if (i == null)
			return false;
		if (!i.isConnected())
			return false;
		if (!i.isAvailable())
			return false;

		return true;
	}
	
	@Override
	protected void onResume() {
        
		super.onResume();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		new AlertDialog.Builder(ctx)
	    .setTitle(R.string.app_title)
	    .setMessage(R.string.confirmacao_encerrar)
	    .setPositiveButton(R.string.botao_fechar, new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	            
	            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
	            homeIntent.addCategory( Intent.CATEGORY_HOME );
	            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            startActivity(homeIntent);
	        }
	     })
	    .setNegativeButton(R.string.botao_cancelar, new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	            //reinit();
	        }
	     })
	     .show();	
	}
	
	private int getScale(int value) {
		
		int scale;
		
		DisplayMetrics metrics = this.getResources().getDisplayMetrics();
		
		float s = (float)value/(float)290;
		float xdpi = metrics.xdpi;
		
		scale = (int)Math.ceil(s*xdpi);
		
		return scale;
	}

	public void requestCompleted() {
		
		new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                
            	estado.setClickable(true);
        		dia.setClickable(true);
        		turno.setClickable(true);
        		
        		btnRadarX.setClickable(true);
        		btnRadarS.setClickable(true);
            }
        }, 1000);
	}

	public SQLiteHelper getDB() {
		
		return app.getDB();
	}
}