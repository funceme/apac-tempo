package br.com.ceweather.adapters;

import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.ceweather.activity.R;
import br.com.ceweather.itens.TideItem;
import br.gov.funceme.database.model.Mare;
import br.gov.funceme.database.model.PortoLuaMares;

public class MareAdapter extends ArrayAdapter<PortoLuaMares> {

	private LayoutInflater mInflater;
	private Activity activity;

	public MareAdapter(Activity activity, int resource, List<PortoLuaMares> objects) {
		
		super(activity, resource, objects);

		this.activity = activity;
		this.mInflater = LayoutInflater.from(this.activity);
	}

	public View getView(final int position, View view, ViewGroup collection) {
		
		final TideItem itemHolder;
		
		itemHolder = new TideItem();

		view = mInflater.inflate(R.layout.tide_day, null);
		
		itemHolder.tideDayDate = (TextView) view.findViewById(R.id.txtTideDayDate);

		itemHolder.tideDayHora1 = (TextView) view.findViewById(R.id.txtTideDayHora1);
		itemHolder.tideDayHora2 = (TextView) view.findViewById(R.id.txtTideDayHora2);
		itemHolder.tideDayHora3 = (TextView) view.findViewById(R.id.txtTideDayHora3);
		itemHolder.tideDayHora4 = (TextView) view.findViewById(R.id.txtTideDayHora4);
		
		itemHolder.tideDayAltura1 = (TextView) view.findViewById(R.id.txtTideDayAltura1);
		itemHolder.tideDayAltura2 = (TextView) view.findViewById(R.id.txtTideDayAltura2);
		itemHolder.tideDayAltura3 = (TextView) view.findViewById(R.id.txtTideDayAltura3);
		itemHolder.tideDayAltura4 = (TextView) view.findViewById(R.id.txtTideDayAltura4);
		
		itemHolder.tideDayMare1 = (ImageView) view.findViewById(R.id.imgTideDayMare1);
		itemHolder.tideDayMare2 = (ImageView) view.findViewById(R.id.imgTideDayMare2);
		itemHolder.tideDayMare3 = (ImageView) view.findViewById(R.id.imgTideDayMare3);
		itemHolder.tideDayMare4 = (ImageView) view.findViewById(R.id.imgTideDayMare4);
		
		Typeface fontRegular = Typeface.createFromAsset(this.activity.getAssets(), "fonts/TitilliumWeb-Regular.ttf");
		Typeface fontSemiBold = Typeface.createFromAsset(this.activity.getAssets(), "fonts/TitilliumWeb-SemiBold.ttf");
		
		itemHolder.tideDayDate.setTypeface(fontSemiBold);
		itemHolder.tideDayDate.setTextSize(15);
		
		itemHolder.tideDayHora1.setTypeface(fontRegular);
		itemHolder.tideDayHora1.setTextSize(15);
		itemHolder.tideDayHora2.setTypeface(fontRegular);
		itemHolder.tideDayHora2.setTextSize(15);
		itemHolder.tideDayHora3.setTypeface(fontRegular);
		itemHolder.tideDayHora3.setTextSize(15);
		itemHolder.tideDayHora4.setTypeface(fontRegular);
		itemHolder.tideDayHora4.setTextSize(15);
		
		itemHolder.tideDayAltura1.setTypeface(fontSemiBold);
		itemHolder.tideDayAltura1.setTextSize(15);
		itemHolder.tideDayAltura2.setTypeface(fontSemiBold);
		itemHolder.tideDayAltura2.setTextSize(15);
		itemHolder.tideDayAltura3.setTypeface(fontSemiBold);
		itemHolder.tideDayAltura3.setTextSize(15);
		itemHolder.tideDayAltura4.setTypeface(fontSemiBold);
		itemHolder.tideDayAltura4.setTextSize(15);
		
		int mareBaixa = this.activity.getResources().getIdentifier("icon_mare_baixa3", "drawable", activity.getPackageName());
		int mareAlta = this.activity.getResources().getIdentifier("icon_mare_alta3", "drawable", activity.getPackageName());

		view.setTag(itemHolder);

		PortoLuaMares item = getItem(position);
		
		itemHolder.tideDayDate.setText(item.getData());
		
		List<Mare> mares = (List<Mare>)item.getMares();
		
		int aux = 0;
		
		for (Mare mare : mares) {
			aux++;
			if (aux == 1) {
				
				itemHolder.tideDayMare1.setImageResource((mare.getValor()>1.6f)? mareAlta : mareBaixa);
				itemHolder.tideDayHora1.setText(mare.getHora());
				itemHolder.tideDayAltura1.setText(mare.getValor()+" m");
				
				if (mare.getValor()>1.6)
					itemHolder.tideDayAltura1.setTextColor(Color.parseColor("#fe884e"));
				
			} else if (aux == 2) {
				
				itemHolder.tideDayMare2.setImageResource((mare.getValor()>1.6f)? mareAlta : mareBaixa);
				itemHolder.tideDayHora2.setText(mare.getHora());
				itemHolder.tideDayAltura2.setText(mare.getValor()+" m");
				
				if (mare.getValor()>1.6)
					itemHolder.tideDayAltura2.setTextColor(Color.parseColor("#fe884e"));
				
			} else if (aux == 3) {
				
				itemHolder.tideDayMare3.setImageResource((mare.getValor()>1.6f)? mareAlta : mareBaixa);
				itemHolder.tideDayHora3.setText(mare.getHora());
				itemHolder.tideDayAltura3.setText(mare.getValor()+" m");
				
				if (mare.getValor()>1.6)
					itemHolder.tideDayAltura3.setTextColor(Color.parseColor("#fe884e"));
				
			} else if (aux == 4) {
				
				itemHolder.tideDayMare4.setImageResource((mare.getValor()>1.6f)? mareAlta : mareBaixa);
				itemHolder.tideDayHora4.setText(mare.getHora());
				itemHolder.tideDayAltura4.setText(mare.getValor()+" m");
				
				if (mare.getValor()>1.6)
					itemHolder.tideDayAltura4.setTextColor(Color.parseColor("#fe884e"));	
			}
		}
		return view;
	}
}
