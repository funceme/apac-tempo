package br.com.ceweather.adapters;

import java.util.List;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.ceweather.activity.R;
import br.com.ceweather.itens.SunMoonItem;
import br.gov.funceme.database.model.SolLua;

public class SolLuaAdapter extends ArrayAdapter<SolLua> {

	private LayoutInflater mInflater;
	private Activity activity;

	public SolLuaAdapter(Activity activity, int resource, List<SolLua> objects) {
		
		super(activity, resource, objects);

		this.activity = activity;
		this.mInflater = LayoutInflater.from(this.activity);
	}

	public View getView(final int position, View view, ViewGroup collection) {
		
		final SunMoonItem itemHolder;
		
		itemHolder = new SunMoonItem();

		view = mInflater.inflate(R.layout.sun_moon_day, null);

		itemHolder.moon 	= (TextView) view.findViewById(R.id.sunMoonItem_moon);
		itemHolder.moonImg  = (ImageView) view.findViewById(R.id.sunMoonItem_moonImg);
		itemHolder.sunset 	= (TextView) view.findViewById(R.id.sunMoonItem_sunset);
		itemHolder.sunrise 	= (TextView) view.findViewById(R.id.sunMoonItem_sunrise);
		itemHolder.date 	= (TextView) view.findViewById(R.id.sunMoonItem_date);
		
		Typeface fontRegular = Typeface.createFromAsset(this.activity.getAssets(), "fonts/TitilliumWeb-Regular.ttf");
		Typeface fontSemiBold = Typeface.createFromAsset(this.activity.getAssets(), "fonts/TitilliumWeb-SemiBold.ttf");
		
		itemHolder.moon.setTypeface(fontRegular);
		itemHolder.moon.setTextSize(13);
		
		itemHolder.sunset.setTypeface(fontRegular);
		itemHolder.sunset.setTextSize(15);
		
		itemHolder.sunrise.setTypeface(fontRegular);
		itemHolder.sunrise.setTextSize(15);
		
		itemHolder.date.setTypeface(fontSemiBold);
		itemHolder.date.setTextSize(15);
		
		view.setTag(itemHolder);

		SolLua item = getItem(position);

		itemHolder.moon.setText(item.getLua());
		itemHolder.sunset.setText(item.getNascente()+'h');
		itemHolder.sunrise.setText(item.getPoente()+'h');
		itemHolder.date.setText(item.getDia());
		
		int lua_cheia = this.activity.getResources().getIdentifier("lua_cheia", "drawable", activity.getPackageName());
		int lua_minguante = this.activity.getResources().getIdentifier("lua_minguante", "drawable", activity.getPackageName());
		int lua_crescente = this.activity.getResources().getIdentifier("lua_crescente", "drawable", activity.getPackageName());
		int lua_nova = this.activity.getResources().getIdentifier("lua_nova", "drawable", activity.getPackageName());
		
		int fase = 0;
		
		if (item.getLua().equals(getString(R.string.lua_crescente)))
			fase = lua_crescente;
		else if (item.getLua().equals(getString(R.string.lua_minguante)))
			fase = lua_minguante;
		else if (item.getLua().equals(getString(R.string.lua_cheia)))
			fase = lua_cheia;
		else
			fase = lua_nova;
		
		itemHolder.moonImg.setImageResource(fase); 

		return view;
	}
	
	private String getString(int resId) {
		return activity.getResources().getString(resId);
	}
}
