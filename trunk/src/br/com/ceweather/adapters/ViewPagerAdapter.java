package br.com.ceweather.adapters;

import java.util.List;

import android.app.Activity;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

public class ViewPagerAdapter extends PagerAdapter {

	Activity activity;
	List<View> ListaDeView;

	public ViewPagerAdapter(Activity act, List<View> ListaVi) {
		ListaDeView = ListaVi;
		activity = act;
	}

	public int getCount() {
		return ListaDeView.size();
	}

	public Object instantiateItem(ViewGroup collection, int position) {
		((ViewPager) collection).addView(ListaDeView.get(position), 0);
		return ListaDeView.get(position);
	}

	@Override
	public void destroyItem(View arg0, int arg1, Object arg2) {
		((ViewPager) arg0).removeView((View) arg2);
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == ((View) arg1);
	}

	@Override
	public Parcelable saveState() {
		return null;
	}

	@Override
	public int getItemPosition(Object object) {
		return super.getItemPosition(object);
	}

}
