package br.com.ceweather.adapters;

import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.LayoutParams;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import br.com.ceweather.activity.R;
import br.gov.funceme.database.model.Previsao;
import br.gov.funceme.database.model.PrevisaoDia;

public class PrevisaoPeriodoAdapter extends ViewPagerAdapter {
	
	List<PrevisaoDia> list;
	private Activity activity;

	public PrevisaoPeriodoAdapter(Activity activ, List<PrevisaoDia> lista) {	
		
		super(activ, null);
		
		activity = activ;
		list = lista;
	}	
	
	@Override
	public int getCount() {
		return list.size();
	}
	
	@Override
	public Object instantiateItem(ViewGroup collection, int position) {
		
		PrevisaoDia item = list.get(position);
		
		RelativeLayout view = new RelativeLayout(activity);
		
		RelativeLayout.LayoutParams containerParams = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, this.getScale(200));
		
		view.setLayoutParams(containerParams);
		
		LinearLayout container = new LinearLayout(activity);
		
        container.setOrientation(LinearLayout.VERTICAL);
		container.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		container.setGravity(Gravity.CENTER_HORIZONTAL);
		
		LinearLayout layoutData = new LinearLayout(activity);
	
		layoutData.setOrientation(LinearLayout.HORIZONTAL);

		TextView textData = new TextView(activity);
		textData.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		textData.setGravity(Gravity.CENTER_HORIZONTAL);
		textData.setText(item.getData());
		textData.setTextSize(15);
		textData.setPadding(0, this.getScale(10), 0, 0);
		textData.setTextColor(Color.WHITE);

		Typeface fontSemiBold = Typeface.createFromAsset(this.activity.getAssets(), "fonts/TitilliumWeb-SemiBold.ttf");
		
		textData.setTypeface(fontSemiBold);
		layoutData.addView(textData);
		
		container.addView(layoutData);
		
		LinearLayout containerPeriods = new LinearLayout(activity);
		
		containerPeriods.setOrientation(LinearLayout.HORIZONTAL);
		containerPeriods.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT));
		
		containerPeriods.setGravity(Gravity.CENTER_HORIZONTAL);
		
		containerPeriods.addView(this.createPeriod(getString(R.string.periodo_madrugada)	, item.getMadrugada()));
		containerPeriods.addView(this.createPeriod(getString(R.string.periodo_manha)		, item.getManha()));
		containerPeriods.addView(this.createPeriod(getString(R.string.periodo_tarde)		, item.getTarde()));
		containerPeriods.addView(this.createPeriod(getString(R.string.periodo_noite)		, item.getNoite()));
		
		container.addView(containerPeriods);
		
		view.addView(container);
		
		((ViewPager) collection).addView(view);	

		return view;
	}
	
	private View createPeriod(String title, Previsao forecast) {
		
		Typeface fontRegular = Typeface.createFromAsset(this.activity.getAssets(), "fonts/TitilliumWeb-Regular.ttf");
		Typeface fontSemiBold = Typeface.createFromAsset(this.activity.getAssets(), "fonts/TitilliumWeb-SemiBold.ttf");
		
		LinearLayout layoutPeriod = new LinearLayout(activity);
		
		layoutPeriod.setOrientation(LinearLayout.VERTICAL);
		layoutPeriod.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		
		layoutPeriod.setGravity(Gravity.CENTER);
		
		layoutPeriod.setPadding(this.getScale(10), this.getScale(5), this.getScale(10), 0);
		
		TextView labelTitle = new TextView(activity);
		labelTitle.setText(title);
		labelTitle.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		labelTitle.setTextSize(13);
		labelTitle.setGravity(Gravity.CENTER_HORIZONTAL);
		labelTitle.setTextColor(Color.WHITE);
		labelTitle.setTypeface(fontSemiBold);
		
		ImageView image = new ImageView(activity);
		
		image.setLayoutParams(new LinearLayout.LayoutParams(this.getScale(120), this.getScale(140)));
	
		String img = forecast.getImg();
		String mDrawableName = this.getImageName(title, img);
		int resID = activity.getResources().getIdentifier(mDrawableName , "drawable", activity.getPackageName());
		Drawable drawable = activity.getResources().getDrawable(resID );
		image.setImageDrawable(drawable);
		
		TextView description = new TextView(activity);
		description.setSingleLine(false);
		description.setLines(4);
		
		String descr = forecast.getDescricao();
		
		description.setText(descr);
		description.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		description.setEllipsize(TextUtils.TruncateAt.END);
		description.setTextSize(10);
		description.setGravity(Gravity.CENTER_HORIZONTAL);
		description.setTextColor(Color.WHITE);
		description.setTypeface(fontRegular);
		
		layoutPeriod.addView(labelTitle);
		layoutPeriod.addView(image);
		layoutPeriod.addView(description);
		
		return layoutPeriod;
	}
	
	private String getImageName(String turn, String icon) {
		
		String name = "img_indisponivel";	
		
		if (icon == null)
			return name;
		
		if (icon.equals("14.png")) {
			name = "icon_14";
		} else if (icon.equals("71.png") || icon.equals("88.png") ) {
	    	name = "icon_88";
	    } else if (icon.equals("79.png")) {
	    	name = "icon_79";
	    } else if (icon.equals("81.png")) {
	    	name = "icon_81";
	    } else if (icon.equals("82.png")) {
	    	name = "icon_82";
	    }
	    	    
	    if (turn.equals(getString(R.string.periodo_manha)) || turn.equals(getString(R.string.periodo_tarde))) {
	        if (icon.equals("34.png") || icon.equals("null.png")){
	        	name = "icon_34_dia";
	        } else if (icon.equals("12.png")){
	        	name = "icon_12_dia";
	        } else if (icon.equals("29.png") || icon.equals("44.png")){
	        	name = "icon_29_dia";
	        } else if (icon.equals("63.png") || icon.equals("39.png") || icon.equals("89.png")){
	        	name = "icon_63_dia";
	        } else if(icon.equals("64.png")){
	        	name = "icon_64_dia";
	        } else if(icon.equals("65.png")){
	        	name = "icon_65_dia";
		    } else if(icon.equals("66.png")){
	        	name = "icon_66_dia";
		    } else if(icon.equals("69.png")){
	        	name = "icon_69_dia";
		    } else if(icon.equals("73.png")){
	        	name = "icon_73_dia";
		    } else if(icon.equals("83.png")){
	        	name = "icon_83_dia";
		    } else if(icon.equals("84.png")){
	        	name = "icon_84_dia";
		    } else if(icon.equals("85.png")){
	        	name = "icon_85_dia";
		    } else if(icon.equals("86.png")){
	        	name = "icon_86_dia";
		    } else if(icon.equals("87.png")){
	        	name = "icon_87_dia";
		    }
	    }
	    else if (turn.equals(getString(R.string.periodo_madrugada)) || turn.equals(getString(R.string.periodo_noite))){
	    	if (icon.equals("34.png") || icon.equals("null.png")){
	        	name = "icon_34_noite";
	        } else if (icon.equals("12.png")){
	        	name = "icon_12_noite";
	        } else if (icon.equals("29.png") || icon.equals("44.png")){
	        	name = "icon_29_noite";
	        } else if (icon.equals("63.png") || icon.equals("39.png") || icon.equals("89.png")){
	        	name = "icon_63_noite";
	        } else if(icon.equals("64.png")){
	        	name = "icon_64_noite";
	        } else if(icon.equals("65.png")){
	        	name = "icon_65_noite";
		    } else if(icon.equals("66.png")){
	        	name = "icon_66_noite";
		    } else if(icon.equals("69.png")){
	        	name = "icon_69_noite";
		    } else if(icon.equals("73.png")){
	        	name = "icon_73_noite";
		    } else if(icon.equals("83.png")){
	        	name = "icon_83_noite";
		    } else if(icon.equals("84.png")){
	        	name = "icon_84_noite";
		    } else if(icon.equals("85.png")){
	        	name = "icon_85_noite";
		    } else if(icon.equals("86.png")){
	        	name = "icon_86_noite";
		    } else if(icon.equals("87.png")){
	        	name = "icon_87_noite";
		    }
	    }
	    
	    return name;
	}

	private String getString(int resId) {
		return activity.getResources().getString(resId);
	}
	
	private int getScale(int value) {
		
		int scale;
		
		DisplayMetrics metrics = activity.getResources().getDisplayMetrics();
		
		float s = (float)value/(float)290;
		float xdpi = metrics.xdpi;
		
		scale = (int)Math.ceil(s*xdpi);
		
		return scale;
	}
}