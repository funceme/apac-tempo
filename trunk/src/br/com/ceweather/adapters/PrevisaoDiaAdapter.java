package br.com.ceweather.adapters;

import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.LayoutParams;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import br.com.ceweather.activity.R;
import br.gov.funceme.database.model.Previsao;
import br.gov.funceme.database.model.PrevisaoDia;

public class PrevisaoDiaAdapter extends ViewPagerAdapter {

	List<PrevisaoDia> list;
	private Activity activity;
	
	Typeface fontRegular;
	Typeface fontSemiBold;
	Typeface fontBold;

	public PrevisaoDiaAdapter(Activity activ, List<PrevisaoDia> lista) {	
		
		super(activ, null);
		
		activity = activ;
		list = lista;
		
		fontRegular = Typeface.createFromAsset(this.activity.getAssets(), "fonts/TitilliumWeb-Regular.ttf");
		fontSemiBold = Typeface.createFromAsset(this.activity.getAssets(), "fonts/TitilliumWeb-SemiBold.ttf");
		fontBold = Typeface.createFromAsset(this.activity.getAssets(), "fonts/TitilliumWeb-Bold.ttf");
	}	
	
	@Override
	public int getCount() {
		return list.size();
	}
	
	@Override
	public Object instantiateItem(ViewGroup collection, int position) {
		
		PrevisaoDia item = list.get(position);
		
		RelativeLayout view = new RelativeLayout(activity);
		
		RelativeLayout.LayoutParams containerParams = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		
		view.setLayoutParams(containerParams);
		view.setGravity(Gravity.CENTER_HORIZONTAL);
		
		
		LinearLayout container = new LinearLayout(activity);
		
        container.setOrientation(LinearLayout.VERTICAL);
		container.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		
		container.setGravity(Gravity.CENTER_HORIZONTAL);
		container.setPadding(this.getScale(10), this.getScale(10), this.getScale(10), 0);
		
		TextView textData = new TextView(activity);
		
		textData.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		textData.setGravity(Gravity.CENTER_HORIZONTAL);
		textData.setText(item.getData());
		textData.setTextSize(15);
		textData.setTextColor(Color.WHITE);
		
		textData.setTypeface(fontSemiBold);
		container.addView(textData);
		
		LinearLayout imgContainer = new LinearLayout(activity);
		
		imgContainer.setOrientation(LinearLayout.HORIZONTAL);
		imgContainer.setLayoutParams(new LinearLayout.LayoutParams(this.getScale(280)*2+10, LayoutParams.WRAP_CONTENT));
		imgContainer.setGravity(Gravity.LEFT);
		imgContainer.setPadding(0, 0, 0, this.getScale(-10));//(left, top, right, bottom)
		
		ImageView image = new ImageView(activity);
		
		image.setLayoutParams(new LinearLayout.LayoutParams(this.getScale(120), this.getScale(140)));
		
		Previsao forecast = item.getDia();
		
		String img = forecast.getImg();
		String mDrawableName = this.getImageName(getString(R.string.periodo_manha), img);
		int resID = activity.getResources().getIdentifier(mDrawableName , "drawable", activity.getPackageName());
		Drawable drawable = activity.getResources().getDrawable(resID );
		image.setImageDrawable(drawable);
		image.setPadding(0, this.getScale(-10), this.getScale(10), 0);
		
		TextView description = new TextView(activity);
		description.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		description.setGravity(Gravity.LEFT);
		description.setText(forecast.getDescricao());
		description.setTextSize(13);
		description.setPadding(0, this.getScale(10), 0, 0);
		description.setTextColor(Color.WHITE);
		description.setTypeface(fontRegular);
		
		imgContainer.addView(image);
		imgContainer.addView(description);
		
		container.addView(imgContainer);
		
		LinearLayout infoContainer = new LinearLayout(activity);
		
		infoContainer.setOrientation(LinearLayout.HORIZONTAL);
		infoContainer.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		
		infoContainer.setGravity(Gravity.CENTER);
		infoContainer.setPadding(0, this.getScale(5), 0, 0);
		
		LinearLayout column1Container = new LinearLayout(activity);
		
		column1Container.setOrientation(LinearLayout.VERTICAL);
		column1Container.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		column1Container.setGravity(Gravity.LEFT);
		column1Container.setPadding(this.getScale(10), this.getScale(-5), 0, 0);
		column1Container.addView(createInfoComponent(getString(R.string.label_temperatura)+":", getString(R.string.label_maxima)+":", forecast.getTemperaturaMax()+getString(R.string.unidade_temperatura), getString(R.string.label_minima)+":", forecast.getTemperaturaMin()+getString(R.string.unidade_temperatura)));
		column1Container.addView(createInfoComponent(getString(R.string.label_umidade)+":", getString(R.string.label_maxima)+":", forecast.getUmidadeRelativaMax()+getString(R.string.unidade_umidade), getString(R.string.label_minima)+":", forecast.getUmidadeRelativaMin()+getString(R.string.unidade_umidade)));
		
		LinearLayout column2Container = new LinearLayout(activity);
		
		column2Container.setOrientation(LinearLayout.VERTICAL);
		column2Container.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		column2Container.setGravity(Gravity.RIGHT);
		column2Container.setPadding(0, this.getScale(-5), this.getScale(10), 0);
		column2Container.addView(createInfoComponent(getString(R.string.label_precipitacao)+":", getString(R.string.label_acumulado)+":", forecast.getPrecipitacaoAcumulada()+" "+getString(R.string.unidade_precipitacao), "", ""));
		column2Container.addView(createInfoComponent(getString(R.string.label_velocidade_vento)+":", getString(R.string.label_maxima)+":", forecast.getVentoVelMax()+" "+getString(R.string.unidade_vento), getString(R.string.label_minima)+":", forecast.getVentoVelMin()+" "+getString(R.string.unidade_vento)));
		
		infoContainer.addView(column1Container);
		infoContainer.addView(column2Container);
		
		container.addView(infoContainer);
		
		view.addView(container);
		
		((ViewPager) collection).addView(view);	

		return view;
	}
	
	private LinearLayout createInfoComponent(String title, String label1, String value1,  String label2, String value2) {
		
		LinearLayout componentContainer = new LinearLayout(activity);
		
		int containerWidth = this.getScale(280);
		int labelWidth = this.getScale(135);
		int valueWidth = containerWidth - labelWidth;
		
		int max_color = activity.getResources().getColor(R.color.max_color);
		int min_color = activity.getResources().getColor(R.color.min_color);
		
		componentContainer.setOrientation(LinearLayout.VERTICAL);
		componentContainer.setLayoutParams(new LinearLayout.LayoutParams(containerWidth, LayoutParams.WRAP_CONTENT));
		
		componentContainer.setGravity(Gravity.LEFT);
		componentContainer.setPadding(0, 0, 0, this.getScale(20));
		
		TextView titleLabel = new TextView(activity);
		titleLabel.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		titleLabel.setGravity(Gravity.LEFT);
		titleLabel.setText(title);
		titleLabel.setTextSize(15);
		titleLabel.setPadding(0, 0, 0, 0);
		titleLabel.setTextColor(Color.WHITE);
		titleLabel.setTypeface(fontSemiBold);
		
        LinearLayout info1Container = new LinearLayout(activity);
		
		info1Container.setOrientation(LinearLayout.HORIZONTAL);
		info1Container.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		
		info1Container.setGravity(Gravity.LEFT);
	
		TextView info1Label = new TextView(activity);
		info1Label.setLayoutParams(new LinearLayout.LayoutParams(
				 labelWidth, LayoutParams.WRAP_CONTENT));
		info1Label.setGravity(Gravity.LEFT);
		info1Label.setText(label1);
		info1Label.setTextSize(13);
		info1Label.setPadding(0, 0, 0, 0);
		info1Label.setTextColor(Color.WHITE);
		info1Label.setTypeface(fontRegular);
		
		TextView info1Value = new TextView(activity);
		info1Value.setLayoutParams(new LinearLayout.LayoutParams(
				valueWidth, LayoutParams.WRAP_CONTENT));
		info1Value.setGravity(Gravity.LEFT);
		info1Value.setText(value1);
		info1Value.setTextSize(13);
		info1Value.setPadding(0, 0, 0, 0);
		info1Value.setTextColor(max_color);
		info1Value.setTypeface(fontBold);
		
		info1Container.addView(info1Label);
		info1Container.addView(info1Value);
		
		LinearLayout info2Container = new LinearLayout(activity);
		
		info2Container.setOrientation(LinearLayout.HORIZONTAL);
		info2Container.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		
		info2Container.setGravity(Gravity.LEFT);
	
		TextView info2Label = new TextView(activity);
		info2Label.setLayoutParams(new LinearLayout.LayoutParams(
				 labelWidth, LayoutParams.WRAP_CONTENT));
		info2Label.setGravity(Gravity.LEFT);
		info2Label.setText(label2);
		info2Label.setTextSize(13);
		info2Label.setPadding(0, 0, 0, 0);
		info2Label.setTextColor(Color.WHITE);
		info2Label.setTypeface(fontRegular);
		
		TextView info2Value = new TextView(activity);
		info2Value.setLayoutParams(new LinearLayout.LayoutParams(
				valueWidth, LayoutParams.WRAP_CONTENT));
		info2Value.setGravity(Gravity.LEFT);
		info2Value.setText(value2);
		info2Value.setTextSize(13);
		info2Value.setPadding(0, 0, 0, 0);
		info2Value.setTextColor(min_color);
		info2Value.setTypeface(fontBold);
		
		info2Container.addView(info2Label);
		info2Container.addView(info2Value);
		
		componentContainer.addView(titleLabel);
		componentContainer.addView(info1Container);
		componentContainer.addView(info2Container);
		
		return componentContainer;
	}
	
	private String getImageName(String turn, String icon) {
		
		String name = "img_indisponivel";
		
		// mobile: /var/www/module/Servico/src/Servico/Model/PrevisaoNumericaModel.php
		
		if (icon.equals("34.png")){				// Predomínio de céu claro
        	name = "icon_34_dia";
		} else if (icon.equals("44.png")){		// Parcialmente nublado
        	name = "icon_29_dia";
        } else if (icon.equals("89.png")){		// Nebulosidade variável com possibilidade de chuvas
        	name = "icon_63_dia";
        } else if (icon.equals("14.png")) {		// Nublado		
			name = "icon_14";
		} else if (icon.equals("64.png")){		// Nebulosidade variável com chuvas
        	name = "icon_64_dia";
		} else if (icon.equals("81.png")) {		// Nublado com chuvas
	    	name = "icon_81";
		}
	  
	    return name;
	}
	
	private String getString(int resId) {
		return activity.getResources().getString(resId);
	}
	
	private int getScale(int value) {
		
		int scale;
		
		DisplayMetrics metrics = activity.getResources().getDisplayMetrics();
		
		float s = (float)value/(float)290;
		float xdpi = metrics.xdpi;
		
		scale = (int)Math.ceil(s*xdpi);
		
		return scale;
	}
}
