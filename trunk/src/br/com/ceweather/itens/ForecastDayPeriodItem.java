package br.com.ceweather.itens;

import android.widget.ImageView;
import android.widget.TextView;

public class ForecastDayPeriodItem {

	public TextView date;
	
	public TextView turn1;
	public ImageView img1;	
	public TextView text1;

	public TextView turn2;
	public ImageView img2;	
	public TextView text2;

	public TextView turn3;
	public ImageView img3;	
	public TextView text3;

	public TextView turn4;
	public ImageView img4;	
	public TextView text4;
}