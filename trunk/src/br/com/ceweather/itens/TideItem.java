package br.com.ceweather.itens;

import android.widget.ImageView;
import android.widget.TextView;

public class TideItem {
	
	public TextView tideDayDate;
	
	public TextView tideDayHora1;
	public TextView tideDayHora2;
	public TextView tideDayHora3;
	public TextView tideDayHora4;
	
	public TextView tideDayAltura1;
	public TextView tideDayAltura2;
	public TextView tideDayAltura3;
	public TextView tideDayAltura4;
	
	public ImageView tideDayMare1;
	public ImageView tideDayMare2;
	public ImageView tideDayMare3;
	public ImageView tideDayMare4;
}
