package br.com.ceweather.itens;

import android.widget.ImageView;
import android.widget.TextView;

public class SunMoonItem {
	public ImageView moonImg;
	public TextView moon;
	public TextView sunrise;
	public TextView sunset;
	public TextView date;
}
