package br.gov.funceme.database;

public class DatabaseModel {
	
	private String name;
	private int version;
	private String createSQL;
	private String updateSQL;
	
	public DatabaseModel(String name, int version, String createSQL, String updateSQL) {
		
		super();
		
		this.name = name;
		this.version = version;
		this.createSQL = createSQL;
		this.updateSQL = updateSQL;	
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getVersion() {
		return version;
	}
	
	public void setVersion(int version) {
		this.version = version;
	}
	
	public String getCreateSQL() {
		return createSQL;
	}
	
	public void setCreateSQL(String createSQL) {
		this.createSQL = createSQL;
	}
	
	public String getUpdateSQL() {
		return updateSQL;
	}
	
	public void setUpdateSQL(String updateSQL) {
		this.updateSQL = updateSQL;
	}
}
