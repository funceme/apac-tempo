package br.gov.funceme.database.tipo;

public enum RequisicaoTipo {
	
	TEMPO_AGORA 	("tempoagora"),
	PREVISAO_DIA 	("previsaodia"),
	PREVISAO_TURNO 	("previsaoturno"),
	PREVISAO_CE 	("previsaoce"),
	MARE 			("mare"),
	SOL_LUA			("sollua");
	
	private final String info;
	
	RequisicaoTipo(String info) {
        this.info = info;
    }
	
	public static RequisicaoTipo fromValue(String value) throws IllegalArgumentException {
        try{
        	for (RequisicaoTipo b : RequisicaoTipo.values()) {
                if (value.equalsIgnoreCase(b.info)) {
                	return b;
                }
            }
            return null;
        }catch( ArrayIndexOutOfBoundsException e ) {
             throw new IllegalArgumentException("Unknown enum value :"+ value);
        }
    }

	public String getValue() {
		return info;
	}	
}