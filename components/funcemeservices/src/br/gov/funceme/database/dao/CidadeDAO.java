package br.gov.funceme.database.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.gov.funceme.database.SQLiteHelper;
import br.gov.funceme.database.model.Cidade;

public class CidadeDAO {
		
	public static Cidade create(SQLiteHelper dbHelper, Cidade reg) {
	    
		ContentValues values = CidadeDAO.modelToContentValues(reg);
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    database.insert(Cidade.TABLE, null, values);
	    
	    Cidade newReg = CidadeDAO.getCidade(dbHelper, reg.getCodigo());
	    
	    dbHelper.close();
	    
	    return newReg;
	}
	
	public static int getCidadesCount(SQLiteHelper dbHelper) {
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    Cursor cursor = database.rawQuery("select count(*) from "+Cidade.TABLE, null);

	    cursor.moveToFirst();
	    
	    dbHelper.close();
	    
	    return cursor.getInt(0);
	}
	
	public static List<Cidade> getCidades(SQLiteHelper dbHelper) {
		
		List<Cidade> regs = new ArrayList<Cidade>();

		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
		String sql = "select "+Cidade.COLUMNS+" from "+Cidade.TABLE+" order by nome";
		
	    Cursor cursor = database.rawQuery(sql, null);
	  
	    cursor.moveToFirst();
	    
	    while (!cursor.isAfterLast()) {
	    	Cidade reg = CidadeDAO.cursorToModel(cursor);
	    	regs.add(reg);
	    	cursor.moveToNext();
	    }

	    cursor.close();
	    dbHelper.close();
	    
	    return regs;
	}
	
	public static List<Cidade> getCidadesFavoritas(SQLiteHelper dbHelper) {
		
		List<Cidade> regs = new ArrayList<Cidade>();

		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    Cursor cursor = database.query(Cidade.TABLE,
	    		Cidade.COLUMNS.split(","), "favorita = 1", null, null, null, null);

	    cursor.moveToFirst();
	    
	    while (!cursor.isAfterLast()) {
	    	Cidade reg = CidadeDAO.cursorToModel(cursor);
	    	regs.add(reg);
	    	cursor.moveToNext();
	    }

	    cursor.close();
	    dbHelper.close();
	    
	    return regs;
	}
	
	public static Cidade getCidade(SQLiteHelper dbHelper, String id) {
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    Cursor cursor = database.query(Cidade.TABLE,
	    	Cidade.COLUMNS.split(","), Cidade.ID + " = " + id, null, null, null, null);

	    cursor.moveToFirst();
	    
	    dbHelper.close();
	    
	    return cursorToModel(cursor);
	}
	
	public static Cidade getCidadeCarregada(SQLiteHelper dbHelper) {
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    Cursor cursor = database.query(Cidade.TABLE,
	    	Cidade.COLUMNS.split(","), "carregada = 1", null, null, null, null);

	    cursor.moveToFirst();
	    
	    dbHelper.close();
	    
	    return cursorToModel(cursor);
	}
	
	public static void atualizarCidadeCarregada (SQLiteHelper dbHelper, String codigo) {
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		
		values.put("carregada", 0);
		
		database.update(Cidade.TABLE, values, null, null);
		
		values.clear();
		values.put("carregada", 1);
		
		database.update(Cidade.TABLE, values, Cidade.ID + " = '"+codigo+"'", null);

		dbHelper.close();
	}
	
	public static void atualizarCidadeCarregada (SQLiteHelper dbHelper, Cidade cidade) {
		
		CidadeDAO.atualizarCidadeCarregada(dbHelper, cidade.getCodigo());
	}
	
	public static void favoritarCidade (SQLiteHelper dbHelper, String codigo) {
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		
		values.put("favorita", 	1);
		
		database.update(Cidade.TABLE, values, Cidade.ID + " = '"+codigo+"'", null);

		dbHelper.close();
	}
	
	public static void desfavoritarCidade (SQLiteHelper dbHelper, String codigo) {
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		
		values.put("favorita", 0);
		
		database.update(Cidade.TABLE, values, Cidade.ID + " = '"+codigo+"'", null);

		dbHelper.close();
	}
	
	public static void truncate(SQLiteHelper dbHelper) {
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    Cursor cursor = database.rawQuery("delete from "+Cidade.TABLE, null);

	    cursor.moveToFirst();
	    
	    dbHelper.close();
	}
	
	public static Cidade cursorToModel(Cursor cursor) {
		
		Cidade reg = null;
		
		try {
			
			reg = new Cidade();
			
			reg.setCodigo(cursor.getString(0));
			reg.setNome(cursor.getString(1));
			reg.setUfCodigo(cursor.getString(2));
			reg.setUf(cursor.getString(3));
			reg.setUfNome(cursor.getString(4));
			reg.setCat(cursor.getString(5));
			reg.setLat(cursor.getFloat(6));
			reg.setLon(cursor.getFloat(7));
			reg.setAtual(cursor.getString(8));
			reg.setFavorita(cursor.getInt(9));
			reg.setCarregada(cursor.getInt(10));
			
		} catch (Exception e) {
			reg = null;
		}
		
	    return reg;
	}
	
	public static ContentValues modelToContentValues(Cidade reg) {
		
		ContentValues values = new ContentValues();
		
		values.put("codigo", 	reg.getCodigo());
		values.put("nome", 		reg.getNome());
		values.put("ufCodigo", 	reg.getUfCodigo());
		values.put("uf", 		reg.getUf());
		values.put("cat", 		reg.getCat());
		values.put("lat", 		reg.getLat());
		values.put("lon",		reg.getLon());
		values.put("atual",		reg.getAtual());
		values.put("favorita",	reg.getFavorita());
		values.put("carregada",	reg.getCarregada());
	    
	    return values;
	}
}