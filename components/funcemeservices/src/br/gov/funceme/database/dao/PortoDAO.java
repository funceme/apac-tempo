package br.gov.funceme.database.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.gov.funceme.database.SQLiteHelper;
import br.gov.funceme.database.model.Cidade;
import br.gov.funceme.database.model.Porto;

public class PortoDAO {

	public static Porto create(SQLiteHelper dbHelper, Porto reg) {
	    
		ContentValues values = PortoDAO.modelToContentValues(reg);
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    database.insert(Porto.TABLE, null, values);
	    	    
	    Porto newReg = PortoDAO.getPorto(dbHelper, reg.getCodigo());
	    
	    dbHelper.close();
	    
	    return newReg;
	}
	
	private static Porto getPorto(SQLiteHelper dbHelper, int codigo) {

		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    Cursor cursor = database.query(Porto.TABLE,
	    		Porto.COLUMNS.split(","), Porto.ID + " = " + codigo, null, null, null, null);

	    cursor.moveToFirst();
	    
	    dbHelper.close();
	    
	    return cursorToModel(cursor);
	}

	public static int getPortosCount(SQLiteHelper dbHelper) {
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    Cursor cursor = database.rawQuery("select count(*) from "+Porto.TABLE, null);

	    cursor.moveToFirst();
	    
	    dbHelper.close();
	    
	    return cursor.getInt(0);
	}
	
	public static List<Porto> getPortos(SQLiteHelper dbHelper) {
		
		List<Porto> regs = new ArrayList<Porto>();

		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
		String sql = "select "+Porto.COLUMNS+" from "+Porto.TABLE+" order by nome";
		
	    Cursor cursor = database.rawQuery(sql, null);
	  
	    cursor.moveToFirst();
	    
	    while (!cursor.isAfterLast()) {
	    	Porto reg = PortoDAO.cursorToModel(cursor);
	    	regs.add(reg);
	    	cursor.moveToNext();
	    }

	    cursor.close();
	    dbHelper.close();
	    
	    return regs;
	}
	
	public static void truncate(SQLiteHelper dbHelper) {
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    Cursor cursor = database.rawQuery("delete from "+Porto.TABLE, null);

	    cursor.moveToFirst();
	    
	    dbHelper.close();
	}
	
	public static Porto cursorToModel(Cursor cursor) {
		
		Porto reg = null;
		
		try {
			
			reg = new Porto();
			
			reg.setCodigo(cursor.getInt(0));
			reg.setNome(cursor.getString(1));
			reg.setCodigoMunicipio(cursor.getString(2));
			reg.setDataInicio(cursor.getString(3));
			reg.setDataFim(cursor.getString(4));
			reg.setLat(cursor.getFloat(5));
			reg.setLon(cursor.getFloat(6));
			
		} catch (Exception e) {
			reg = null;
		}
		
	    return reg;
	}
	
	public static ContentValues modelToContentValues(Porto reg) {
		
		ContentValues values = new ContentValues();
		
		values.put("codigo", 			reg.getCodigo());
		values.put("nome", 				reg.getNome());
		values.put("codigoMunicipio", 	reg.getCodigoMunicipio());
		values.put("dataInicio", 		reg.getDataInicio());
		values.put("dataFim", 			reg.getDataFim());
		values.put("lat", 				reg.getLat());
		values.put("lon",				reg.getLon());
	    
	    return values;
	}
}
