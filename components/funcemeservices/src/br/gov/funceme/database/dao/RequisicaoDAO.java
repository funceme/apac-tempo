package br.gov.funceme.database.dao;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.gov.funceme.database.SQLiteHelper;
import br.gov.funceme.database.model.Requisicao;
import br.gov.funceme.database.tipo.RequisicaoTipo;

public class RequisicaoDAO {

	public static Requisicao create(SQLiteHelper dbHelper, Requisicao reg) {
	    
		ContentValues values = RequisicaoDAO.modelToContentValues(reg);
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    database.insert(Requisicao.TABLE, null, values);
	    	    
	    Requisicao newReg = RequisicaoDAO.getRequisicao(dbHelper, reg.getInfo().getValue(), reg.getDatahora());
	    
	    dbHelper.close();
	    
	    return newReg;
	}
	
	public static Requisicao getRequisicao(SQLiteHelper dbHelper, String info, Timestamp datahora) {

		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    Cursor cursor = database.query(Requisicao.TABLE,
	    		Requisicao.COLUMNS.split(","), Requisicao.ID + " = '" + datahora.toString() + "' and info = '" + info +"'" , null, null, null, null);

	    cursor.moveToFirst();
	    
	    dbHelper.close();
	    
	    return cursorToModel(cursor);
	}
	
	public static Requisicao getUltimaRequisicao(SQLiteHelper dbHelper, RequisicaoTipo info) {

		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    Cursor cursor = database.query(Requisicao.TABLE,
	    	Requisicao.COLUMNS.split(","), "info = '" + info.getValue() +"'" , null, null, null, "datahora DESC");

	    cursor.moveToFirst();
	    
	    dbHelper.close();
	    
	    return cursorToModel(cursor);
	}

	public static int getRequisicaoCount(SQLiteHelper dbHelper, RequisicaoTipo info) {
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    Cursor cursor = database.rawQuery("select count(*) from "+Requisicao.TABLE+" where info = '"+info.getValue()+"'", null);

	    cursor.moveToFirst();
	    
	    dbHelper.close();
	    
	    return cursor.getInt(0);
	}
	
	public static void truncate(SQLiteHelper dbHelper) {
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    Cursor cursor = database.rawQuery("delete from "+Requisicao.TABLE, null);

	    cursor.moveToFirst();
	    
	    dbHelper.close();
	}
	
	public static Requisicao cursorToModel(Cursor cursor) {
		
		Requisicao reg = null;
		
		try {
			
			reg = new Requisicao();
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");
			
			reg.setDatahora(new Timestamp(format.parse(cursor.getString(0)).getTime()));
			reg.setInfo(RequisicaoTipo.fromValue(cursor.getString(1)));
			reg.setJson(cursor.getString(2));
			
		} catch (Exception e) {
			reg = null;
		}
		
	    return reg;
	}
	
	public static ContentValues modelToContentValues(Requisicao reg) {
		
		ContentValues values = new ContentValues();
		
		values.put("datahora", 	reg.getDatahora().toString());
		values.put("info", 		reg.getInfo().getValue());
		values.put("json", 		reg.getJson());
	    
	    return values;
	}
}
