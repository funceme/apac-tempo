package br.gov.funceme.database.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.gov.funceme.database.SQLiteHelper;
import br.gov.funceme.database.model.Porto;
import br.gov.funceme.database.model.Previsao;

public class PrevisaoDAO {
	
	public static Previsao create(SQLiteHelper dbHelper, Previsao reg) {
	    
		ContentValues values = PrevisaoDAO.modelToContentValues(reg);
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    database.insert(Previsao.TABLE, null, values);
	    
	    Previsao newReg = PrevisaoDAO.getPrevisao(dbHelper, reg.getCodigo());
	    
	    dbHelper.close();
	    
	    return newReg;
	}
	
	private static Previsao getPrevisao(SQLiteHelper dbHelper, String codigo) {

		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    Cursor cursor = database.query(Porto.TABLE,
	    		Previsao.COLUMNS.split(","), Previsao.ID + " = " + codigo, null, null, null, null);

	    cursor.moveToFirst();
	    
	    dbHelper.close();
	    
	    return cursorToModel(cursor);
	}

	public static int getPrevisaoCount(SQLiteHelper dbHelper) {
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		
	    Cursor cursor = database.rawQuery("select count(*) from "+Previsao.TABLE, null);

	    cursor.moveToFirst();
	    
	    dbHelper.close();
	    
	    return cursor.getInt(0);
	}
	
	public static Previsao cursorToModel(Cursor cursor) {
		
		Previsao reg = null;
		
		try {
			
			reg = new Previsao();
			
			reg.setCodigo(cursor.getString(0));
			reg.setRodadaIni(cursor.getString(1));
			reg.setRodadaFim(cursor.getString(2));
			reg.setModelo(cursor.getString(3));
			reg.setTipoArea(cursor.getString(4));
			reg.setArea(cursor.getString(5));
			reg.setDataIni(cursor.getString(6));
			reg.setDataFim(cursor.getString(7));
			reg.setTemperaturaMax(cursor.getString(8));
			reg.setTemperaturaMin(cursor.getString(9));
			reg.setPrecipitacaoAcumulada(cursor.getString(10));
			reg.setPrecipitacaoMin(cursor.getString(11));
			reg.setPrecipitacaoMax(cursor.getString(12));
			reg.setVentoVelMin(cursor.getString(13));
			reg.setVentoVelMax(cursor.getString(14));
			reg.setVentoDirecao(cursor.getString(15));
			reg.setCoberturaNuvensMin(cursor.getString(16));
			reg.setCoberturaNuvensMax(cursor.getString(17));
			reg.setUmidadeRelativaMin(cursor.getString(18));
			reg.setUmidadeRelativaMax(cursor.getString(19));
			reg.setRadiacaoUv(cursor.getString(20));
			reg.setDescricao(cursor.getString(21));
			reg.setImgCodigo(cursor.getString(22));
			reg.setImg(cursor.getString(23));
			reg.setData(cursor.getString(24));
			reg.setPeriodo(cursor.getString(25));
			reg.setAtualizacao(cursor.getInt(26));
			
		} catch (Exception e) {
			reg = null;
		}
		
	    return reg;
	}
	
	public static ContentValues modelToContentValues(Previsao reg) {
		
		ContentValues values = new ContentValues();
		
		values.put("codigo", 				reg.getCodigo());
		values.put("rodadaIni", 			reg.getRodadaIni());
		values.put("rodadaFim", 			reg.getRodadaFim());
		values.put("modelo", 				reg.getModelo());
		values.put("tipoArea", 				reg.getTipoArea());
		values.put("area", 					reg.getArea());
		values.put("dataIni",				reg.getDataIni());
		values.put("dataFim",				reg.getDataFim());
		values.put("temperaturaMax",		reg.getTemperaturaMax());
		values.put("temperaturaMin",		reg.getTemperaturaMin());
		values.put("precipitacaoAcumulada",	reg.getPrecipitacaoAcumulada());
		values.put("precipitacaoMin", 		reg.getPrecipitacaoMin());
		values.put("precipitacaoMax", 		reg.getPrecipitacaoMax());
		values.put("ventoVelMin", 			reg.getVentoVelMin());
		values.put("ventoVelMax", 			reg.getVentoVelMax());
		values.put("ventoDirecao", 			reg.getVentoDirecao());
		values.put("coberturaNuvensMin", 	reg.getCoberturaNuvensMin());
		values.put("coberturaNuvensMax", 	reg.getCoberturaNuvensMax());
		values.put("umidadeRelativaMin", 	reg.getUmidadeRelativaMin());
		values.put("umidadeRelativaMax", 	reg.getUmidadeRelativaMax());
		values.put("radiacaoUv", 			reg.getRadiacaoUv());
		values.put("descricao", 			reg.getDescricao());
		values.put("imgCodigo", 			reg.getImgCodigo());
		values.put("img", 					reg.getImg());
		values.put("data", 					reg.getData());
		values.put("preriodo", 				reg.getPeriodo());
		values.put("atualizacao", 			reg.getAtualizacao());
	    
	    return values;
	}

}
