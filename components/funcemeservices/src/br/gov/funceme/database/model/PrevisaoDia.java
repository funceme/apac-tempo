package br.gov.funceme.database.model;

public class PrevisaoDia {

	private String data;
	private Previsao dia;
	private Previsao manha;
	private Previsao tarde;
	private Previsao noite;
	private Previsao madrugada;

	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	public Previsao getDia() {
		return dia;
	}
	public void setDia(Previsao dia) {
		this.dia = dia;
	}
	public Previsao getManha() {
		return manha;
	}
	public void setManha(Previsao manha) {
		this.manha = manha;
	}
	public Previsao getTarde() {
		return tarde;
	}
	public void setTarde(Previsao tarde) {
		this.tarde = tarde;
	}
	public Previsao getNoite() {
		return noite;
	}
	public void setNoite(Previsao noite) {
		this.noite = noite;
	}
	public Previsao getMadrugada() {
		return madrugada;
	}
	public void setMadrugada(Previsao madrugada) {
		this.madrugada = madrugada;
	}
}