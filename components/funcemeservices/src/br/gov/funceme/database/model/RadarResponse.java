package br.gov.funceme.database.model;

import java.util.ArrayList;

public class RadarResponse {
	
	private String info;
	private String alert;
	private ArrayList<Radar> list;
	
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getAlert() {
		return alert;
	}
	public void setAlert(String alert) {
		this.alert = alert;
	}
	public ArrayList<Radar> getList() {
		return list;
	}
	public void setList(ArrayList<Radar> radarList) {
		this.list = radarList;
	}
}