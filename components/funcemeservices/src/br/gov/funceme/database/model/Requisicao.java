package br.gov.funceme.database.model;

import java.lang.reflect.Field;
import java.sql.Timestamp;

import org.json.JSONObject;

import br.gov.funceme.database.tipo.RequisicaoTipo;

public class Requisicao implements GenericModel {

	public static final String TABLE = "requisicoes";
	public static final String ID = "datahora";
	public static final String COLUMNS = "datahora,info,json";
	
	private Timestamp datahora;
	private RequisicaoTipo info;
	private String json;
	
	public Timestamp getDatahora() {
		return datahora;
	}

	public void setDatahora(Timestamp datahora) {
		this.datahora = datahora;
	}

	public RequisicaoTipo getInfo() {
		return info;
	}

	public void setInfo(RequisicaoTipo info) {
		
		this.info = info;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	@Override
	public String getTable() {
		return TABLE;
	}

	@Override
	public String getColumns() {
		StringBuffer str = new StringBuffer();
		
		Field[] fields = this.getClass().getDeclaredFields();
		
		for(int i = 0; i < fields.length; i++) {
			str.append(fields[i].toString());
			if (i < (fields.length-1))
				str.append(", ");
	    }
		
		return str.toString();
	}
}
