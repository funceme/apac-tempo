package br.gov.funceme.database.model;

public class Mare {

	private String hora;
	private float valor;
	
	public String getHora() {
		return hora;
	}
	
	public void setHora(String hora) {
		this.hora = hora;
	}
	
	public float getValor() {
		return valor;
	}
	
	public void setValor(float f) {
		this.valor = f;
	}
	
	public boolean isCres() {
		
		if(valor > 1.2)
			return true;
		else
			return false;
	}
}