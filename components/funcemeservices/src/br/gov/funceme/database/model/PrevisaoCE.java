package br.gov.funceme.database.model;

import org.json.JSONObject;

import android.util.Log;

public class PrevisaoCE {

	private String dataPrevisao;
	private String diaPrevisao;
	private String previsao24horas;
	private String previsao48horas;
	private String previsao72horas;
	private String previsao96horas;
	private String colunas;
	
	public PrevisaoCE() {
		super();
	}
	
	public PrevisaoCE(String response) {
		super();
		
		JSONObject jsonResponse = null;
		JSONObject informacoes = null;
		
		try {
			
			jsonResponse = new JSONObject(response);
			
			if (jsonResponse.has("informacoes")) 
				informacoes = jsonResponse.getJSONObject("informacoes");
			
			if (informacoes.has("dia24")) 	
				this.setDataPrevisao(informacoes.getString("dia24"));
			
			if (informacoes.has("dia48")) 	
				this.setDiaPrevisao(informacoes.getString("dia48"));
			
			this.setPrevisao24horas(informacoes.getString("previsao24"));
			
			this.setPrevisao48horas(informacoes.getString("previsao48"));
			
			this.setPrevisao72horas(informacoes.getString("previsao72"));
			
			if (informacoes.has("previsao96")) 
				this.setPrevisao96horas(informacoes.getString("previsao96"));
			
			if (jsonResponse.has("colunas")) 
				this.setColunas(jsonResponse.getString("colunas").toString());
			
		} catch(Exception e)
		{
			Log.d("DEBUG", e.getMessage());
		}
	}
	
	public String getDataPrevisao() {
		return dataPrevisao;
	}
	public void setDataPrevisao(String dataPrevisao) {
		this.dataPrevisao = dataPrevisao;
	}
	public String getDiaPrevisao() {
		return diaPrevisao;
	}
	public void setDiaPrevisao(String diaPrevisao) {
		this.diaPrevisao = diaPrevisao;
	}
	public String getPrevisao24horas() {
		return previsao24horas;
	}
	public void setPrevisao24horas(String previsao24horas) {
		this.previsao24horas = previsao24horas;
	}
	public String getPrevisao48horas() {
		return previsao48horas;
	}
	public void setPrevisao48horas(String previsao48horas) {
		this.previsao48horas = previsao48horas;
	}
	public String getPrevisao72horas() {
		return previsao72horas;
	}
	public void setPrevisao72horas(String previsao72horas) {
		this.previsao72horas = previsao72horas;
	}
	public String getPrevisao96horas() {
		return previsao96horas;
	}
	public void setPrevisao96horas(String previsao96horas) {
		this.previsao96horas = previsao96horas;
	}
	public String getColunas() {
		return colunas;
	}
	public void setColunas(String colunas) {
		this.colunas = colunas;
	}	
}
