package br.gov.funceme.database.model;

import org.json.JSONObject;

import android.util.Log;

public class TempoAgora {
	
	private String codigo;
	private String codigoMunicipio;
	private String nome;
	private String latitude;
	private String longitude;
	private String imagem;
	private String temperatura;
	private String temperaturaMin;
	private String temperaturaMax;
	private String umidade;
	private String umidadeMin;
	private String umidadeMax;
	private String ventoVel;
	private String ventoVelMin;
	private String dirVentoVelMin;
	private String ventoVelMax;
	private String dirVentoVelMax;
	private String ventoDir;
	private String chuva;
	private String chuvaAcum;
	private String pressao;
	private String pressaoMin;
	private String pressaoMax;
	private String rad;
	private String radMin;
	private String radMax;
	private String radAcum;
	private String atualizacao;
	private String imgFundo;
	
	public TempoAgora() {
		super();
	}
	
	public TempoAgora(String response) {
		
		super();
		
		JSONObject jsonResponse = null;
		try {
			
			if(response == null)
				throw new Exception("Oops! Resposta vazia para a solicitação de tempo agora da cidade.");
			
			jsonResponse = new JSONObject(response);

			if (jsonResponse.has("codigo")) 			this.setCodigo(jsonResponse.getString("codigo"));
			if (jsonResponse.has("codigo_municipio")) 	this.setCodigoMunicipio(jsonResponse.getString("codigo_municipio"));
			if (jsonResponse.has("nome")) 				this.setNome(jsonResponse.getString("nome"));
			if (jsonResponse.has("latitude")) 			this.setLatitude(jsonResponse.getString("latitude"));
			if (jsonResponse.has("longitude")) 			this.setLongitude(jsonResponse.getString("longitude"));
			if (jsonResponse.has("imagem")) 			this.setImagem(jsonResponse.getString("imagem"));
			if (jsonResponse.has("temperatura")) 		this.setTemperatura(jsonResponse.getString("temperatura"));
			if (jsonResponse.has("temperatura_min")) 	this.setTemperaturaMin(jsonResponse.getString("temperatura_min"));
			if (jsonResponse.has("temperatura_max")) 	this.setTemperaturaMax(jsonResponse.getString("temperatura_max"));
			if (jsonResponse.has("umidade")) 			this.setUmidade(jsonResponse.getString("umidade"));
			if (jsonResponse.has("umidade_min")) 		this.setUmidadeMin(jsonResponse.getString("umidade_min"));
			if (jsonResponse.has("umidade_max")) 		this.setUmidadeMax(jsonResponse.getString("umidade_max"));
			if (jsonResponse.has("vento_vel")) 			this.setVentoVel(jsonResponse.getString("vento_vel"));
			if (jsonResponse.has("vento_vel_min")) 		this.setVentoVelMin(jsonResponse.getString("vento_vel_min"));
			if (jsonResponse.has("dir_vento_vel_min")) 	this.setDirVentoVelMin(jsonResponse.getString("dir_vento_vel_min"));
			if (jsonResponse.has("vento_vel_max")) 		this.setVentoVelMax(jsonResponse.getString("vento_vel_max"));
			if (jsonResponse.has("dir_vento_vel_max")) 	this.setDirVentoVelMax(jsonResponse.getString("dir_vento_vel_max"));
			if (jsonResponse.has("vento_dir")) 			this.setVentoDir(jsonResponse.getString("vento_dir"));
			if (jsonResponse.has("chuva")) 				this.setChuva(jsonResponse.getString("chuva"));
			if (jsonResponse.has("chuva_acum")) 		this.setChuvaAcum(jsonResponse.getString("chuva_acum"));
			if (jsonResponse.has("pressao")) 			this.setPressao(jsonResponse.getString("pressao"));
			if (jsonResponse.has("pressao_min")) 		this.setPressaoMin(jsonResponse.getString("pressao_min"));
			if (jsonResponse.has("pressao_max")) 		this.setPressaoMax(jsonResponse.getString("pressao_max"));
			if (jsonResponse.has("rad")) 				this.setRad(jsonResponse.getString("rad"));
			if (jsonResponse.has("rad_min")) 			this.setRadMin(jsonResponse.getString("rad_min"));
			if (jsonResponse.has("rad_max")) 			this.setRadMax(jsonResponse.getString("rad_max"));
			if (jsonResponse.has("rad_acum")) 			this.setRadAcum(jsonResponse.getString("rad_acum"));
			if (jsonResponse.has("atualizacao")) 		this.setAtualizacao(jsonResponse.getString("atualizacao"));
			if (jsonResponse.has("img_fundo")) 			this.setImgFundo(jsonResponse.getString("img_fundo"));
			
		} catch(Exception e)
		{
			Log.d("DEBUG", e.getMessage());
		}
		
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigoMunicipio() {
		return codigoMunicipio;
	}

	public void setCodigoMunicipio(String codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public String getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}

	public String getTemperaturaMin() {
		return temperaturaMin;
	}

	public void setTemperaturaMin(String temperatura_min) {
		this.temperaturaMin = temperatura_min;
	}

	public String getTemperaturaMax() {
		return temperaturaMax;
	}

	public void setTemperaturaMax(String temperatura_max) {
		this.temperaturaMax = temperatura_max;
	}

	public String getUmidade() {
		return umidade;
	}

	public void setUmidade(String umidade) {
		this.umidade = umidade;
	}

	public String getUmidadeMin() {
		return umidadeMin;
	}

	public void setUmidadeMin(String umidadeMin) {
		this.umidadeMin = umidadeMin;
	}

	public String getUmidadeMax() {
		return umidadeMax;
	}

	public void setUmidadeMax(String umidadeMax) {
		this.umidadeMax = umidadeMax;
	}

	public String getVentoVel() {
		return ventoVel;
	}

	public void setVentoVel(String ventoVel) {
		this.ventoVel = ventoVel;
	}

	public String getVentoVelMin() {
		return ventoVelMin;
	}

	public void setVentoVelMin(String ventoVelMin) {
		this.ventoVelMin = ventoVelMin;
	}

	public String getDirVentoVelMin() {
		return dirVentoVelMin;
	}

	public void setDirVentoVelMin(String dirVentoVelMin) {
		this.dirVentoVelMin = dirVentoVelMin;
	}

	public String getVentoVelMax() {
		return ventoVelMax;
	}

	public void setVentoVelMax(String ventoVelMax) {
		this.ventoVelMax = ventoVelMax;
	}

	public String getDirVentoVelMax() {
		return dirVentoVelMax;
	}

	public void setDirVentoVelMax(String dirVentoVelMax) {
		this.dirVentoVelMax = dirVentoVelMax;
	}

	public String getVentoDir() {
		return ventoDir;
	}

	public void setVentoDir(String ventoDir) {
		this.ventoDir = ventoDir;
	}

	public String getChuva() {
		return chuva;
	}

	public void setChuva(String chuva) {
		this.chuva = chuva;
	}

	public String getChuvaAcum() {
		return chuvaAcum;
	}

	public void setChuvaAcum(String chuvaAcum) {
		this.chuvaAcum = chuvaAcum;
	}

	public String getPressao() {
		return pressao;
	}

	public void setPressao(String pressao) {
		this.pressao = pressao;
	}

	public String getPressaoMin() {
		return pressaoMin;
	}

	public void setPressaoMin(String pressaoMin) {
		this.pressaoMin = pressaoMin;
	}

	public String getPressaoMax() {
		return pressaoMax;
	}

	public void setPressaoMax(String pressaoMax) {
		this.pressaoMax = pressaoMax;
	}

	public String getRad() {
		return rad;
	}

	public void setRad(String rad) {
		this.rad = rad;
	}

	public String getRadMin() {
		return radMin;
	}

	public void setRadMin(String radMin) {
		this.radMin = radMin;
	}

	public String getRadMax() {
		return radMax;
	}

	public void setRadMax(String radMax) {
		this.radMax = radMax;
	}

	public String getRadAcum() {
		return radAcum;
	}

	public void setRadAcum(String radAcum) {
		this.radAcum = radAcum;
	}

	public String getAtualizacao() {
		return atualizacao;
	}

	public void setAtualizacao(String atualizacao) {
		this.atualizacao = atualizacao;
	}

	public String getImgFundo() {
		return imgFundo;
	}

	public void setImgFundo(String imgFundo) {
		this.imgFundo = imgFundo;
	}

	@Override
	public boolean equals(Object object)
	{
		if(object.getClass().equals(this.getClass()))
		{
			TempoAgora o = (TempoAgora) object;
			if(o.getCodigo() == this.getCodigo())
				return true;
		}
		return false;
	}	
}
