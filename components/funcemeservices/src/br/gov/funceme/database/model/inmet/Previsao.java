package br.gov.funceme.database.model.inmet;

public class Previsao {

	private long geocode;
	private String data;
	private String periodo;
	private String uf;
	private String cidade;
	private String resumo;
	private String tempo;
	private float temperatura_maxima;
	private float temperatura_minima;
	private String direcao_vento;
	private String intensidade_vento;
	private String icone;
	private String dia_semana;
	private int umidade_maxima;
	private int umidade_minima;
	private String nascer;
	private String ocaso;
	private String temperatura_maxima_tendencia;
	private String temperatura_maxima_tendencia_icone;
	private String temperatura_minima_tendencia;
	private String temperatura_minima_tendencia_icone;
	private String fonte;
	private long created_at;
	
	public long getGeocode() {
		return geocode;
	}
	public void setGeocode(long geocode) {
		this.geocode = geocode;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getResumo() {
		return resumo;
	}
	public void setResumo(String resumo) {
		this.resumo = resumo;
	}
	public String getTempo() {
		return tempo;
	}
	public void setTempo(String tempo) {
		this.tempo = tempo;
	}
	public float getTemperaturaMaxima() {
		return temperatura_maxima;
	}
	public void setTemperaturaMaxima(float temperaturaMaxima) {
		this.temperatura_maxima = temperaturaMaxima;
	}
	public float getTemperaturaMinima() {
		return temperatura_minima;
	}
	public void setTemperaturaMinima(float temperaturaMinima) {
		this.temperatura_minima = temperaturaMinima;
	}
	public String getDirecaoVento() {
		return direcao_vento;
	}
	public void setDirecaoVento(String direcaoVento) {
		this.direcao_vento = direcaoVento;
	}
	public String getIntensidadeVento() {
		return intensidade_vento;
	}
	public void setIntensidadeVento(String intensidadeVento) {
		this.intensidade_vento = intensidadeVento;
	}
	public String getIcone() {
		return icone;
	}
	public void setIcone(String icone) {
		this.icone = icone;
	}
	public String getDiaSemana() {
		return dia_semana;
	}
	public void setDiaSemana(String diaSemana) {
		this.dia_semana = diaSemana;
	}
	public int getUmidadeMaxima() {
		return umidade_maxima;
	}
	public void setUmidadeMaxima(int umidadeMaxima) {
		this.umidade_maxima = umidadeMaxima;
	}
	public int getUmidadeMinima() {
		return umidade_minima;
	}
	public void setUmidadeMinima(int umidadeMinima) {
		this.umidade_minima = umidadeMinima;
	}
	public String getNascer() {
		return nascer;
	}
	public void setNascer(String nascer) {
		this.nascer = nascer;
	}
	public String getOcaso() {
		return ocaso;
	}
	public void setOcaso(String ocaso) {
		this.ocaso = ocaso;
	}
	public String getTemperaturaMaximaTendencia() {
		return temperatura_maxima_tendencia;
	}
	public void setTemperaturaMaximaTendencia(String temperaturaMaximaTendencia) {
		this.temperatura_maxima_tendencia = temperaturaMaximaTendencia;
	}
	public String getTemperaturaMaximaTendenciaIcone() {
		return temperatura_maxima_tendencia_icone;
	}
	public void setTemperaturaMaximaTendenciaIcone(
			String temperaturaMaximaTendenciaIcone) {
		this.temperatura_maxima_tendencia_icone = temperaturaMaximaTendenciaIcone;
	}
	public String getTemperaturaMinimaTendencia() {
		return temperatura_minima_tendencia;
	}
	public void setTemperaturaMinimaTendencia(String temperaturaMinimaTendencia) {
		this.temperatura_minima_tendencia = temperaturaMinimaTendencia;
	}
	public String getTemperaturaMinimaTendenciaIcone() {
		return temperatura_minima_tendencia_icone;
	}
	public void setTemperaturaMinimaTendenciaIcone(
			String temperaturaMinimaTendenciaIcone) {
		this.temperatura_minima_tendencia_icone = temperaturaMinimaTendenciaIcone;
	}
	public String getFonte() {
		return fonte;
	}
	public void setFonte(String fonte) {
		this.fonte = fonte;
	}
	public long getCreatedAt() {
		return created_at;
	}
	public void setCreatedAt(long createdAt) {
		this.created_at = createdAt;
	}
}
