package br.gov.funceme.database.model;

public class Previsao {
	
	private String codigo;
	private String rodadaIni;
	private String rodadaFim;
	private String modelo;
	private String tipoArea;
	private String area;
	private String dataIni;
	private String dataFim;
	private String temperaturaMax;
	private String temperaturaMin;
	private String precipitacaoAcumulada;
	private String precipitacaoMin;
	private String precipitacaoMax;
	private String ventoVelMin;
	private String ventoVelMax;
	private String ventoDirecao;
	private String coberturaNuvensMin;
	private String coberturaNuvensMax;
	private String umidadeRelativaMin;
	private String umidadeRelativaMax;
	private String radiacaoUv;
	private String descricao;
	private String imgCodigo;
	private String img;
	private String data;
	private String periodo;
	private int atualizacao;
	
	public static final String TABLE = "previsao";
	public static final String ID = "codigo";
	public static final String COLUMNS = "codigo,rodadaIni,rodadaFim,modelo,tipoArea,area,dataIni," +
			"dataFim,temperaturaMax,temperaturaMin,precipitacaoAcumulada,precipitacaoMin,precipitacaoMax," +
			"ventoVelMin,ventoVelMax,ventoDirecao,coberturaNuvensMin,coberturaNuvensMax,umidadeRelativaMin," +
			"umidadeRelativaMax,radiacaoUv,descricao,imgCodigo,img,data,periodo,atualizacao";
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getRodadaIni() {
		return rodadaIni;
	}
	public void setRodadaIni(String rodadaIni) {
		this.rodadaIni = rodadaIni;
	}
	public String getRodadaFim() {
		return rodadaFim;
	}
	public void setRodadaFim(String rodadaFim) {
		this.rodadaFim = rodadaFim;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getTipoArea() {
		return tipoArea;
	}
	public void setTipoArea(String tipoArea) {
		this.tipoArea = tipoArea;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getDataIni() {
		return dataIni;
	}
	public void setDataIni(String dataIni) {
		this.dataIni = dataIni;
	}
	public String getDataFim() {
		return dataFim;
	}
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
	public String getTemperaturaMax() {
		return temperaturaMax;
	}
	public void setTemperaturaMax(String temperaturaMax) {
		this.temperaturaMax = temperaturaMax;
	}
	public String getTemperaturaMin() {
		return temperaturaMin;
	}
	public void setTemperaturaMin(String temperaturaMin) {
		this.temperaturaMin = temperaturaMin;
	}
	public String getPrecipitacaoAcumulada() {
		return precipitacaoAcumulada;
	}
	public void setPrecipitacaoAcumulada(String precipitacaoAcumulada) {
		this.precipitacaoAcumulada = precipitacaoAcumulada;
	}
	public String getPrecipitacaoMin() {
		return precipitacaoMin;
	}
	public void setPrecipitacaoMin(String precipitacaoMin) {
		this.precipitacaoMin = precipitacaoMin;
	}
	public String getPrecipitacaoMax() {
		return precipitacaoMax;
	}
	public void setPrecipitacaoMax(String precipitacaoMax) {
		this.precipitacaoMax = precipitacaoMax;
	}
	public String getVentoVelMin() {
		return ventoVelMin;
	}
	public void setVentoVelMin(String ventoVelMin) {
		this.ventoVelMin = ventoVelMin;
	}
	public String getVentoVelMax() {
		return ventoVelMax;
	}
	public void setVentoVelMax(String ventoVelMax) {
		this.ventoVelMax = ventoVelMax;
	}
	public String getVentoDirecao() {
		return ventoDirecao;
	}
	public void setVentoDirecao(String ventoDirecao) {
		this.ventoDirecao = ventoDirecao;
	}
	public String getCoberturaNuvensMin() {
		return coberturaNuvensMin;
	}
	public void setCoberturaNuvensMin(String coberturaNuvensMin) {
		this.coberturaNuvensMin = coberturaNuvensMin;
	}
	public String getCoberturaNuvensMax() {
		return coberturaNuvensMax;
	}
	public void setCoberturaNuvensMax(String coberturaNuvensMax) {
		this.coberturaNuvensMax = coberturaNuvensMax;
	}
	public String getUmidadeRelativaMin() {
		return umidadeRelativaMin;
	}
	public void setUmidadeRelativaMin(String umidadeRelativaMin) {
		this.umidadeRelativaMin = umidadeRelativaMin;
	}
	public String getUmidadeRelativaMax() {
		return umidadeRelativaMax;
	}
	public void setUmidadeRelativaMax(String umidadeRelativaMax) {
		this.umidadeRelativaMax = umidadeRelativaMax;
	}
	public String getRadiacaoUv() {
		return radiacaoUv;
	}
	public void setRadiacaoUv(String radiacaoUv) {
		this.radiacaoUv = radiacaoUv;
	}
	public String getImgCodigo() {
		return imgCodigo;
	}
	public void setImgCodigo(String imgCodigo) {
		this.imgCodigo = imgCodigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public int getAtualizacao() {
		return atualizacao;
	}
	public void setAtualizacao(int atualizacao) {
		this.atualizacao = atualizacao;
	}
	
	@Override
	public boolean equals(Object object)
	{
		if(object.getClass().equals(this.getClass()))
		{
			Previsao o = (Previsao) object;
			if(o.getCodigo() == this.getCodigo())
				return true;
		}
		return false;
	}
}