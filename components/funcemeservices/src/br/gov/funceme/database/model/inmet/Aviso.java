package br.gov.funceme.database.model.inmet;

public class Aviso {
	private String id;
	private Number idAviso;
	private String idEstado;
	private String local;
	private String texto;
	private String tipoAviso;
	private String arquivo;
	private String dataInicio;
	private String horaInicio;
	private String dataFim;
	private String horaFim;
	private String condicaoSevera;
	
	public String getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}
	public String getLocal() {
		return local;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Number getIdAviso() {
		return idAviso;
	}
	public void setIdAviso(Number idAviso) {
		this.idAviso = idAviso;
	}
	public String getTipoAviso() {
		return tipoAviso;
	}
	public void setTipoAviso(String tipoAviso) {
		this.tipoAviso = tipoAviso;
	}
	public String getArquivo() {
		return arquivo;
	}
	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public String getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}
	public String getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	public String getDataFim() {
		return dataFim;
	}
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
	public String getHoraFim() {
		return horaFim;
	}
	public void setHoraFim(String horaFim) {
		this.horaFim = horaFim;
	}
	public String getCondicaoSevera() {
		return condicaoSevera;
	}
	public void setCondicaoSevera(String condicaoSevera) {
		this.condicaoSevera = condicaoSevera;
	}
}
