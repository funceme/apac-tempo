package br.gov.funceme.database.model;

public interface GenericModel {
	
	public abstract String getTable(); 
	public abstract String getColumns(); 

}
