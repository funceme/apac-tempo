package br.gov.funceme.database.model;

import android.graphics.Bitmap;

public class Imagens {
	Bitmap imagem;
	String validade;
	String urlImagem;

	public Bitmap getImagem() {
		return imagem;
	}
	public void setImagem(Bitmap imagem) {
		this.imagem = imagem;
	}
	public String getValidade() {
		return validade;
	}
	public void setValidade(String validade) {
		this.validade = validade;
	}
	public String getUrlImagem() {
		return urlImagem;
	}
	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}
	public void clear(){
		this.imagem.recycle();
	}

}
