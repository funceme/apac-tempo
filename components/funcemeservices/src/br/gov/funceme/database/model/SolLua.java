package br.gov.funceme.database.model;

public class SolLua {
	
	private String dia;
	private String nascente;
	private String poente;
	private String lua;
	
	public String getDia() {
		return dia;
	}
	
	public void setDia(String dia) {
		this.dia = dia;
	}
	
	public String getNascente() {
		return nascente;
	}
	
	public void setNascente(String nascente) {
		this.nascente = nascente;
	}
	
	public String getPoente() {
		return poente;
	}
	
	public void setPoente(String poente) {
		this.poente = poente;
	}
	
	public String getLua() {
		return lua;
	}
	
	public void setLua(String lua) {
		this.lua = lua;
	}
}