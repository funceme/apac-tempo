package br.gov.funceme.database.model;

public class Porto {

	private int codigo; 
	private String nome;
	private String codigoMunicipio;
	private String dataInicio;
	private String dataFim;
	private double lat;
	private double lon;
	
	public static final String TABLE = "portos";
	public static final String ID = "codigo";
	public static final String COLUMNS = "codigo,nome,codigoMunicipio,dataInicio,dataFim,lat,lon";
	
	public double getLat() {
		return lat;
	}
	
	public void setLat(double lat) {
		this.lat = lat;
	}
	
	public double getLon() {
		return lon;
	}
	
	public void setLon(double d) {
		this.lon = d;
	}
	
	public int getCodigo() {
		return codigo;
	}
	
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCodigoMunicipio() {
		return codigoMunicipio;
	}
	
	public void setCodigoMunicipio(String codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}
	
	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
	
	@Override
	public boolean equals(Object object)
	{
		if(object.getClass().equals(this.getClass()))
		{
			Porto o = (Porto) object;
			if(o.getCodigo() == this.getCodigo())
				return true;
		}
		return false;
	}
}