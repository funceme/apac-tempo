package br.gov.funceme.database.model;

import java.lang.reflect.Field;

public class Cidade implements GenericModel {
	
	public static final String TABLE = "cidades";
	public static final String ID = "codigo";
	public static final String COLUMNS = "codigo,nome,ufCodigo,uf,ufNome,cat,lat,lon,atual,favorita,carregada";

	private String codigo;
	private String nome;
	private String ufCodigo;
	private String uf;
	private String ufNome;
	private String cat;
	private float lat;
	private float lon;
	private String atual;
	private int favorita;
	private int carregada;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUfCodigo() {
		return ufCodigo;
	}
	public void setUfCodigo(String ufCodigo) {
		this.ufCodigo = ufCodigo;
	}
	public String getUfNome() {
		return ufNome;
	}
	public void setUfNome(String ufNome) {
		this.ufNome = ufNome;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getCat() {
		return cat;
	}
	public void setCat(String cat) {
		this.cat = cat;
	}
	public float getLat() {
		return lat;
	}
	public void setLat(float lat) {
		this.lat = lat;
	}
	public float getLon() {
		return lon;
	}
	public void setLon(float lon) {
		this.lon = lon;
	}
	public String getAtual() {
		return atual;
	}
	public void setAtual(String atual) {
		this.atual = atual;
	}
	public int getFavorita() {
		return favorita;
	}
	public void setFavorita(int favorita) {
		this.favorita = favorita;
	}
	public int getCarregada() {
		return carregada;
	}
	public void setCarregada(int carregada) {
		this.carregada = carregada;
	}
	
	public String getColumns() {
		
		StringBuffer str = new StringBuffer();
		
		Field[] fields = this.getClass().getDeclaredFields();
		
		for(int i = 0; i < fields.length; i++) {
			str.append(fields[i].toString());
			if (i < (fields.length-1))
				str.append(", ");
	    }
		
		return str.toString();
	}
	
	@Override
	public boolean equals(Object object)
	{
		if(object.getClass().equals(this.getClass()))
		{
			Cidade o = (Cidade) object;
			if(o.getCodigo() == this.getCodigo())
				return true;
		}
		return false;
	}
	
	@Override
	public String getTable() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String toString() {
		return "[codigo: "+ codigo +
		" nome: " + nome +
		" ufCodigo: " + ufCodigo +
		" uf: " + uf +
		" ufNome: " + ufNome +
		" cat: " + cat +
		" lat: " + lat +
		" lon: "+ lon +
		" atual: " + atual +
		" favorita: " + favorita +
		" carregada: " + carregada + "]";
	}
}