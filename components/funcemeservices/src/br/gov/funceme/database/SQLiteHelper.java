package br.gov.funceme.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLiteHelper extends SQLiteOpenHelper {
	
	private DatabaseModel dbModel;
	
	public SQLiteHelper(Context context, DatabaseModel database) {
				
		super(context, database.getName(), null, database.getVersion());
		
		dbModel = database;
	}
	 
	@Override
	public void onCreate(SQLiteDatabase db) {
		
		Log.i("DB", "Criando banco de dados.");
		
		try {
			if (dbModel.getCreateSQL() == null)
				throw new Exception("String de criação do banco não definida.");
			
			String sqlCreate = dbModel.getCreateSQL();
			
			String[] createList = sqlCreate.split(";");
			
			for (String sql : createList) {
				db.execSQL(sql);
			}
			
			Log.i("DB", "Banco de dados criado com sucesso!");
			
		} catch (Exception e) {
			Log.e("DB_ERROR", e.getMessage());
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		try {
			if (dbModel.getUpdateSQL() == null)
				throw new Exception("String de update do banco não definida.");
			
			db.execSQL(dbModel.getUpdateSQL());
			onCreate(db);
			
		} catch (Exception e) {
			Log.e("DB_ERROR", e.getMessage());
		}
	}
}