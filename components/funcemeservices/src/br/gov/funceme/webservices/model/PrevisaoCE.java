package br.gov.funceme.webservices.model;

public class PrevisaoCE {

	private String dataPrevisao;
	private String diaPrevisao;
	private String previsao24horas;
	private String previsao24horas1;
	private String previsao24horas2;
	private String previsao24horas3;
	private String a;
	private String b;
	private String c;
	private String e;
	private String d;
	private String f;
	private String h;
	private String g;
	
	public String getDataPrevisao() {
		return dataPrevisao;
	}
	public void setDataPrevisao(String dataPrevisao) {
		this.dataPrevisao = dataPrevisao;
	}
	public String getDiaPrevisao() {
		return diaPrevisao;
	}
	public void setDiaPrevisao(String diaPrevisao) {
		this.diaPrevisao = diaPrevisao;
	}
	public String getPrevisao24horas() {
		return previsao24horas;
	}
	public void setPrevisao24horas(String previsao24horas) {
		this.previsao24horas = previsao24horas;
	}
	public String getPrevisao24horas1() {
		return previsao24horas1;
	}
	public void setPrevisao24horas1(String previsao24horas1) {
		this.previsao24horas1 = previsao24horas1;
	}
	public String getPrevisao24horas2() {
		return previsao24horas2;
	}
	public void setPrevisao24horas2(String previsao24horas2) {
		this.previsao24horas2 = previsao24horas2;
	}
	public String getPrevisao24horas3() {
		return previsao24horas3;
	}
	public void setPrevisao24horas3(String previsao24horas3) {
		this.previsao24horas3 = previsao24horas3;
	}
	public String getA() {
		return a;
	}
	public void setA(String a) {
		this.a = a;
	}
	public String getB() {
		return b;
	}
	public void setB(String b) {
		this.b = b;
	}
	public String getC() {
		return c;
	}
	public void setC(String c) {
		this.c = c;
	}
	public String getE() {
		return e;
	}
	public void setE(String e) {
		this.e = e;
	}
	public String getD() {
		return d;
	}
	public void setD(String d) {
		this.d = d;
	}
	public String getF() {
		return f;
	}
	public void setF(String f) {
		this.f = f;
	}
	public String getH() {
		return h;
	}
	public void setH(String h) {
		this.h = h;
	}
	public String getG() {
		return g;
	}
	public void setG(String g) {
		this.g = g;
	}
	
	
}
