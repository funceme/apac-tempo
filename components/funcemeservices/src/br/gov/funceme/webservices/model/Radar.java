package br.gov.funceme.webservices.model;

public class Radar {

	private String txt;
	private String img;
	
	public String getText() {
		return txt;
	}
	
	public void setText(String text) {
		this.txt = text;
	}
	
	public String getImg() {
		return img;
	}
	
	public void setImg(String img) {
		this.img = img;
	}
}