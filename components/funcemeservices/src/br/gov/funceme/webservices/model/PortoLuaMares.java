package br.gov.funceme.webservices.model;

import java.util.List;

public class PortoLuaMares {
	
	private String data;
	private String faseLua;
	private String valorMedio;
	private List<Mare> mares;
	
	public String getData() {
		return data;
	}
	
	public void setData(String data) {
		this.data = data;
	}
	
	public String getFaseLua() {
		return faseLua;
	}
	
	public void setFaseLua(String faseLua) {
		this.faseLua = faseLua;
	}
	
	public String getValorMedio() {
		return valorMedio;
	}
	
	public void setValorMedio(String valorMedio) {
		this.valorMedio = valorMedio;
	}
	
	public List<Mare> getMares() {
		return mares;
	}
	
	public void setMares(List<Mare> mares) {
		this.mares = mares;
	}
}