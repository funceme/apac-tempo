package br.gov.funceme.webservices.model;

public class CidadeLatLon {
	
	String codigo;
	String nome;
	String uf_codigo;
	String uf;
	String uf_nome;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUf_codigo() {
		return uf_codigo;
	}
	public void setUf_codigo(String uf_codigo) {
		this.uf_codigo = uf_codigo;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getUf_nome() {
		return uf_nome;
	}
	public void setUf_nome(String uf_nome) {
		this.uf_nome = uf_nome;
	}
}