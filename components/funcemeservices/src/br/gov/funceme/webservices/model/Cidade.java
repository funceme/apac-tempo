package br.gov.funceme.webservices.model;

public class Cidade {

	private String cod;
	private String nome;
	private String uf;
	private String cat;
	private String atual;
	
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getCat() {
		return cat;
	}
	public void setCat(String cat) {
		this.cat = cat;
	}
	public String getAtual() {
		return atual;
	}
	public void setAtual(String atual) {
		this.atual = atual;
	}	
	
	@Override
	public boolean equals(Object object)
	{
		if(object.getClass().equals(this.getClass()))
		{
			Cidade o = (Cidade) object;
			if(o.getCod() == this.getCod())
				return true;
		}
		return false;
	}
}