package br.gov.funceme.webservices.model.inmet;

public class Cidade {
	
	private int geocode;
	private String nome;
	private String uf;
	private float lat;
	private float lon;
	
	public int getGeocode() {
		return geocode;
	}
	public void setGeocode(int geocode) {
		this.geocode = geocode;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public float getLat() {
		return lat;
	}
	public void setLat(float lat) {
		this.lat = lat;
	}
	public float getLon() {
		return lon;
	}
	public void setLon(float lon) {
		this.lon = lon;
	}

}
