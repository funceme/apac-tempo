package br.gov.funceme.webservices.model.inmet;

public class PrevisaoRegiao {
	
	private String data;
	private String geocode;
	private String uf;
	private String resumo;
	private String tempo;
	private String temperatura;
	private String tendencia;
	private String tempMax;
	private String tempMin;
	private String direcaoVento;
	private String velocidadeVento;
	private String intensidadeVento;
	private String idIcone;
	private String diaSemana;
	private String umidadeMax;
	private String umidadeMin;
	private String visibilidade;
	private String sensacao;
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getGeocode() {
		return geocode;
	}
	public void setGeocode(String geocode) {
		this.geocode = geocode;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getResumo() {
		return resumo;
	}
	public void setResumo(String resumo) {
		this.resumo = resumo;
	}
	public String getTempo() {
		return tempo;
	}
	public void setTempo(String tempo) {
		this.tempo = tempo;
	}
	public String getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}
	public String getTendencia() {
		return tendencia;
	}
	public void setTendencia(String tendencia) {
		this.tendencia = tendencia;
	}
	public String getTempMax() {
		return tempMax;
	}
	public void setTempMax(String tempMax) {
		this.tempMax = tempMax;
	}
	public String getTempMin() {
		return tempMin;
	}
	public void setTempMin(String tempMin) {
		this.tempMin = tempMin;
	}
	public String getDirecaoVento() {
		return direcaoVento;
	}
	public void setDirecaoVento(String direcaoVento) {
		this.direcaoVento = direcaoVento;
	}
	public String getVelocidadeVento() {
		return velocidadeVento;
	}
	public void setVelocidadeVento(String velocidadeVento) {
		this.velocidadeVento = velocidadeVento;
	}
	public String getIntensidadeVento() {
		return intensidadeVento;
	}
	public void setIntensidadeVento(String intensidadeVento) {
		this.intensidadeVento = intensidadeVento;
	}
	public String getIdIcone() {
		return idIcone;
	}
	public void setIdIcone(String idIcone) {
		this.idIcone = idIcone;
	}
	public String getDiaSemana() {
		return diaSemana;
	}
	public void setDiaSemana(String diaSemana) {
		this.diaSemana = diaSemana;
	}
	public String getUmidadeMax() {
		return umidadeMax;
	}
	public void setUmidadeMax(String umidade) {
		this.umidadeMax = umidade;
	}
	public String getUmidadeMin() {
		return umidadeMin;
	}
	public void setUmidadeMin(String umidade) {
		this.umidadeMin = umidade;
	}
	public String getVisibilidade() {
		return visibilidade;
	}
	public void setVisibilidade(String visibilidade) {
		this.visibilidade = visibilidade;
	}
	public String getSensacao() {
		return sensacao;
	}
	public void setSensacao(String sensacao) {
		this.sensacao = sensacao;
	}
	
	@Override
	public String toString() {
		return "PrevisaoRegioesBeans [data=" +data
				+ ", geocode="+ geocode
				+ ", uf=" + uf
				+ ", resumo=" + resumo
				+ ", tempo=" + tempo
				+ ", temperatura=" + temperatura
				+ ", tendencia=" + tendencia
				+ ", tempMax=" + tempMax
				+ ", tempMin=" + tempMin
				+ ", direcaoVento=" + direcaoVento
				+ ", velocidadeVento=" + velocidadeVento
				+ ", intensidadeVento=" + intensidadeVento
				+ ", idIcone=" + idIcone
				+ ", diaSemana=" + diaSemana
				+ ", umidadeMax=" + umidadeMax
				+ ", umidadeMin=" + umidadeMin
				+ ", visibilidade=" + visibilidade
				+ ", sensacao=" + sensacao
				+ "]";
	}
}