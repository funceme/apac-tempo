package br.gov.funceme.webservices.model;

public class Porto {

	private String id;
	private double lat;
	private double lon;
	private int codigo; 
	private String nome;
	private int codMunicipio;
	private String dataIni;
	private String dataFim;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public double getLat() {
		return lat;
	}
	
	public void setLat(double lat) {
		this.lat = lat;
	}
	
	public double getLon() {
		return lon;
	}
	
	public void setLon(double d) {
		this.lon = d;
	}
	
	public int getCodigo() {
		return codigo;
	}
	
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getCodMunicipio() {
		return codMunicipio;
	}
	
	public void setCodMunicipio(int codMunicipio) {
		this.codMunicipio = codMunicipio;
	}
	
	public String getDataIni() {
		return dataIni;
	}

	public void setDataIni(String dataIni) {
		this.dataIni = dataIni;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
	
	@Override
	public boolean equals(Object object)
	{
		if(object.getClass().equals(this.getClass()))
		{
			Porto o = (Porto) object;
			if(o.getId() == this.getId())
				return true;
		}
		return false;
	}
}