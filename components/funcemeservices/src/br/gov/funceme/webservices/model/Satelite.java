package br.gov.funceme.webservices.model;

public class Satelite {
	
	private String text;
	private String img;
	private String anomesdia;
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getAnomesdia() {
		return anomesdia;
	}
	public void setAnomesdia(String anomesdia) {
		this.anomesdia = anomesdia;
	}
}
