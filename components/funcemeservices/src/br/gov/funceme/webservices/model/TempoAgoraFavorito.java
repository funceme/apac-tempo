package br.gov.funceme.webservices.model;

public class TempoAgoraFavorito {

	private String codigo;
	private String codigoMunicipio;
	private String nome;
	private String latitude;
	private String longitude;
	private String imagem;
	private String temperatura;
	private String temperaturaMin;
	private String temperaturaMax;
	private String umidade;
	private String umidadeMin;
	private String umidadeMax;
	private String ventoVel;
	private String ventoVelMin;
	private String dirVentoVelMin;
	private String ventoVelMax;
	private String dirVentoVelMax;
	private String ventoDir;
	private String chuva;
	private String chuvaAcum;
	private String pressao;
	private String pressaoMin;
	private String pressaoMax;
	private String rad;
	private String radMin;
	private String radMax;
	private String radAcum;
	private String atualizacao;
	private String imgFundo;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getCodigoMunicipio() {
		return codigoMunicipio;
	}
	public void setCodigoMunicipio(String codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public String getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}
	public String getTemperaturaMin() {
		return temperaturaMin;
	}
	public void setTemperaturaMin(String temperaturaMin) {
		this.temperaturaMin = temperaturaMin;
	}
	public String getTemperaturaMax() {
		return temperaturaMax;
	}
	public void setTemperaturaMax(String temperaturaMax) {
		this.temperaturaMax = temperaturaMax;
	}
	public String getUmidade() {
		return umidade;
	}
	public void setUmidade(String umidade) {
		this.umidade = umidade;
	}
	public String getUmidadeMin() {
		return umidadeMin;
	}
	public void setUmidadeMin(String umidadeMin) {
		this.umidadeMin = umidadeMin;
	}
	public String getUmidadeMax() {
		return umidadeMax;
	}
	public void setUmidadeMax(String umidadeMax) {
		this.umidadeMax = umidadeMax;
	}
	public String getVentoVel() {
		return ventoVel;
	}
	public void setVentoVel(String ventoVel) {
		this.ventoVel = ventoVel;
	}
	public String getVentoVelMin() {
		return ventoVelMin;
	}
	public void setVentoVelMin(String ventoVelMin) {
		this.ventoVelMin = ventoVelMin;
	}
	public String getDirVentoVelMin() {
		return dirVentoVelMin;
	}
	public void setDirVentoVelMin(String dirVentoVelMin) {
		this.dirVentoVelMin = dirVentoVelMin;
	}
	public String getVentoVelMax() {
		return ventoVelMax;
	}
	public void setVentoVelMax(String ventoVelMax) {
		this.ventoVelMax = ventoVelMax;
	}
	public String getDirVentoVelMax() {
		return dirVentoVelMax;
	}
	public void setDirVentoVelMax(String dirVentoVelMax) {
		this.dirVentoVelMax = dirVentoVelMax;
	}
	public String getVentoDir() {
		return ventoDir;
	}
	public void setVentoDir(String ventoDir) {
		this.ventoDir = ventoDir;
	}
	public String getChuva() {
		return chuva;
	}
	public void setChuva(String chuva) {
		this.chuva = chuva;
	}
	public String getChuvaAcum() {
		return chuvaAcum;
	}
	public void setChuvaAcum(String chuvaAcum) {
		this.chuvaAcum = chuvaAcum;
	}
	public String getPressao() {
		return pressao;
	}
	public void setPressao(String pressao) {
		this.pressao = pressao;
	}
	public String getPressaoMin() {
		return pressaoMin;
	}
	public void setPressaoMin(String pressaoMin) {
		this.pressaoMin = pressaoMin;
	}
	public String getPressaoMax() {
		return pressaoMax;
	}
	public void setPressaoMax(String pressaoMax) {
		this.pressaoMax = pressaoMax;
	}
	public String getRad() {
		return rad;
	}
	public void setRad(String rad) {
		this.rad = rad;
	}
	public String getRadMin() {
		return radMin;
	}
	public void setRadMin(String radMin) {
		this.radMin = radMin;
	}
	public String getRadMax() {
		return radMax;
	}
	public void setRadMax(String radMax) {
		this.radMax = radMax;
	}
	public String getRadAcum() {
		return radAcum;
	}
	public void setRadAcum(String radAcum) {
		this.radAcum = radAcum;
	}
	public String getAtualizacao() {
		return atualizacao;
	}
	public void setAtualizacao(String atualizacao) {
		this.atualizacao = atualizacao;
	}
	public String getImgFundo() {
		return imgFundo;
	}
	public void setImgFundo(String imgFundo) {
		this.imgFundo = imgFundo;
	}
	
	@Override
	public boolean equals(Object object)
	{
		if(object.getClass().equals(this.getClass()))
		{
			TempoAgora o = (TempoAgora) object;
			if(o.getCodigo() == this.getCodigo())
				return true;
		}
		return false;
	}
}