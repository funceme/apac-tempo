package br.gov.funceme.webservices.service.inmet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.gov.funceme.database.model.inmet.Aviso;
import br.gov.funceme.database.model.inmet.PrevisaoRegiao;
import br.gov.funceme.funcemeservices.R;
import br.gov.funceme.webservices.ws.AbstractService;
import br.gov.funceme.webservices.ws.GenericWebService;

public class PrevisaoRegioesService extends AbstractService {
	
	private String url;
	private Context context;
	
	public PrevisaoRegioesService(Context context) {
		this.context = context;
	}

	@Override
	public GenericWebService prepareWebService() {
		
		GenericWebService gWS = new GenericWebService(this.url, GenericWebService.GET);

		return gWS;
	}
	
	public void getPrevisao() {
		
		this.url = context.getString(R.string.URL_PREVISAO_REGIOES);
		
		super.makeRequest();
	}

	@Override
	public Object parseResponse(String response) {
		
		JSONObject jsonResponse = null;
		
		Map<String, ArrayList<PrevisaoRegiao>> map = new HashMap<String, ArrayList<PrevisaoRegiao>>();
		
		try {
			
			if(response == null)
				throw new Exception("Oops! Resposta vazia para a solicita��o de previs~ao por regiao.");
			
			jsonResponse = new JSONObject(response);
			
			Iterator<?> regioes = jsonResponse.keys();
			
			while(regioes.hasNext()){
		        
				String regiao = (String)regioes.next();
		        
		        JSONObject datasObj = jsonResponse.getJSONObject(regiao);
		        
		        Iterator<?> datas = datasObj.keys();
		        
		        ArrayList<PrevisaoRegiao> previsoes = new ArrayList<PrevisaoRegiao>(datasObj.length()); 
		        
		        while(datas.hasNext()){
		        	
		        	String data = (String)datas.next();
		        	
		        	JSONObject previsaoObj = datasObj.getJSONObject(data);
		        	
		        	PrevisaoRegiao obj = new PrevisaoRegiao();
		        			        	
		        	obj.setData(data);
		        	obj.setUf(previsaoObj.getString("uf"));
		        	obj.setResumo(previsaoObj.getString("resumo"));
		        	obj.setTempo(previsaoObj.getString("tempo"));
		        	obj.setTempMax(previsaoObj.getString("temp_max"));
		        	obj.setTempMin(previsaoObj.getString("temp_min"));
		        	obj.setDirecaoVento(previsaoObj.getString("dir_vento"));
		        	obj.setIdIcone(previsaoObj.getString("icone"));
		        	obj.setDiaSemana(previsaoObj.getString("dia_semana"));
		        	obj.setUmidadeMax(previsaoObj.getString("umidade_max"));
		        	obj.setUmidadeMin(previsaoObj.getString("umidade_min"));
		        	
		        	previsoes.add(obj);
		        }
		        map.put(regiao, previsoes);
		    }
		} catch(Exception e)
		{
			Log.d("DEBUG", e.getMessage());
		}
		return map;
	}
}