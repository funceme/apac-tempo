package br.gov.funceme.webservices.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.gov.funceme.database.model.Previsao;
import br.gov.funceme.database.model.PrevisaoDia;
import br.gov.funceme.funcemeservices.R;
import br.gov.funceme.webservices.ws.AbstractService;
import br.gov.funceme.webservices.ws.GenericWebService;

public class PrevisaoService extends AbstractService {
	
	private String url;
	private Context context;
	
	private String codigoMunicipio;
	private String dataInicio;
	private String dataFim;
	
	public PrevisaoService(Context context) {
		this.context = context;
	}
	
	public void getForecast(String codigoMunicipio, String dataInicio, String dataFim) {
		
		url = context.getString(R.string.URL_FORECAST);
		
		this.codigoMunicipio = codigoMunicipio;
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
		
		super.makeRequest();
	}
	
	@Override
	public GenericWebService prepareWebService() {
		
		GenericWebService gWS = new GenericWebService(this.url, GenericWebService.GET);
		
		gWS.adicionarParametro("codMun", this.codigoMunicipio);
		gWS.adicionarParametro("dataIni", this.dataInicio);
		gWS.adicionarParametro("dataFim", this.dataFim);
		
		return gWS;
	}

	@Override
	public Object parseResponse(String response) {
		
		return response;
		
	}
	
	public static List<PrevisaoDia> parse(String response) {
		
		List<PrevisaoDia> forecastDays = null;

		JSONObject jsonResponse = null;
		try {
			
			if(response == null)
				throw new Exception("Oops! Resposta vazia para a solicitação de pevisão.");
			
			jsonResponse = new JSONObject(response);
			
			forecastDays = new ArrayList<PrevisaoDia>();
			
			Iterator<?> keys = jsonResponse.keys();
			
			while( keys.hasNext() ){
				
	            String key = (String)keys.next();
	            
	            if( jsonResponse.get(key) instanceof JSONObject ){
	            	
	            	JSONObject forecastDayJSON = (JSONObject) jsonResponse.get(key);
	            	
	            	Previsao forecastDia 		= null;
	            	
	            	if (forecastDayJSON.has("dia")) {
	            	
		            	JSONObject forecastDiaJSON = (JSONObject) forecastDayJSON.getJSONObject("dia");
		            	
		            	forecastDia = new Previsao();
		            	
		            	if (forecastDiaJSON.has("codigo")) 					forecastDia.setCodigo(forecastDiaJSON.getString("codigo"));
		            	if (forecastDiaJSON.has("rodadaIni")) 				forecastDia.setRodadaIni(forecastDiaJSON.getString("rodadaIni"));
		            	if (forecastDiaJSON.has("rodadaFim")) 				forecastDia.setRodadaFim(forecastDiaJSON.getString("rodadaFim"));
		            	if (forecastDiaJSON.has("modelo")) 					forecastDia.setModelo(forecastDiaJSON.getString("modelo"));
		            	if (forecastDiaJSON.has("tipoArea")) 				forecastDia.setTipoArea(forecastDiaJSON.getString("tipoArea"));
		            	if (forecastDiaJSON.has("area")) 					forecastDia.setArea(forecastDiaJSON.getString("area"));
		            	if (forecastDiaJSON.has("dataIni")) 				forecastDia.setDataIni(forecastDiaJSON.getString("dataIni"));
		            	if (forecastDiaJSON.has("dataFim")) 				forecastDia.setDataFim(forecastDiaJSON.getString("dataFim"));
		            	if (forecastDiaJSON.has("temperaturaMax")) 			forecastDia.setTemperaturaMax(forecastDiaJSON.getString("temperaturaMax"));
		            	if (forecastDiaJSON.has("temperaturaMin")) 			forecastDia.setTemperaturaMin(forecastDiaJSON.getString("temperaturaMin"));
		            	if (forecastDiaJSON.has("precipitacaoAcumulada")) 	forecastDia.setPrecipitacaoAcumulada(forecastDiaJSON.getString("precipitacaoAcumulada"));
		            	if (forecastDiaJSON.has("precipitacaoMin")) 		forecastDia.setPrecipitacaoMin(forecastDiaJSON.getString("precipitacaoMin"));
		            	if (forecastDiaJSON.has("precipitacaoMax")) 		forecastDia.setPrecipitacaoMax(forecastDiaJSON.getString("precipitacaoMax"));
		            	if (forecastDiaJSON.has("ventoVelMin")) 			forecastDia.setVentoVelMin(forecastDiaJSON.getString("ventoVelMin"));
		            	if (forecastDiaJSON.has("ventoVelMax")) 			forecastDia.setVentoVelMax(forecastDiaJSON.getString("ventoVelMax"));
		            	if (forecastDiaJSON.has("ventoDirecao")) 			forecastDia.setVentoDirecao(forecastDiaJSON.getString("ventoDirecao"));
		            	if (forecastDiaJSON.has("coberturaNuvensMin")) 		forecastDia.setCoberturaNuvensMin(forecastDiaJSON.getString("coberturaNuvensMin"));
		            	if (forecastDiaJSON.has("coberturaNuvensMax")) 		forecastDia.setCoberturaNuvensMax(forecastDiaJSON.getString("coberturaNuvensMax"));
		            	if (forecastDiaJSON.has("umidadeRelativaMin")) 		forecastDia.setUmidadeRelativaMin(forecastDiaJSON.getString("umidadeRelativaMin"));
		            	if (forecastDiaJSON.has("umidadeRelativaMax")) 		forecastDia.setUmidadeRelativaMax(forecastDiaJSON.getString("umidadeRelativaMax"));
		            	if (forecastDiaJSON.has("radiacaoUv")) 				forecastDia.setRadiacaoUv(forecastDiaJSON.getString("radiacaoUv"));
						
		            	if (forecastDiaJSON.has("descricao")) {
							
		            		JSONObject descricaoDia = forecastDiaJSON.getJSONObject("descricao");
			            	
		            		if (descricaoDia.has("codigo") && descricaoDia.has("descricao") && descricaoDia.has("img")) {
		            			forecastDia.setImgCodigo(descricaoDia.getString("codigo"));
		            			forecastDia.setDescricao(descricaoDia.getString("descricao"));
		            			forecastDia.setImg(descricaoDia.getString("img"));
		            		}
		            	}
	            	}
	            	
	            	PrevisaoDia forecastDay = new PrevisaoDia();
	            	
	            	String[] keyParts = key.split("-");
	            	
	            	if(keyParts.length == 3)
	            		forecastDay.setData(keyParts[2]+"/"+keyParts[1]+"/"+keyParts[0]);
	            	else 
	            		forecastDay.setData(key);
	            	
	            	forecastDay.setDia(forecastDia);
	            	
	            	forecastDays.add(forecastDay);
	            }
	        }			
		} catch(Exception e)
		{
			forecastDays = null;
			Log.d("DEBUG", e.getMessage());
		}
		
		return forecastDays;
	}
}