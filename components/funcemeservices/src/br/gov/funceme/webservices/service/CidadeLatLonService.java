package br.gov.funceme.webservices.service;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.gov.funceme.database.model.Cidade;
import br.gov.funceme.funcemeservices.R;
import br.gov.funceme.webservices.ws.AbstractService;
import br.gov.funceme.webservices.ws.GenericWebService;

public class CidadeLatLonService extends AbstractService {

	private String url;
	private Context context;
	
	private float latitude;
	private float longitude;
	
	public CidadeLatLonService(Context context) {
		this.context = context;
	}
	
	public void getCity(float lat, float lon) {
		
		url = context.getString(R.string.URL_CITY_LAT_LON);
		
		this.latitude = lat;
		this.longitude = lon;
		
		super.makeRequest();
	}
	
	@Override
	public GenericWebService prepareWebService() {
		
		GenericWebService gWS = new GenericWebService(this.url, GenericWebService.GET);
		
		gWS.adicionarParametro("lat" , ""+this.latitude);
		gWS.adicionarParametro("lon", ""+this.longitude);
		
		return gWS;
	}

	@Override
	public Cidade parseResponse(String response) {
		
		JSONObject jsonResponse = null;
		
		Cidade city = new Cidade();
		
		try {
			
			if(response == null)
				throw new Exception("Oops! Resposta vazia para a solicitação da cidade.");
			
			jsonResponse = new JSONObject(response);
			
			if (jsonResponse.has("cidade_geocode")) city.setCodigo(jsonResponse.getString("cidade_geocode"));
			if (jsonResponse.has("cidade_nome")) 	city.setNome(jsonResponse.getString("cidade_nome"));
			if (jsonResponse.has("uf_codigo")) 		city.setUfCodigo(jsonResponse.getString("uf_codigo"));
			if (jsonResponse.has("uf_abr")) 		city.setUf(jsonResponse.getString("uf_abr"));
			if (jsonResponse.has("uf_nome")) 		city.setUfNome(jsonResponse.getString("uf_nome"));
			
		} catch(Exception e)
		{
			Log.d("DEBUG", e.getMessage());
		}
		
		return city;
	}
}