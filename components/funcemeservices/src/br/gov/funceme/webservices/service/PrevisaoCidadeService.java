package br.gov.funceme.webservices.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.gov.funceme.database.model.Cidade;
import br.gov.funceme.funcemeservices.R;
import br.gov.funceme.webservices.ws.AbstractService;
import br.gov.funceme.webservices.ws.GenericWebService;

public class PrevisaoCidadeService extends AbstractService {

	private String url;
	private Context context;
	
	public PrevisaoCidadeService(Context context) {
		this.context = context;
	}
	
	public void getForecastCities() {
		
		url = context.getString(R.string.URL_FORECAST_CITIES);
		super.makeRequest();
	}
	
	@Override
	public GenericWebService prepareWebService() {
		
		GenericWebService gWS = new GenericWebService(this.url, GenericWebService.GET);
		return gWS;
	}

	@Override
	public Object parseResponse(String response) {
		
		List<Cidade> cities = null;

		JSONObject jsonResponse = null;
		try {
			
			if(response == null)
				throw new Exception("Oops! Resposta vazia para a solicitação de cidades da previsão.");
			
			jsonResponse = new JSONObject(response);
			
			if (!jsonResponse.has("cidades"))
				throw new Exception("Oops! Nenhuma cidade retornada.");

			cities = new ArrayList<Cidade>();
			
			JSONArray citiesJSON = jsonResponse.getJSONArray("cidades");
			
			for(int i = 0; i < citiesJSON.length(); i++) {
				
				JSONObject cityJSON = citiesJSON.getJSONObject(i);
				
				Cidade city = new Cidade();
				
				if (!(cityJSON.has("nome") && cityJSON.has("cod") && cityJSON.has("cat")))
					continue;
				
				String cidade = cityJSON.getString("nome");
				String uf = cidade.substring(cidade.length()-2, cidade.length());
				
				city.setCodigo(cityJSON.getString("cod"));
				city.setNome(cidade);
				city.setCat(cityJSON.getString("cat"));
				city.setUf(uf);
				
				cities.add(city);
			}
			
		} catch(Exception e)
		{
			cities = null;
			Log.d("DEBUG", e.getMessage());
		}
		
		return cities;
	}
}