package br.gov.funceme.webservices.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.gov.funceme.database.model.Mare;
import br.gov.funceme.database.model.PortoLuaMares;
import br.gov.funceme.funcemeservices.R;
import br.gov.funceme.webservices.ws.AbstractService;
import br.gov.funceme.webservices.ws.GenericWebService;

public class PortosLuaMaresService extends AbstractService {

	private String url;
	private String data;
	private Context context;
	
	private int codigoCidade;
	
	public PortosLuaMaresService(Context context) {
		this.context = context;
	}
	
	public void getLuaMares(int codigoCidade) {
		
		url = context.getString(R.string.URL_TIDES);
		
		this.codigoCidade = codigoCidade;
		
		this.data = null;
		
		super.makeRequest();
	}
	
	public void getLuaMares(int codigoCidade, String data) {
		
		url = context.getString(R.string.URL_TIDES);
		
		this.codigoCidade = codigoCidade;
		
		this.data = data;
		
		super.makeRequest();
	}
	
	@Override
	public GenericWebService prepareWebService() {
		
		GenericWebService gWS;
		
		if (data == null) {
			gWS = new GenericWebService(new StringBuilder(this.url).append(this.codigoCidade).append("/3").toString(), GenericWebService.GET);
		}else{
			gWS = new GenericWebService(new StringBuilder(this.url).append(this.codigoCidade).append("/3").append("/"+data).toString(), GenericWebService.GET);
		}
		
		return gWS;
	}

	@Override
	public Object parseResponse(String response) {
		
		return response;
	}
}