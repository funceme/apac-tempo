package br.gov.funceme.webservices.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.gov.funceme.database.model.Porto;
import br.gov.funceme.funcemeservices.R;
import br.gov.funceme.webservices.ws.AbstractService;
import br.gov.funceme.webservices.ws.GenericWebService;

public class PortosService extends AbstractService {

	private String url;
	private Context context;
	
	public PortosService(Context context) {
		this.context = context;
	}
	
	public void getHarbors() {
		
		url = context.getString(R.string.URL_PORTOS);
		super.makeRequest();
	}
	
	@Override
	public GenericWebService prepareWebService() {
		
		GenericWebService gWS = new GenericWebService(this.url, GenericWebService.GET);
		
		return gWS;
	}
	

	@Override
	public Object parseResponse(String response) {
		
		return response;
		
	}
	
	static public List<Porto> parse(String response) {
		
		List<Porto> harborList = null;

		JSONObject jsonResponse = null;
		try {
			
			if(response == null)
				throw new Exception("Oops! Resposta vazia para a solicitação de portos.");
			
			jsonResponse = new JSONObject(response);
			
			harborList = new ArrayList<Porto>();
			
			if (!jsonResponse.has("portos"))
				throw new Exception("Oops! Nenhum porto retornado.");
			
			JSONArray listJSON = jsonResponse.getJSONArray("portos");
			
			for(int i = 0; i < listJSON.length(); i++) {
				
				JSONObject objJSON = listJSON.getJSONObject(i);
				
				Porto porto = new Porto();
				
				if (objJSON.has("lon")) porto.setLon(objJSON.getDouble("lon"));
				if (objJSON.has("lat")) porto.setLat(objJSON.getDouble("lat"));
				if (objJSON.has("nome")) porto.setNome(objJSON.getString("nome"));
				if (objJSON.has("codMunicipio")) porto.setCodigoMunicipio(objJSON.getString("codMunicipio"));
				if (objJSON.has("dataIni")) porto.setDataInicio(objJSON.getString("dataIni"));
				if (objJSON.has("dataFim")) porto.setDataFim(objJSON.getString("dataFim"));
				
				harborList.add(porto);
			}
			
		} catch(Exception e)
		{
			harborList = null;
			Log.d("DEBUG", e.getMessage());
		}
		
		return harborList;
	}
}