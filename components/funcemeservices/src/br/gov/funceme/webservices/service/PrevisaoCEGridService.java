
package br.gov.funceme.webservices.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.gov.funceme.database.model.PrevisaoCE;
import br.gov.funceme.funcemeservices.R;
import br.gov.funceme.webservices.ws.AbstractService;
import br.gov.funceme.webservices.ws.GenericWebService;

public class PrevisaoCEGridService extends AbstractService {

	private String url;
	private Context context;
	
	public PrevisaoCEGridService(Context context) {
		this.context = context;
	}
	
	public void getForecast() {
		
		url = context.getString(R.string.URL_FORECAST_CE_GRID);
		super.makeRequest();
	}
	
	@Override
	public GenericWebService prepareWebService() {
		
		GenericWebService gWS = new GenericWebService(this.url, GenericWebService.GET);		
		return gWS;
	}

	@Override
	public String parseResponse(String response) {
		
		return response;
	}
	
	private String formatarData(String data) throws ParseException 
	{
		DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		fromFormat.setLenient(false);
		
		DateFormat toFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
		toFormat.setLenient(false);
		
		Date date = fromFormat.parse(data);
		
		return toFormat.format(date);
	}
}