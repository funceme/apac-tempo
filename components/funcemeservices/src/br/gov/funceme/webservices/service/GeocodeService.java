package br.gov.funceme.webservices.service;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.gov.funceme.database.model.Cidade;
import br.gov.funceme.funcemeservices.R;
import br.gov.funceme.webservices.ws.AbstractService;
import br.gov.funceme.webservices.ws.GenericWebService;

public class GeocodeService extends AbstractService {
	
	private String url;
	private Context context;
	
	private float lat;
	private float lon;
	
	public void getCidade(float lat, float lon) {
		
		url = context.getString(R.string.url_geocode);
		
		this.lat = lat;
		this.lon = lon;
		
		super.makeRequest();
	}
	
	public GeocodeService(Context context) {
		this.context = context;
	}

	@Override
	public GenericWebService prepareWebService() {
		GenericWebService gWS = new GenericWebService(this.url, GenericWebService.GET);
		
		gWS.adicionarParametro("lat" , ""+this.lat);
		gWS.adicionarParametro("lon" , ""+this.lon);
		
		return gWS;
	}

	@Override
	public Cidade parseResponse(String response) {
		
		JSONObject jsonResponse = null;
		
		Cidade item = new Cidade();
		
		try {
			
			if(response == null)
				throw new Exception("Oops! Resposta vazia para a solicitação de cidades.");
			
			jsonResponse = new JSONObject(response);

			if (jsonResponse.has("cidade_geocode")) item.setCodigo(jsonResponse.getString("cidade_geocode"));
			if (jsonResponse.has("cidade_nome")) 	item.setNome(jsonResponse.getString("cidade_nome"));
			if (jsonResponse.has("uf_abr")) 		item.setUf(jsonResponse.getString("uf_abr"));
			
		} catch(Exception e)
		{
			Log.d("DEBUG", e.getMessage());
		}
		
		return item;
	}
}