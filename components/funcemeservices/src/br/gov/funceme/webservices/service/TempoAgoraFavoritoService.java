package br.gov.funceme.webservices.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.gov.funceme.database.model.Cidade;
import br.gov.funceme.database.model.TempoAgoraFavorito;
import br.gov.funceme.funcemeservices.R;
import br.gov.funceme.webservices.ws.AbstractService;
import br.gov.funceme.webservices.ws.GenericWebService;

public class TempoAgoraFavoritoService extends AbstractService {

	private String url;
	private Context context;
	
	private List<Cidade> favorites;

	public TempoAgoraFavoritoService(Context context) {
		this.context = context;
	}
	
	public void getWeatherNowFormFavorites(List<Cidade> cidades) {
		
		url = context.getString(R.string.URL_WEATHER_NOW_FAVORITES);
		
		this.favorites = cidades;
		
		super.makeRequest();
	}
	
	@Override
	public GenericWebService prepareWebService() {
		
		GenericWebService gWS = new GenericWebService(this.url, GenericWebService.GET);
		
		for (Cidade favorite: favorites) {
			gWS.adicionarParametro("codigo[]", favorite.getCodigo());
		}

		return gWS;
	}

	@Override
	public Object parseResponse(String response) {
		
		List<TempoAgoraFavorito> weatherNowFavorites = null;

		JSONArray jsonResponse = null;
		try {
			
			if(response == null)
				throw new Exception("Oops! Resposta vazia para a solicitação de tempo agora das cidades favoritas.");
			
			jsonResponse = new JSONArray(response);
			
			weatherNowFavorites = new ArrayList<TempoAgoraFavorito>();
			
			for(int i = 0; i < jsonResponse.length(); i++) {
				
				JSONObject weatherNowFavoriteJSON = jsonResponse.getJSONObject(i);
				
				TempoAgoraFavorito weatherNowFavorite = new TempoAgoraFavorito();
				
				if (weatherNowFavoriteJSON.has("codigo")) 				weatherNowFavorite.setCodigo(weatherNowFavoriteJSON.getString("codigo"));
				if (weatherNowFavoriteJSON.has("codigo_municipio")) 	weatherNowFavorite.setCodigoMunicipio(weatherNowFavoriteJSON.getString("codigo_municipio"));
				if (weatherNowFavoriteJSON.has("nome")) 				weatherNowFavorite.setNome(weatherNowFavoriteJSON.getString("nome"));
				if (weatherNowFavoriteJSON.has("latitude")) 			weatherNowFavorite.setLatitude(weatherNowFavoriteJSON.getString("latitude"));
				if (weatherNowFavoriteJSON.has("longitude")) 			weatherNowFavorite.setLongitude(weatherNowFavoriteJSON.getString("longitude"));
				if (weatherNowFavoriteJSON.has("imagem")) 				weatherNowFavorite.setImagem(weatherNowFavoriteJSON.getString("imagem"));
				if (weatherNowFavoriteJSON.has("temperatura")) 			weatherNowFavorite.setTemperatura(weatherNowFavoriteJSON.getString("temperatura"));
				if (weatherNowFavoriteJSON.has("temperatura_min")) 		weatherNowFavorite.setTemperaturaMin(weatherNowFavoriteJSON.getString("temperatura_min"));
				if (weatherNowFavoriteJSON.has("temperatura_max")) 		weatherNowFavorite.setTemperaturaMax(weatherNowFavoriteJSON.getString("temperatura_max"));
				if (weatherNowFavoriteJSON.has("umidade")) 				weatherNowFavorite.setUmidade(weatherNowFavoriteJSON.getString("umidade"));
				if (weatherNowFavoriteJSON.has("umidade_min")) 			weatherNowFavorite.setUmidadeMin(weatherNowFavoriteJSON.getString("umidade_min"));
				if (weatherNowFavoriteJSON.has("umidade_max")) 			weatherNowFavorite.setUmidadeMin(weatherNowFavoriteJSON.getString("umidade_max"));
				if (weatherNowFavoriteJSON.has("vento_vel")) 			weatherNowFavorite.setVentoVel(weatherNowFavoriteJSON.getString("vento_vel"));
				if (weatherNowFavoriteJSON.has("vento_vel_min")) 		weatherNowFavorite.setVentoVelMin(weatherNowFavoriteJSON.getString("vento_vel_min"));
				if (weatherNowFavoriteJSON.has("dir_vento_vel_min")) 	weatherNowFavorite.setDirVentoVelMin(weatherNowFavoriteJSON.getString("dir_vento_vel_min"));
				if (weatherNowFavoriteJSON.has("vento_vel_max")) 		weatherNowFavorite.setVentoVelMax(weatherNowFavoriteJSON.getString("vento_vel_max"));
				if (weatherNowFavoriteJSON.has("dir_vento_vel_max")) 	weatherNowFavorite.setDirVentoVelMin(weatherNowFavoriteJSON.getString("dir_vento_vel_max"));
				if (weatherNowFavoriteJSON.has("vento_dir")) 			weatherNowFavorite.setVentoDir(weatherNowFavoriteJSON.getString("vento_dir"));
				if (weatherNowFavoriteJSON.has("chuva")) 				weatherNowFavorite.setChuva(weatherNowFavoriteJSON.getString("chuva"));
				if (weatherNowFavoriteJSON.has("chuva_acum")) 			weatherNowFavorite.setChuvaAcum(weatherNowFavoriteJSON.getString("chuva_acum"));
				if (weatherNowFavoriteJSON.has("pressao")) 				weatherNowFavorite.setPressao(weatherNowFavoriteJSON.getString("pressao"));
				if (weatherNowFavoriteJSON.has("pressao_min")) 			weatherNowFavorite.setPressaoMin(weatherNowFavoriteJSON.getString("pressao_min"));
				if (weatherNowFavoriteJSON.has("pressao_max")) 			weatherNowFavorite.setPressaoMax(weatherNowFavoriteJSON.getString("pressao_max"));
				if (weatherNowFavoriteJSON.has("rad")) 					weatherNowFavorite.setRad(weatherNowFavoriteJSON.getString("rad"));
				if (weatherNowFavoriteJSON.has("rad_min")) 				weatherNowFavorite.setRadMin(weatherNowFavoriteJSON.getString("rad_min"));
				if (weatherNowFavoriteJSON.has("rad_max")) 				weatherNowFavorite.setRadMax(weatherNowFavoriteJSON.getString("rad_max"));
				if (weatherNowFavoriteJSON.has("rad_acum")) 			weatherNowFavorite.setRadAcum(weatherNowFavoriteJSON.getString("rad_acum"));
				if (weatherNowFavoriteJSON.has("atualizacao")) 			weatherNowFavorite.setAtualizacao(weatherNowFavoriteJSON.getString("atualizacao"));
				if (weatherNowFavoriteJSON.has("img_fundo")) 			weatherNowFavorite.setImgFundo(weatherNowFavoriteJSON.getString("img_fundo"));
				
				weatherNowFavorites.add(weatherNowFavorite);
			}
			
		} catch(Exception e)
		{
			weatherNowFavorites = null;
			Log.d("DEBUG", e.getMessage());
		}
		
		return weatherNowFavorites;
	}
}