package br.gov.funceme.webservices.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.gov.funceme.database.model.Radar;
import br.gov.funceme.database.model.RadarResponse;
import br.gov.funceme.funcemeservices.R;
import br.gov.funceme.webservices.ws.AbstractService;
import br.gov.funceme.webservices.ws.GenericWebService;

public class RadarXService extends AbstractService {

	private String url;
	private Context context;
	GenericWebService gWS;
	
	private final String PCAPPI = "02000";
	
	private int limitImages;
	private int limitTime;
	
	public RadarXService(Context context) {
		this.context = context;
	}
	
	public void getListImagesRadar(int limitImages, int limitTime) {
		
		this.limitImages = limitImages;
		this.limitTime = limitTime;
		
		url = context.getString(R.string.URL_RADAR_X);
		super.makeRequest();
	}
	
	@Override
	public GenericWebService prepareWebService() {
		
		gWS = new GenericWebService(this.url, GenericWebService.GET);
		
		gWS.adicionarParametro("nimg", ""+this.limitImages);
		gWS.adicionarParametro("tempo", ""+this.limitTime);
		gWS.adicionarParametro("item", PCAPPI);
		
		return gWS;
	}

	@Override
	public Object parseResponse(String response) {
		
		ArrayList<Radar> radarList = null;
		
		RadarResponse resp = new RadarResponse();

		JSONObject jsonResponse = null;
		try {
			
			if(response == null)
				throw new Exception("Oops! Resposta vazia para a solicitação da imagens do Radar X.");
			
			jsonResponse = new JSONObject(response);
			
			radarList = new ArrayList<Radar>();
			
			if (!jsonResponse.has("result"))
				throw new Exception("Oops! Nenhum resultado para as imagens do Radar X.");
			
			JSONArray jsonList = jsonResponse.getJSONArray("result");
			
			String path   = (jsonResponse.has("path"))?   (String) jsonResponse.getString("path") : "";
			String alerta = (jsonResponse.has("alerta"))? (String) jsonResponse.getString("alerta") : "";
			String info   = (jsonResponse.has("info"))?   (String) jsonResponse.getString("info") : "";
			
			resp.setAlert(alerta);
			resp.setInfo(info);
			
			for(int i = 0; i < jsonList.length(); i++) {
				
				JSONObject radarJSON = jsonList.getJSONObject(i);
				
				Radar radar = new Radar();
				
				if (radarJSON.has("datahora"))
					radar.setText(radarJSON.getString("datahora"));
				
				if (radarJSON.has("dia") && radarJSON.has("img")) {
					radar.setImg(path + radarJSON.getString("dia") + "/" + radarJSON.getString("img"));
				
					radarList.add(radar);
				}
			}
			
			resp.setList(radarList);
			
		} catch(Exception e)
		{
			radarList = null;
			Log.d("DEBUG", e.getMessage());
		}
		
		return resp;
	}

	public void cancel() {
		
		gWS.cancel(true);
	}
}