package br.gov.funceme.webservices.service.inmet;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.gov.funceme.database.model.Cidade;
import br.gov.funceme.funcemeservices.R;
import br.gov.funceme.webservices.ws.AbstractService;
import br.gov.funceme.webservices.ws.GenericWebService;

public class CidadesService extends AbstractService {

	private String url;
	private Context context;
	
	private String uf;
	
	public CidadesService(Context context) {
		this.context = context;
	}
	
	public void getCidades() {
		
		url = context.getString(R.string.url_inmet_cidades);
		
		super.makeRequest();
	}

	public void getCidades(String uf) {
		
		this.uf = uf;
		
		super.makeRequest();
	}
	
	@Override
	public GenericWebService prepareWebService() {
		
		GenericWebService gWS = new GenericWebService(this.url, GenericWebService.GET);
		
		if (!this.uf.isEmpty())
			gWS.adicionarParametro("uf" , ""+this.uf);
		
		return gWS;
	}

	@Override
	public ArrayList<Cidade> parseResponse(String response) {
		
		JSONArray jsonResponse = null;
		
		ArrayList<Cidade> lista = new ArrayList<Cidade>();
		
		try {
			
			if(response == null)
				throw new Exception("Oops! Resposta vazia para a solicita��o de cidades.");
			
			jsonResponse = new JSONArray(response);
			
			for(int i = 0; i < jsonResponse.length(); i++) {
				
				JSONObject obj = jsonResponse.getJSONObject(i);
				
				Cidade item = new Cidade();
				
				item.setCodigo(obj.getString("geocode"));
				item.setNome(obj.getString("nome"));
				item.setUf(obj.getString("uf"));
				item.setLat((float)obj.getDouble("lat"));
				item.setLon((float)obj.getDouble("lon"));
				
				lista.add(item);			
			}
			
		} catch(Exception e)
		{
			Log.d("DEBUG", e.getMessage());
		}
		
		return lista;
	}
}
