
package br.gov.funceme.webservices.service;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.gov.funceme.database.model.TempoAgora;
import br.gov.funceme.funcemeservices.R;
import br.gov.funceme.webservices.ws.AbstractService;
import br.gov.funceme.webservices.ws.GenericWebService;

public class TempoAgoraService extends AbstractService {
	
	private String url;
	private Context context;
	
	private String city_id;
	
	public TempoAgoraService(Context context)
	{
		this.context = context;
	}
	
	public void getWeatherNowFromCity(String city_id)
	{
		url = context.getString(R.string.URL_WEATHER_NOW); 
		
		this.city_id = city_id;
		
		super.makeRequest();
	}
	
	@Override
	public GenericWebService prepareWebService() {
		
		GenericWebService gWS = new GenericWebService(this.url, GenericWebService.GET);
		gWS.adicionarParametro("codigo", this.city_id);
		
		return gWS;
	}

	@Override
	public Object parseResponse(String response) {
		
		return response;
	}
}