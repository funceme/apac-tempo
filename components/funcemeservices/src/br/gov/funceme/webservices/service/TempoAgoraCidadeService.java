package br.gov.funceme.webservices.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.gov.funceme.database.model.Cidade;
import br.gov.funceme.funcemeservices.R;
import br.gov.funceme.webservices.ws.AbstractService;
import br.gov.funceme.webservices.ws.GenericWebService;

public class TempoAgoraCidadeService extends AbstractService {

	private String url;
	private Context context;
	
	public TempoAgoraCidadeService(Context context) {
		this.context = context;
	}
	
	public void getWeatherNowCities() {
		
		url = context.getString(R.string.URL_WEATHER_NOW_CITIES);
		super.makeRequest();
	}
	
	@Override
	public GenericWebService prepareWebService() {
		
		GenericWebService gWS = new GenericWebService(this.url, GenericWebService.GET);
		return gWS;
	}

	@Override
	public Object parseResponse(String response) {
		
		List<Cidade> cities = null;

		JSONObject jsonResponse = null;
		try {
			
			if(response == null)
				throw new Exception("Oops! Resposta vazia para a solicitação de cidades do tempo agora.");
			
			jsonResponse = new JSONObject(response);
			
			cities = new ArrayList<Cidade>();
			
			if (jsonResponse.has("cidades"))
				throw new Exception("Oops! Nenhuma informação retornada para o tempo agora da cidade.");
			
			JSONArray citiesJSON = jsonResponse.getJSONArray("cidades");
			
			for(int i = 0; i < citiesJSON.length(); i++) {
				
				JSONObject cityJSON = citiesJSON.getJSONObject(i);
				
				Cidade city = new Cidade();
				
				if (cityJSON.has("cod")) 	city.setCodigo(cityJSON.getString("cod"));
				if (cityJSON.has("nome")) 	city.setNome(cityJSON.getString("nome"));
				if (cityJSON.has("cat")) 	city.setCat(cityJSON.getString("cat"));
				if (cityJSON.has("atual")) 	city.setAtual(cityJSON.getString("atual"));
				
				cities.add(city);
			}
			
		} catch(Exception e)
		{
			cities = null;
			Log.d("DEBUG", e.getMessage());
		}
		
		return cities;
	}
}