package br.gov.funceme.webservices.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.gov.funceme.database.model.SolLua;
import br.gov.funceme.funcemeservices.R;
import br.gov.funceme.webservices.ws.AbstractService;
import br.gov.funceme.webservices.ws.GenericWebService;

public class SolLuaService extends AbstractService {

	private String url;
	private Context context;
	
	private String city_code;
	
	public SolLuaService(Context context)
	{
		this.context = context;
	}
	
	public void getSunCity(String city_code)
	{
		this.city_code = city_code;
		
		url = context.getString(R.string.URL_SUN);
		super.makeRequest();
	}
	
	@Override
	public GenericWebService prepareWebService() {
		
		GenericWebService gWS = new GenericWebService(this.url, GenericWebService.GET);
		
		gWS.adicionarParametro("codMun", ""+this.city_code);
		
		return gWS;
	}

	@Override
	public String parseResponse(String response) {
		
		return response;	
	}
}