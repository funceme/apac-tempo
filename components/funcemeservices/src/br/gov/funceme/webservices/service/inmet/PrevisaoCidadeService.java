package br.gov.funceme.webservices.service.inmet;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.gov.funceme.database.model.inmet.Previsao;
import br.gov.funceme.funcemeservices.R;
import br.gov.funceme.webservices.ws.AbstractService;
import br.gov.funceme.webservices.ws.GenericWebService;

public class PrevisaoCidadeService extends AbstractService {

	private String url;
	private Context context;
	
	private long geocode;
	
	public PrevisaoCidadeService(Context context) {
		this.context = context;
	}
	
	public void getPrevisao(long geocode) {
		
		this.geocode = geocode;
		
		url = context.getString(R.string.url_inmet_previsao_cidade);
		
		super.makeRequest();
	}
	
	@Override
	public GenericWebService prepareWebService() {
		
		GenericWebService gWS = new GenericWebService(this.url, GenericWebService.GET);
		
		gWS.adicionarParametro("r", "prevmet/previsaoWebservice/getJsonPrevisaoDiariaPorCidade");
		gWS.adicionarParametro("code" , ""+this.geocode);
		
		return gWS;
	}

	@Override
	public ArrayList<Previsao> parseResponse(String response) {
		
		JSONObject jsonResponse = null;
		
		ArrayList<Previsao> lista = new ArrayList<Previsao>();
		
		try {
			
			if(response == null)
				throw new Exception("Oops! Resposta vazia para a solicitação de previsão de cidade.");
			
			jsonResponse = new JSONObject(response);
			
			JSONObject previsaoCidade = jsonResponse.getJSONObject(""+this.geocode);
			
			Timestamp tempo = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
			
			Iterator iterator = previsaoCidade.keys();
			
			while (iterator.hasNext()) {
				
				String dia = (String) iterator.next();
				
				JSONObject diaContent = previsaoCidade.getJSONObject(dia);
				
				if (diaContent.has("manha")) {
					lista.add(toPrevisao(diaContent, dia, "manha", tempo.getTime()));
					lista.add(toPrevisao(diaContent, dia, "tarde", tempo.getTime()));
					lista.add(toPrevisao(diaContent, dia, "noite", tempo.getTime()));
				} else {
					lista.add(toPrevisao(diaContent, dia, "dia", tempo.getTime()));
				}
			}
			
		} catch(Exception e)
		{
			Log.d("DEBUG", e.getMessage());
		}
		
		return lista;
	}

	private Previsao toPrevisao(JSONObject diaContent, String dia, String periodo, long tempo) throws JSONException, ParseException {
		
		Previsao previsao = new Previsao();
		
		JSONObject json;
		
		if (periodo.equals("dia"))
			json = diaContent;
		else {
			json = diaContent.getJSONObject(periodo);
		}
			
		previsao.setGeocode(this.geocode);
		previsao.setData(dia);
		previsao.setPeriodo(periodo);
		previsao.setUf(json.getString("uf"));
		previsao.setCidade(json.getString("entidade"));
		previsao.setResumo(json.getString("resumo"));
		previsao.setTempo(json.getString("tempo"));
		previsao.setTemperaturaMaxima((float)json.getDouble("temp_max"));
		previsao.setTemperaturaMinima((float)json.getDouble("temp_min"));
		previsao.setDirecaoVento(json.getString("dir_vento"));
		previsao.setIntensidadeVento(json.getString("int_vento"));
		previsao.setIcone(json.getString("icone"));
		previsao.setDiaSemana(json.getString("dia_semana"));
		previsao.setUmidadeMaxima(json.getInt("umidade_max"));
		previsao.setUmidadeMinima(json.getInt("umidade_min"));
		previsao.setNascer(json.getString("nascer"));
		previsao.setOcaso(json.getString("ocaso"));
		previsao.setTemperaturaMaximaTendencia(json.getString("temp_max_tende"));
		previsao.setTemperaturaMaximaTendenciaIcone(json.getString("temp_max_tende_icone"));
		previsao.setTemperaturaMinimaTendencia(json.getString("temp_min_tende"));
		previsao.setTemperaturaMinimaTendenciaIcone(json.getString("temp_min_tende_icone"));
		previsao.setFonte(json.getString("fonte"));
		previsao.setCreatedAt(tempo);

		return previsao;
	}
}