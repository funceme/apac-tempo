package br.gov.funceme.webservices.service.inmet;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.gov.funceme.database.model.inmet.Aviso;
import br.gov.funceme.funcemeservices.R;
import br.gov.funceme.webservices.ws.AbstractService;
import br.gov.funceme.webservices.ws.GenericWebService;

public class AvisosService extends AbstractService {

	private String url;
	private Context context;
	
	public AvisosService(Context context) {
		this.context = context;
	}
	
	@Override
	public GenericWebService prepareWebService() {
		
		GenericWebService gWS = new GenericWebService(this.url, GenericWebService.GET);

		return gWS;
	}
	
	public void getAvisos() {
		
		this.url = context.getString(R.string.URL_AVISOS);
		
		super.makeRequest();
	}

	@Override
	public Object parseResponse(String response) {
		
		JSONArray jsonResponse = null;
		
		ArrayList<Aviso> lista = new ArrayList<Aviso>();
		
		try {
			
			if(response == null)
				throw new Exception("Oops! Resposta vazia para a solicita��o de avisos.");
			
			jsonResponse = new JSONArray(response);
			
			for(int i = 0; i < jsonResponse.length(); i++) {
				
				JSONObject obj = jsonResponse.getJSONObject(i);
				
				Aviso item = new Aviso();
				
				item.setIdAviso(obj.getInt("id_aviso"));
				item.setId(obj.getString("id"));
				item.setLocal(obj.getString("local"));
				item.setTexto(obj.getString("texto"));
				item.setCondicaoSevera(obj.getString("condicaoSevera"));
				item.setArquivo(obj.getString("arquivo"));
				item.setDataInicio(obj.getString("dataInicio"));
				item.setHoraInicio(obj.getString("horaInicio"));
				item.setDataFim(obj.getString("dataFim"));
				item.setHoraFim(obj.getString("horaFim"));
				
				lista.add(item);			
			}
			
		} catch(Exception e)
		{
			Log.d("DEBUG", e.getMessage());
		}
		
		return lista;
	}
}