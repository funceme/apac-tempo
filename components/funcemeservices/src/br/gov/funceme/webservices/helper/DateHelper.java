package br.gov.funceme.webservices.helper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateHelper {
	
	public static String getFormatedDay(String format, int shift){
		
		DateFormat df = new SimpleDateFormat(format);
		Date today = Calendar.getInstance().getTime(); 
		
		Calendar.getInstance().add(Calendar.DAY_OF_MONTH, shift);
		
		return df.format(today);
	}
	
	public static long getNow(){
		
		return Calendar.getInstance().getTime().getTime();
	}
}
