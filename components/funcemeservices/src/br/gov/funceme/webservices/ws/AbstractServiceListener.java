package br.gov.funceme.webservices.ws;

public abstract class AbstractServiceListener {
	
	public abstract void OnComplete(Object params);
	
}
