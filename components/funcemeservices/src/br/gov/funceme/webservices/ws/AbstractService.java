package br.gov.funceme.webservices.ws;

import android.app.Activity;


public abstract class AbstractService extends GenericWebServiceListener {
	private AbstractServiceListener serviceListener;
	private WaitView waitView;
	private String message;
	private Activity parentActivity;
	
	public AbstractServiceListener getServiceListener() {
		return serviceListener;
	}

	public void setServiceListener(AbstractServiceListener serviceListener) {
		this.serviceListener = serviceListener;
	}
	
	public void setLoadingViewActivity(Activity activity)
	{
		this.parentActivity = activity;
	}
	
	public void setLoadingMessage(String message) {
		this.message = message;
	}
	
	public void makeRequest()
	{
		if(parentActivity != null) {
			if(message != null)
				waitView = new WaitView(parentActivity, message);
			else
				waitView = new WaitView(parentActivity, "Aguarde");
		}
		if(waitView != null)
			waitView.enable();
		
		GenericWebService gWS = prepareWebService();
		gWS.setListener(this);
		gWS.execute();
	}
	
	public abstract GenericWebService prepareWebService();
	
	public abstract Object parseResponse(String response);
	
	
	@Override
	public void onCompleteRequest(GenericWebService gWS, String response) {
		if(waitView != null)
			waitView.disable();
		if(this.serviceListener != null)
			this.serviceListener.OnComplete(parseResponse(response));
	}
	
	
}
