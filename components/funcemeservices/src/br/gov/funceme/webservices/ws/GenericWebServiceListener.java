package br.gov.funceme.webservices.ws;

public abstract class GenericWebServiceListener {
	
	public abstract void onCompleteRequest(GenericWebService gWS, String response);
}
