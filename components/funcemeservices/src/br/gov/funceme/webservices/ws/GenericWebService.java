package br.gov.funceme.webservices.ws;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.mime.MultipartEntity; 
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;


import android.os.AsyncTask;

public class GenericWebService extends AsyncTask<Void,Void,Void>{
	
	private volatile boolean running = true;
	
	public static final int GET = 0;
	public static final int POST = 1;
	
	private static final String XML_CONTENT_TYPE = "text/xml";
	private static final String JSON_CONTENT_TYPE = "text/json";
	private static final String PLAIN_CONTENT_TYPE = "text/plain";
	
	private String url;
	private String getURL;
	private String resposta;
	private int tipo;
	private int timeout = 30000;
	
	private GenericWebServiceListener listener;
	
	private List<String[]> parametros;
	private List<Object[]> arquivos;

	public GenericWebService(String url)
	{
		this.url = url;
		this.arquivos = new ArrayList<Object[]>();
		this.parametros = new ArrayList<String[]>();
		this.tipo = GET;
	}

	public GenericWebService(String url, int tipo)
	{
		this.url = url;
		this.arquivos = new ArrayList<Object[]>();
		this.parametros = new ArrayList<String[]>();
		this.tipo = tipo;
	}
	
	public GenericWebService(String url, int tipo, int timeoutInSeconds)
	{
		this.url = url;
		this.arquivos = new ArrayList<Object[]>();
		this.parametros = new ArrayList<String[]>();
		this.tipo = tipo;
		this.timeout = timeoutInSeconds * 1000;
	}
	
	public void setTimeout(int timeoutInSeconds)
	{
		this.timeout = timeoutInSeconds * 1000;
	}
	
	public void setListener(GenericWebServiceListener listener)
	{
		this.listener = listener;
	}
	
	public void setTipo(int tipo)
	{
		this.tipo = tipo;
	}

	public void adicionarParametro(String chave, String valor)
	{
		String[] chaveParametro = new String[3];
		chaveParametro[0] = chave;
		chaveParametro[1] = valor;
		chaveParametro[2] = "text/plain";
		parametros.add(chaveParametro);
	}
	
	public void adicionarParametro(String chave, String valor, String mimetype)
	{
		String[] chaveParametro = new String[3];
		chaveParametro[0] = chave;
		chaveParametro[1] = valor;
		chaveParametro[2] = mimetype;
		parametros.add(chaveParametro);
	}
	
	public void adicionarArquivo(String chave, File arquivo, String mimetype)
	{
		Object[] chaveArquivo = new Object[3];
		chaveArquivo[0] = chave;
		chaveArquivo[1] = arquivo;
		chaveArquivo[2] = mimetype;
		
		arquivos.add(chaveArquivo);
	}
	
	public void request()
	{
		if(this.tipo == GET)
		{
			executarGET();
		} else 
		{
			executarPOST();
		}
	}

	private void executarPOST()
	{
		try {
			HttpClient client = new DefaultHttpClient();  
			HttpPost post = new HttpPost(this.url); 
			
			post.getParams().setParameter("http.socket.timeout", Integer.valueOf(timeout));
			if(this.arquivos.size() > 0) {
				MultipartEntity mEntity = new MultipartEntity();
				for(Object[] chaveArquivo : this.arquivos) {
					mEntity.addPart((String)chaveArquivo[0], new FileBody((File)chaveArquivo[1],((File)chaveArquivo[1]).getName(),(String)chaveArquivo[2],"UTF-8"));
				}
				for(String[] n : parametros) {
					mEntity.addPart(n[0], new StringBody(n[1], n[2], Charset.forName("UTF-8")));
				}
				post.setEntity(mEntity);
			} else {
				List<NameValuePair> nVP = new ArrayList<NameValuePair>();
				for(String[] p : parametros) {
					nVP.add(new BasicNameValuePair(p[0], p[1]));
				}
				
				post.setEntity(new UrlEncodedFormEntity(nVP));
			}
			HttpResponse responsePOST = client.execute(post);
			HttpEntity resEntity = responsePOST.getEntity(); 
			if (resEntity != null) {
				String stringEntity = EntityUtils.toString(resEntity);
				String contentTypeEntity = resEntity.getContentType().getValue().split(";")[0];
				this.resposta = parseResult(contentTypeEntity, stringEntity);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void executarGET()
	{		
		try {
			HttpClient client = new DefaultHttpClient();  
			gerarGETURL();
			HttpGet get = new HttpGet(this.getURL);
			get.getParams().setParameter("http.socket.timeout", Integer.valueOf(timeout));
			HttpResponse responseGet = client.execute(get);  
			HttpEntity resEntityGet = responseGet.getEntity(); 
			
			if (resEntityGet != null) {  
				//do something with the response
				String stringEntity = EntityUtils.toString(resEntityGet);
				String contentTypeEntity = resEntityGet.getContentType().getValue().split(";")[0];
				this.resposta = parseResult(contentTypeEntity, stringEntity);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
	private String parseResult(String contentType, String result)
	{
		if(contentType.equalsIgnoreCase(XML_CONTENT_TYPE))
		{
			String resultInJSON = null;
//			try {
//				//resultInJSON = XML.toJSONObject(result).toString();
//			} catch(JSONException e) {
//			}
			
			if(resultInJSON != null)
				result = resultInJSON;

		} else if(contentType.equalsIgnoreCase(JSON_CONTENT_TYPE)) {
		
		} else if(contentType.equalsIgnoreCase(PLAIN_CONTENT_TYPE)) {
		
		}
		
		return result;
	}
	
	private void gerarGETURL()
	{
		this.getURL = this.url + "?";
		List<NameValuePair> nVP = new ArrayList<NameValuePair>();
		for(String[] p : parametros) {
			nVP.add(new BasicNameValuePair(p[0], p[1]));
		}
		String params = URLEncodedUtils.format(nVP, "utf-8");
		this.getURL+= params;
		
		System.out.print(this.getURL);
	}

	@Override
	protected Void doInBackground(Void... params) {
		this.request();
		
		return null;
	}
	
	@Override
    protected void onCancelled() {
        running = false;
    }
	
	@Override
	protected void onPostExecute(Void result) {
		if(this.listener != null)
			this.listener.onCompleteRequest(this, resposta);
    }
}
