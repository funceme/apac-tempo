package br.gov.funceme.webservices.ws;

import android.app.Activity;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class WaitView {
	
	private Activity activity;
	private ViewGroup rootView;
	private ViewGroup view;
	private ProgressBar progressBar;
	private int lifeCounter;
	private String message =  "Aguarde...";
	private TextView messageTextView;
	
	public WaitView(Activity activity, String message)
	{
		this.activity = activity;
		this.rootView = (ViewGroup) this.activity.getWindow().getDecorView().findViewById(android.R.id.content);
		
		lifeCounter = 0;
		this.view = new LinearLayout(this.activity);
		this.view.setClickable(true);
		((LinearLayout)this.view).setGravity(Gravity.CENTER);
		this.view.setBackgroundColor(0xCC000000);
		this.view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		progressBar = new ProgressBar(activity, null, android.R.attr.progressBarStyle);
		this.view.addView(progressBar);
		this.message = message;
		
		messageTextView = new TextView(this.activity);
		messageTextView.setText(this.message);
		messageTextView.setTextColor(0xFFFFFFFF);
		messageTextView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
		messageTextView.setPadding(20, 0, 0, 0);
		this.view.addView(messageTextView);
	}
	
	public WaitView(Activity activity, ViewGroup target)
	{
		this.activity = activity;
		this.rootView = target;
		
		lifeCounter = 0;
		this.view = new RelativeLayout(this.activity);
		((RelativeLayout)this.view).setGravity(Gravity.CENTER);
		this.view.setBackgroundColor(0xCC000000);
		this.view.setAlpha(0.5f);
		this.view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		progressBar = new ProgressBar(activity, null, android.R.attr.progressBarStyle);
		this.view.addView(progressBar);
	}

	public void enable()
	{
		lifeCounter++;
		if(lifeCounter == 1)
		{
			this.rootView.addView(this.view);
		}
	}
	
	public void setMessage(String message)
	{
		this.message = message;
		
		if (messageTextView == null) {
			
			messageTextView = new TextView(this.activity);
			messageTextView.setText(this.message);
			messageTextView.setTextColor(0xFFFFFFFF);
			messageTextView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
			messageTextView.setPadding(20, 0, 0, 0);
			this.view.addView(messageTextView);
		}
		messageTextView.setText(this.message);
		
		//progressBar.setPadding((messageTextView.getWidth()/2) - (progressBar.getWidth()/2), 0, 0, 0);
	}
	
	public void disable()
	{
		if(lifeCounter > 0)
		{
			lifeCounter--;
			if(lifeCounter == 0)
			{
				this.rootView.removeView(this.view);
			}
		}
	}

	public boolean isEnabled() {
		
		return (lifeCounter>0);
	}
}
