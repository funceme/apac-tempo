package br.gov.funceme.webservices.util;

import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import br.gov.funceme.database.model.Imagens;

public class DownloadImagensSatelite extends AsyncTask<Void, Void, Imagens> {
	public String url = "";
	public String validade = "";
	DownloadImagensSatelite singleton = null;
	private Context ctx = null;
	int tipoRadar = -1;
	int quant;



	public DownloadImagensSatelite(String url, Context ctx, String validade, int quant ) {
		this.url = url;
		this.validade = validade;
		this.ctx = ctx;
		this.quant = quant;
	}
	

	@Override
	protected void onPreExecute() {
		//singleton = this;

		// count.setText( count.getText() + " ... in progress image "+ id+1 );
	}

	@Override
	protected Imagens doInBackground(Void... params) {
		// write here code that can take a long time
		Bitmap imagem = null;
		try {
			
			if (params != null && this.quant <= 8) {
				DownloadImagensAsync das = new DownloadImagensAsync();
				imagem = das.ImageOperations( ctx,  url, "id" + ".jpg");	
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		// publishProgress calls onProgressUpdate to update something on UI
		//publishProgress();

		Imagens imagemRadar = new Imagens();
		imagemRadar.setImagem(imagem);
		imagemRadar.setUrlImagem(url);
		imagemRadar.setValidade(validade);
		return imagemRadar;
	}

	@Override
	// Call this method when doInBackground is working to update the progress
	protected void onProgressUpdate(Void... values) {
		// int number = id+1;
		// count.setText("Loaded " + number + "/5");
	}

	@Override
	protected void onPostExecute(Imagens result) {
		
			//SateliteActivity.singleton.retornoDownloadImagensSatelite(result);
		
		try {
			finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
 
}
