package br.gov.funceme.webservices.util;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

public class LoadRadarImageryTask extends AsyncTask<String, Void, Map<String, Bitmap>> {
	
    Bitmap bmImage;
    public Map<String, Bitmap> imgList;

    public LoadRadarImageryTask() {
        super();
    }

    protected Map<String, Bitmap> doInBackground(String... urls) {
    	
    	imgList = new HashMap<String, Bitmap>();
    	
    	for (String url : urls) {
    		Bitmap image = null;
            try {
                InputStream in = new java.net.URL(url).openStream();
                BitmapFactory.Options options=new BitmapFactory.Options();
        		options.inSampleSize = 2;
        		options.inPreferredConfig = Bitmap.Config.ALPHA_8;
        		image = BitmapFactory.decodeStream(in, null, options);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            
            imgList.put(url, image);
		}       
        return imgList; 
    }

    protected void onPostExecute(ArrayList<Bitmap> array) {
        
    }
}