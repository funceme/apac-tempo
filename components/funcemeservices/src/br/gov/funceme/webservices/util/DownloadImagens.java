package br.gov.funceme.webservices.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.gov.funceme.funcemeservices.R;

 public class DownloadImagens implements IDownloadDadosWeb {
	public String url = "";
	public String validade = "";
	public Context ctx;
	public boolean downloadValidaListaImagensRadarX = false;
	public boolean downloadValidaListaImagensRadarS = false;
	public boolean downloadValidaListaImagens = false;
	boolean downloadImagensRadarX = false;
	boolean downloadImagensRadarS = false;
	private int qntImagens = 1;
	int i = 0;
	boolean downloadListaImagensSatelite = false;

	public DownloadImagens(Context ctx) {
		this.ctx = ctx;
    }

	public void downloadListaImagensRadarS(int qntImagens) {
		this.qntImagens = qntImagens;
		this.downloadValidaListaImagensRadarS = true;

		String url = "";
		if (ctx != null) {
			url = ctx.getString(R.string.URL_RADAR_S) + qntImagens;
		} else {
			Log.i("ERRO", "ERRO: Contexto nulo na class DownloadImagem");
		}
		DownloadDadosWeb downloadLista = new DownloadDadosWeb(this);
		if (!url.equals("")) {
			downloadLista.execute(url);
		} else {
			Log.i("ERRO", "ERRO: Url Invalida na Classe DownloadImagem");
		}

	}

	public void downloadListaImagensRadarX(int qntImagens) {
		this.downloadValidaListaImagensRadarX = true;
		this.qntImagens = qntImagens;
		String url = "";
		if (ctx != null) {
			url = ctx.getString(R.string.URL_RADAR_X) + qntImagens;
		} else {
			Log.i("ERRO", "ERRO: Contexto nulo na class DownloadImagem");
		}
		DownloadDadosWeb downloadLista = new DownloadDadosWeb(this);
		if (!url.equals("")) {
			downloadLista.execute(url);
		} else {
			Log.i("ERRO", "ERRO: Url Inválida na Classe DownloadImagem");
		}
	}

	public void mountListImages(String  dados, int qntImagens){

		String[] urlList = new String[qntImagens];
		String[] validade = new String[qntImagens];
		JSONObject objJson;
		
		try {
			objJson = new JSONObject(dados);

			JSONArray imagens = objJson.getJSONArray("radar");

			for (int i = 0; i < qntImagens; i++) {
				JSONObject objJsonCidade = new JSONObject(imagens.getString(i));

				urlList[i] =  objJsonCidade.getString("img");
				validade[i] = objJsonCidade.getString("txt");
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String onPostExecute(String dados) {

		if (this.downloadValidaListaImagens ||  this.downloadValidaListaImagensRadarS || this.downloadValidaListaImagensRadarX) {

			this.downloadValidaListaImagens = false;
			this.downloadValidaListaImagensRadarX = false;
			this.downloadValidaListaImagensRadarS = false;
			
			mountListImages(dados, qntImagens);
		}
		
		return dados;
	}

}
