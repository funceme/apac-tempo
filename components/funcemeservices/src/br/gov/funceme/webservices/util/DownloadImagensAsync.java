package br.gov.funceme.webservices.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.ByteArrayBuffer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import br.gov.funceme.database.model.Imagens;

public class DownloadImagensAsync extends AsyncTask<Void, Void, Imagens>  {
	public String url = "";
	public String validade = "";
	private Context ctx;
	int tipoRadar = -1; 


	public DownloadImagensAsync(){
	
	}
	
	public void  setDataDownloadImagens(String url,Context ctx, String validade) {
		this.url = url;
		this.ctx = ctx;
		this.validade = validade;
	}

	@Override
	protected Imagens doInBackground(Void... params) {
		
		Bitmap imagem = null;
		try {
			imagem = this.ImageOperations(ctx, url, "id" + ".png");
		} catch (IOException e) {
			e.printStackTrace();
		}
		// publishProgress calls onProgressUpdate to update something on UI
		//publishProgress();


		Imagens imagemRadar = new Imagens();
		imagemRadar.setImagem(imagem);
		imagemRadar.setUrlImagem(url);
		imagemRadar.setValidade(validade);
		
		return imagemRadar;
	}


	@Override
	protected void onPreExecute(){
		
	}

	@Override
	protected void onPostExecute(Imagens result) {
	//	MapRadar.singleton.retornoDownloadImagensRadar(result);
		try {
			finalize();
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}


	public  Bitmap ImageOperations( Context ctx, String url, String saveFilename)
			throws IOException {

		Bitmap imagem = null;
		HttpURLConnection connection = (HttpURLConnection) new URL(url)
		.openConnection();
		connection.setRequestProperty("User-agent", "Mozilla/4.0");

		connection.connect();
		InputStream input = connection.getInputStream();

		try {
			imagem = BitmapFactory.decodeStream(input);
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			input.close();
			input = null;
			connection.disconnect();
		   }
	
		return imagem;
	}
	public  String[] downloadBitmap(String[] urlList, String path, String[] fileName) {

		String[] bArray = new String[urlList.length];
		for(int i = 0; i < urlList.length; i++){
			String url = urlList[i];
			final AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
			final HttpGet getRequest = new HttpGet(url);

			try {
				HttpResponse response = client.execute(getRequest);
				final int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode != HttpStatus.SC_OK) { 
					return null;
				}

				final HttpEntity entity = response.getEntity();
				if (entity != null) {
					InputStream inputStream = null;
					try {
						inputStream = entity.getContent();
						FlushedInputStream fis = new FlushedInputStream(inputStream);
						BufferedInputStream bis = new BufferedInputStream(fis);

						/*
						 * Read bytes to the Buffer until there is nothing more to read(-1).
						 */
						ByteArrayBuffer baf = new ByteArrayBuffer(50000);
						int current = 0;
						while ((current = bis.read()) != -1) {
							baf.append((byte) current);
						}
						bis.close();
						File file = new File(path, fileName[i] + ".png");
						OutputStream fOut = null;
						try {
							fOut = new FileOutputStream(file);
							fOut.write(baf.toByteArray());
							fOut.flush();
							fOut.close();
							bArray[i] = path + "/" + fileName[i] + ".png";
						} catch (Exception e) {
							bArray[i] = null;
						}

					} finally {
						if (inputStream != null) {
							inputStream.close();  
						}
						entity.consumeContent();
					}
				}
			} catch (Exception e) {
				bArray = null;
				// Could provide a more explicit error message for IOException or IllegalStateException
				getRequest.abort();
			} finally {
				if (client != null) {
					client.close();
				}
			}
		}
		return bArray;
	}


	public Imagens downloadByStreaming( Context ctx,  String url, String date, int id, String extension){

		Bitmap imagem = null;
		try {
			imagem = this.ImageOperations( ctx, url, id + extension);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Imagens imagemRadar = new Imagens();
		imagemRadar.setImagem(imagem);
		imagemRadar.setUrlImagem(url);
		imagemRadar.setValidade(date);

		return imagemRadar;
	}
}
