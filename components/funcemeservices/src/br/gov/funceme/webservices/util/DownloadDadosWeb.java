package br.gov.funceme.webservices.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

public class DownloadDadosWeb extends AsyncTask<String, Void, String> {

	private IDownloadDadosWeb task;

	public DownloadDadosWeb(IDownloadDadosWeb todo) {
		task = todo;
	}

	@Override
	protected String doInBackground(String... dados) {
		if (task != null) {
			try {
				return buscarDados(dados[0]);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			return null;
		}
		return null;

	}

	@Override
	protected void onPostExecute(String result) {

		if (task != null) {
			task.onPostExecute(result);
			try {
				finalize();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public String buscarDados(String url) throws Exception {
		
		HttpClient client = new DefaultHttpClient();
		HttpGet getRequest = new HttpGet();
		getRequest.setURI(new URI(url));
		HttpResponse response = client.execute(getRequest);
		int statusCode = response.getStatusLine().getStatusCode();
		if (statusCode != HttpStatus.SC_OK) {
			Log.w("Conexao", "Error " + statusCode
					+ " while retrieving json from " + url);
			return null;
		}

		final HttpEntity entity = response.getEntity();
		InputStream is = entity.getContent();

		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		
		
		String linha = br.readLine();
		String dados = null;

		while (linha != null) {

			if (linha == "" || linha == null){
				continue;
			}
			else{
				dados = linha;
			}

			linha = br.readLine();
		}
		br.close();
		isr.close();
		is.close();
		
		client.getConnectionManager().shutdown();
		return dados;

	}
	

	
}

