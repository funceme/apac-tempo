package br.gov.funceme.components.map;

import java.util.ArrayList;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Debug;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;
import br.gov.funceme.components.util.LoadImageryTask;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

public class MapAnimationFragment extends Fragment {
	
	protected Activity activity;
	protected GoogleMap map;
	
	private int layoutId;
	private int mapId;
	private Point mapSize;
	
	private ArrayList<BitmapDescriptor> overlays;

	private float overlayAlpha;
	
	private int animationInterval = 2000;
	
	private Timer timer;
	private TimerTask refresher;
	protected int countTimer = 0;
	
	protected GroundOverlay overlay;
	
	private String[] images;
	private LatLngBounds bounds;
	
	protected View view;
	
	private Runnable runableAnimation;

	public MapAnimationFragment() {
		super();
	}
	
	protected void setup(int layoutId, int mapId, Point mapSize) {
	
		this.layoutId = layoutId;
		this.mapId 	  = mapId;
		this.mapSize  = mapSize;
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		this.activity = getActivity();
		
		this.view = inflater.inflate(this.layoutId, container, false);
		
		MapsInitializer.initialize(getActivity().getApplicationContext());
		
		this.view.setLayoutParams(new LayoutParams(this.mapSize.x, this.mapSize.y));
		
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

		if(status!=ConnectionResult.SUCCESS){
			int requestCode = 10;
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getActivity(), requestCode);
			dialog.show();
		}
		else {
			map = ((MapFragment) getActivity().getFragmentManager().findFragmentById(this.mapId)).getMap();			
		}
		
		return view;
	}
	
	protected void centralize(LatLng center, float zoom) {
		
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(center, zoom));
		
	}
	
	protected void loadImages(String[] imageList) {
				
		this.images = imageList;
		
		LoadImageryTask downloadImgs = new LoadImageryTask(){
			@Override
			protected void onPostExecute(Map<String, Bitmap> map) {
				
				super.onPostExecute(map);
				
				createNewListOverlays(map.size());
				
				for (String url : images) {
					
					final BitmapDescriptor image = BitmapDescriptorFactory.fromBitmap(map.get(url));

					overlays.add(image);
				}
			}
		};
		
		downloadImgs.execute(images);		
	}
	 
	protected void doAnimation() 
	{
		runableAnimation = new Runnable() {
			
			public void run() 
			{
				if (overlays == null || overlays.size()==0)
					return;
				
				try {
					if (overlay != null) overlay.remove();
				} catch (Exception e){
					//
				}
				BitmapDescriptor bitmap = overlays.get(countTimer);
				
				if (bitmap == null)
					return;
				
		    	GroundOverlayOptions overlayMap = new GroundOverlayOptions().image(bitmap).positionFromBounds(bounds);
				overlayMap.transparency(overlayAlpha);
		    	
		    	overlay = map.addGroundOverlay(overlayMap);
		
				onStepAnimation();
			}
		};
		
		if (getActivity() != null && map != null && runableAnimation != null)
			getActivity().runOnUiThread(runableAnimation);
	}
	
	public void onStepAnimation() {
		
		countTimer++;
		
		if (overlays.size() == countTimer) 
			countTimer = 0;	
		
	}
	
	protected void clear() {
		
		if (overlays != null) {
			overlays.clear();
			overlays = null;
		}
		
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		
		if (map != null)
			map.clear();
	}
		
	public void startAnimation(double latNorth, double latSouth, double lngEast, double lngWest){
		
		LatLng southwest = new LatLng(latSouth, lngWest);
		LatLng northeast = new LatLng(latNorth, lngEast);;
		
		this.bounds = new LatLngBounds(southwest, northeast);
		
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		
		timer = new Timer();

		refresher = new TimerTask() {
			public void run() {
				doAnimation();
			};
		};
		
		timer.scheduleAtFixedRate(refresher, 0, animationInterval);
	}
	
	private void createNewListOverlays(int size){
		
		if (overlays != null) {
			overlays.clear();
			overlays = null;
		}
		
		overlays = new ArrayList<BitmapDescriptor>(size);
	}
	
	protected int imagesDownload (int memoryLimit, int qtde) {
		
		long total_memory = Debug.getNativeHeapSize();
		
		long memory = Debug.getNativeHeapFreeSize();
		
		float memory_free = (float)memory/1024;
		
		return (memory_free < memoryLimit)? 1 : qtde;		
	}
	
	protected void showInfo(String info) {
		
		Toast.makeText(activity, info, Toast.LENGTH_LONG).show();
	}
	
	protected void showAlert(String alert) {
		
		showInfo(alert);
	}
}